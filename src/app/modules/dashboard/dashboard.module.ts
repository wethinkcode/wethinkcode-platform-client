import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';

import { PageDashboardComponent } from '../../components/pages/page-dashboard/page-dashboard.component';

import { SubPageDashboardComponent } from '../../components/sub-pages/sub-page-dashboard/sub-page-dashboard.component';
import { SubPageUsersComponent } from '../../components/sub-pages/sub-page-users/sub-page-users.component';
import { SubPageUserComponent } from '../../components/sub-pages/sub-page-user/sub-page-user.component';
import { SubPageAnalyticsComponent } from '../../components/sub-pages/sub-page-analytics/sub-page-analytics.component';
import { SubPageApplyComponent } from '../../components/sub-pages/sub-page-apply/sub-page-apply.component';
import { SubPageCalendarComponent } from '../../components/sub-pages/sub-page-calendar/sub-page-calendar.component';
import { SubPageCampusesComponent } from '../../components/sub-pages/sub-page-campuses/sub-page-campuses.component';
import { SubPageChatsComponent } from '../../components/sub-pages/sub-page-chats/sub-page-chats.component';
import { SubPageCodersCupComponent } from '../../components/sub-pages/sub-page-coders-cup/sub-page-coders-cup.component';
import { SubPageCommunityServiceComponent } from '../../components/sub-pages/sub-page-community-service/sub-page-community-service.component';
import { SubPageCurriculumComponent } from '../../components/sub-pages/sub-page-curriculum/sub-page-curriculum.component';
import { SubPageInternsComponent } from '../../components/sub-pages/sub-page-interns/sub-page-interns.component';
import { SubPageInternshipsComponent } from '../../components/sub-pages/sub-page-internships/sub-page-internships.component';
import { SubPageInternshipComponent } from '../../components/sub-pages/sub-page-internship/sub-page-internship.component';
import { SubPageNotificationsComponent } from '../../components/sub-pages/sub-page-notifications/sub-page-notifications.component';
import { SubPageProjectsComponent } from '../../components/sub-pages/sub-page-projects/sub-page-projects.component';
import { SubPageStudentPerformanceComponent } from '../../components/sub-pages/sub-page-student-performance/sub-page-student-performance.component';
import { SubPageSupportComponent } from '../../components/sub-pages/sub-page-support/sub-page-support.component';
import { SubPageSearchComponent } from '../../components/sub-pages/sub-page-search/sub-page-search.component';
import { SubPageStudentReportsComponent } from '../../components/sub-pages/sub-page-student-reports/sub-page-student-reports.component';
import { SubPageStudentReportsYear1Component } from '../../components/sub-pages/sub-page-student-reports-year-1/sub-page-student-reports-year-1.component';
import { SubPageStudentReportsYear2Component } from '../../components/sub-pages/sub-page-student-reports-year-2/sub-page-student-reports-year-2.component';
import { SubPageModulesComponent } from '../../components/sub-pages/sub-page-modules/sub-page-modules.component';
import { SubPageProjectComponent } from '../../components/sub-pages/sub-page-project/sub-page-project.component';
import { SubPageBootcampsComponent } from '../../components/sub-pages/sub-page-bootcamps/sub-page-bootcamps.component';
import { SubPageBootcampComponent } from '../../components/sub-pages/sub-page-bootcamp/sub-page-bootcamp.component';
import { SubPageReportsComponent } from '../../components/sub-pages/sub-page-reports/sub-page-reports.component';
import { SubPageCohortComponent } from '../../components/sub-pages/sub-page-cohort/sub-page-cohort.component';
import { SubPageCohortsComponent } from '../../components/sub-pages/sub-page-cohorts/sub-page-cohorts.component';
import { SubPageLdapUsersComponent } from '../../components/sub-pages/sub-page-ldap-users/sub-page-ldap-users.component';
import { SubPageStudentPerformanceProcessComponent } from '../../components/sub-pages/sub-page-student-performance-process/sub-page-student-performance-process.component';

import { ElementNavDashboardComponent } from '../../components/elements/element-nav-dashboard/element-nav-dashboard.component';
import { ElementQuickLinksDashboardComponent } from '../../components/elements/element-quick-links-dashboard/element-quick-links-dashboard.component';
import { ElementMenuDashboardComponent } from '../../components/elements/element-menu-dashboard/element-menu-dashboard.component';
import { ElementNotificationsComponent } from '../../components/elements/element-notifications/element-notifications.component';
import { ElementStudentPerformanceAttendanceComponent } from '../../components/elements/element-student-performance-attendance/element-student-performance-attendance.component';
import { ElementStudentPerformanceBootcampComponent } from '../../components/elements/element-student-performance-bootcamp/element-student-performance-bootcamp.component';
import { ElementStudentPerformanceYear1Component } from '../../components/elements/element-student-performance-year1/element-student-performance-year1.component';
import { ElementStudentPerformanceInternship1Component } from '../../components/elements/element-student-performance-internship1/element-student-performance-internship1.component';
import { ElementStudentPerformanceInternship2Component } from '../../components/elements/element-student-performance-internship2/element-student-performance-internship2.component';
import { ElementStudentPerformanceYear2Component } from '../../components/elements/element-student-performance-year2/element-student-performance-year2.component';
import { ElementComponentFailureComponent } from '../../components/elements/element-component-failure/element-component-failure.component';
import { ElementStudentModulesComponent } from '../../components/elements/element-student-modules/element-student-modules.component';
import { ElementUserInformationComponent } from '../../components/elements/element-user-information/element-user-information.component';
import { ElementEditUserInformationComponent } from '../../components/elements/element-edit-user-information/element-edit-user-information.component';
import { ElementUserNotesComponent } from '../../components/elements/element-user-notes/element-user-notes.component';
import { ElementNoteComponent } from '../../components/elements/element-note/element-note.component';
import { ElementProjectInstancesComponent } from '../../components/elements/element-project-instances/element-project-instances.component';
import { ElementsAttendanceComponent } from '../../components/elements/elements-attendance/elements-attendance.component';
import { ElementSliderFinalizeBootcampComponent } from '../../components/elements/element-slider-finalize-bootcamp/element-slider-finalize-bootcamp.component';

import { ElementSliderNewCampusComponent } from '../../components/elements/element-slider-new-campus/element-slider-new-campus.component';
import { ElementSliderNewCurriculumComponent } from '../../components/elements/element-slider-new-curriculum/element-slider-new-curriculum.component';
import { ElementSliderNewModuleComponent } from '../../components/elements/element-slider-new-module/element-slider-new-module.component';
import { ElementSliderNewProjectInstanceComponent } from '../../components/elements/element-slider-new-project-instance/element-slider-new-project-instance.component';
import { ElementSliderNewUserComponent } from '../../components/elements/element-slider-new-user/element-slider-new-user.component';
import { ElementSliderNewBootcampComponent } from '../../components/elements/element-slider-new-bootcamp/element-slider-new-bootcamp.component';
import { ElementSliderManageStaffVotersComponent } from '../../components/elements/element-slider-manage-staff-voters/element-slider-manage-staff-voters.component';
import { ElementSliderNewCohortComponent } from '../../components/elements/element-slider-new-cohort/element-slider-new-cohort.component';

import { ElementDateSelectorComponent } from '../../components/elements/element-date-selector/element-date-selector.component';
import { ElementSequenceComponent } from '../../components/elements/element-sequence/element-sequence.component';
import { ElementFileUploaderComponent } from '../../components/elements/element-file-uploader/element-file-uploader.component';
import { ElementBootcamperProjectsComponent } from '../../components/elements/element-bootcamper-projects/element-bootcamper-projects.component';
import { ElementMilestonesComponent } from '../../components/elements/element-milestones/element-milestones.component';

import { CampusService } from '../../services/campus.service';
import { CurriculumService } from '../../services/curriculum.service';
import { ModulesService } from '../../services/modules.service';
import { MilestoneService } from '../../services/milestone.service';
import { ToolService } from '../../services/tool.service';
import { BootcampService } from '../../services/bootcamp.service';
import { StaffService } from '../../services/staff.service';
import { BootcamperService } from '../../services/bootcamper.service';

import { StaffGuard } from '../../guards/staff.guard';
import { FileService } from '../../services/file.service';
import { CohortService } from '../../services/cohort.service';
import {StudentPerformanceService} from '../../services/student-performance.service';

import { StudentFilterPipe } from '../../pipes/student-filter.pipe';

const appRoutes = [
  {path: '', component: PageDashboardComponent, children:
    [
      { path: '', component: SubPageDashboardComponent },
      { path: 'analytics', component: SubPageAnalyticsComponent },
      { path: 'apply', component: SubPageApplyComponent },
      { path: 'calendar', component: SubPageCalendarComponent },
      { path: 'campuses', component: SubPageCampusesComponent },
      { path: 'chats', component: SubPageChatsComponent },
      { path: 'coders-cup', component: SubPageCodersCupComponent },
      { path: 'community-service', component: SubPageCommunityServiceComponent },
      { path: 'curriculum', component: SubPageCurriculumComponent },
      { path: 'interns', component: SubPageInternsComponent },
      { path: 'internship', component: SubPageInternshipComponent },
      { path: 'internships', component: SubPageInternshipsComponent },
      { path: 'notifications', component: SubPageNotificationsComponent },
      { path: 'projects/:id', component: SubPageProjectComponent },
      { path: 'projects', component: SubPageProjectsComponent },
      { path: 'reports/student-reports/year-1', component: SubPageStudentReportsYear1Component, canActivate: [StaffGuard]  },
      { path: 'reports/student-reports/year-2', component: SubPageStudentReportsYear2Component, canActivate: [StaffGuard]  },
      { path: 'reports/student-reports', redirectTo: 'reports/student-reports/year-1', pathMatch: 'full' },
      { path: 'reports', component: SubPageReportsComponent, canActivate: [StaffGuard] },
      { path: 'student-performance/:id', component: SubPageStudentPerformanceProcessComponent, canActivate: [StaffGuard] },
      { path: 'student-performance', component: SubPageStudentPerformanceComponent, canActivate: [StaffGuard] },
      { path: 'support', component: SubPageSupportComponent },
      { path: 'users/:id', component: SubPageUserComponent },
      { path: 'users', component: SubPageUsersComponent },
      { path: 'modules', component: SubPageModulesComponent },
      { path: 'bootcamps/:id', component: SubPageBootcampComponent, canActivate: [StaffGuard]  },
      { path: 'bootcamps', component: SubPageBootcampsComponent, canActivate: [StaffGuard]  },
      { path: 'cohorts/:id', component: SubPageCohortComponent, canActivate: [StaffGuard]  },
      { path: 'cohorts', component: SubPageCohortsComponent, canActivate: [StaffGuard]  },
      { path: 'ldap-users', component: SubPageLdapUsersComponent, canActivate: [StaffGuard]  }

    ]},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
    FormsModule,
    ChartsModule
  ],
  declarations: [

    StudentFilterPipe,
    PageDashboardComponent,
    SubPageDashboardComponent,
    SubPageUsersComponent,
    SubPageAnalyticsComponent,
    SubPageApplyComponent,
    SubPageCalendarComponent,
    SubPageCampusesComponent,
    SubPageChatsComponent,
    SubPageCodersCupComponent,
    SubPageCommunityServiceComponent,
    SubPageCurriculumComponent,
    SubPageInternsComponent,
    SubPageInternshipComponent,
    SubPageInternshipsComponent,
    SubPageNotificationsComponent,
    SubPageProjectsComponent,
    SubPageStudentPerformanceComponent,
    SubPageSupportComponent,
    SubPageUserComponent,
    SubPageSearchComponent,
    SubPageStudentReportsComponent,
    SubPageStudentReportsYear1Component,
    SubPageStudentReportsYear2Component,
    SubPageModulesComponent,
    SubPageProjectComponent,
    SubPageBootcampsComponent,
    SubPageBootcampComponent,
    SubPageCohortComponent,
    SubPageReportsComponent,
    SubPageCohortsComponent,
    SubPageLdapUsersComponent,
    SubPageStudentPerformanceProcessComponent,
    ElementDateSelectorComponent,
    ElementNavDashboardComponent,
    ElementQuickLinksDashboardComponent,
    ElementMenuDashboardComponent,
    ElementNotificationsComponent,
    ElementStudentPerformanceAttendanceComponent,
    ElementStudentPerformanceBootcampComponent,
    ElementStudentPerformanceInternship1Component,
    ElementStudentPerformanceInternship2Component,
    ElementStudentPerformanceYear1Component,
    ElementStudentPerformanceYear2Component,
    ElementComponentFailureComponent,
    ElementStudentModulesComponent,
    ElementUserInformationComponent,
    ElementEditUserInformationComponent,
    ElementUserNotesComponent,
    ElementNoteComponent,
    ElementProjectInstancesComponent,
    ElementsAttendanceComponent,
    ElementSliderNewCampusComponent,
    ElementSliderNewCurriculumComponent,
    ElementSliderNewModuleComponent,
    ElementSliderNewProjectInstanceComponent,
    ElementSliderNewUserComponent,
    ElementSliderNewBootcampComponent,
    ElementSliderFinalizeBootcampComponent,
    ElementSliderNewCohortComponent,
    ElementDateSelectorComponent,
    ElementSequenceComponent,
    ElementFileUploaderComponent,
    ElementSliderManageStaffVotersComponent,
    ElementBootcamperProjectsComponent,
    ElementMilestonesComponent

  ],
  providers: [
    StaffGuard,
    StaffService,
    CampusService,
    CurriculumService,
    BootcamperService,
    ModulesService,
    MilestoneService,
    ToolService,
    BootcampService,
    FileService,
    CohortService,
    StudentPerformanceService
  ]
})
export class DashboardModule { }
