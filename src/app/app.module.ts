import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { PageSigninComponent } from './components/pages/page-signin/page-signin.component';
import { PageHomeComponent } from './components/pages/page-home/page-home.component';
import { ElementNavComponent } from './components/elements/element-nav/element-nav.component';
import { ElementMenuComponent } from './components/elements/element-menu/element-menu.component';
import { ElementImageLoaderComponent } from './components/elements/element-image-loader/element-image-loader.component';
import { ElementSvgsComponent } from './components/elements/element-svgs/element-svgs.component';
import { PageSignupComponent } from './components/pages/page-signup/page-signup.component';
import { Page404Component } from './components/pages/page-404/page-404.component';

import { NavService } from './services/nav.service';
import { UserService } from './services/user.service';
import { StudentService } from './services/student.service';
import { APIService } from './services/api.service';
import { AuthenticationService } from './services/authentication.service';
import { NotificationService } from './services/notification.service';
import { NoteService } from './services/note.service';
import { ProjectService } from './services/project.service';
import { SocketService } from './services/socket.service';

import { AuthGuard } from './guards/auth.guard';

const routes = [
  { path: '', component: PageHomeComponent },
  { path: 'signin', component: PageSigninComponent },
  { path: 'signup', component: PageSignupComponent },
  { path: 'dashboard', loadChildren: './modules/dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard] },
  { path: 'page-not-found', component: Page404Component },
  { path: '**', component: Page404Component },
];

@NgModule({
  declarations: [
    AppComponent,
    PageSigninComponent,
    PageHomeComponent,
    ElementNavComponent,
    ElementMenuComponent,
    ElementSvgsComponent,
    PageSignupComponent,
    Page404Component,
    ElementImageLoaderComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpModule,
    ChartsModule
  ],
  providers: [
    NavService,
    UserService,
    StudentService,
    APIService,
    AuthGuard,
    AuthenticationService,
    NotificationService,
    NoteService,
    ProjectService,
    SocketService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
