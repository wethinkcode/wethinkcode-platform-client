export class SubPage {
  loading: Boolean = false;
  failure: Boolean = false;
  success: Boolean = false;
  resultMessage: String = '';
}
