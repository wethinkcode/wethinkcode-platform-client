import { Error } from '../models/error';

export class ElementSlider {
  loading: Boolean = false;
  success: Boolean = false;
  failure: Boolean = false;
  resultMessage: String = '';
  error: Error;

  constructor() {
    this.error = new Error('', '', '', 10);
  }

  clearError() {
    delete this.error;
  }
}
