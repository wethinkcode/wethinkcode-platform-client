import { Pipe, PipeTransform } from '@angular/core';

import { Student} from '../models/student';

import { CohortService } from '../services/cohort.service';

@Pipe({
  name: 'studentFilter',
  pure: false
})
export class StudentFilterPipe implements PipeTransform {
  constructor(
    private _cohortService: CohortService
  ) {}

  transform(students: Array<Student>, criteria: any): any {
    if (!students || !criteria) {
      return students;
    }
    const newArray = students.filter(student => {
      const filter = {
        tags: {
          nonPerformer: false,
          lowPerformer: false,
          performer: false,
          highPerformer: false,
          exempt: false,
          internship1Ready: false,
          year2Ready: false,
          internship2Ready: false
        },
        state: {
          active: false,
          inactive: false,
          awol: false,
          employed: false,
          terminated: false,
        },
        warningCount: {
          0: false,
          1: false,
          2: false,
          3: false,
          4: false
        }
      };
      if (student.state !== 'active' && (criteria.tags.nonPerformer || criteria.tags.lowPerformer || criteria.performer ||
          criteria.tags.highPerformer || criteria.tags.exempt || criteria.tags.internship1Ready || criteria.tags.year2Ready ||
          criteria.tags.internship2Ready)) {
        return false;
      }
      if (!criteria.tags.nonPerformer) {
        filter.tags.nonPerformer = false;
      } else {
        if (student.isNonPerformer) {
          filter.tags.nonPerformer = true;
        }
      }
      if (!criteria.tags.lowPerformer) {
        filter.tags.lowPerformer = false;
      } else {
        if (student.isLowPerformer) {
          filter.tags.lowPerformer = true;
        }
      }
      if (!criteria.tags.performer) {
        filter.tags.lowPerformer = false;
      } else {
        if (student.isPerformer) {
          filter.tags.performer = true;
        }
      }
      if (!criteria.tags.highPerformer) {
        filter.tags.highPerformer = false;
      } else {
        if (student.isHighPerformer) {
          filter.tags.highPerformer = true;
        }
      }
      if (!criteria.tags.highPerformer) {
        filter.tags.highPerformer = false;
      } else {
        if (student.isHighPerformer) {
          filter.tags.highPerformer = true;
        }
      }
      if (!criteria.tags.exempt) {
        filter.tags.exempt = false;
      } else {
        if (student.isExempt) {
          filter.tags.exempt = true;
        }
      }
      if (!criteria.tags.internship1Ready) {
        filter.tags.internship1Ready = false;
      } else {
        if (student.isInternship1Ready) {
          filter.tags.internship1Ready = true;
        }
      }
      if (!criteria.tags.year2Ready) {
        filter.tags.year2Ready = false;
      } else {
        if (student.isYear2Ready) {
          filter.tags.year2Ready = true;
        }
      }
      if (!criteria.tags.internship2Ready) {
        filter.tags.internship2Ready = false;
      } else {
        if (student.isInternship2Ready) {
          filter.tags.internship2Ready = true;
        }
      }
      if (!criteria.state.active) {
        filter.state.active = false;
      } else {
        if (student.state === 'active') {
          filter.state.active = true;
        }
      }
      if (!criteria.state.inactive) {
        filter.state.inactive = false;
      } else {
        if (student.state === 'inactive') {
          filter.state.active = true;
        }
      }
      if (!criteria.state.awol) {
        filter.state.awol = false;
      } else {
        if (student.state === 'awol') {
          filter.state.active = true;
        }
      }
      if (!criteria.state.terminated) {
        filter.state.terminated = false;
      } else {
        if (student.state === 'terminated') {
          filter.state.active = true;
        }
      }
      if (!criteria.state.employed) {
        filter.state.employed = false;
      } else {
        if (student.state === 'employed') {
          filter.state.active = true;
        }
      }
      if (filter.tags.nonPerformer || filter.tags.lowPerformer || filter.tags.performer || filter.tags.highPerformer ||
        filter.tags.exempt  || filter.tags.internship1Ready || filter.tags.year2Ready || filter.tags.internship2Ready ||
      filter.state.active || filter.state.inactive || filter.state.awol || filter.state.terminated || filter.state.employed) {
        return false;
      }
      return true;
    });
    this._cohortService.selectedCohort.studentCount = newArray.length;
    return newArray;
  }

}
