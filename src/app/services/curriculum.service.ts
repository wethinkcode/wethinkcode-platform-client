import { Injectable } from '@angular/core';

import { APIService } from './api.service';

import { Curriculum } from '../models/curriculum';

@Injectable()
export class CurriculumService {
  limit = 20;
  page = 1;
  curriculum: Array<Curriculum> = [];

  constructor(
    private _APIService: APIService
  ) {
  }

  newCurriculum(curriculum) {
    return this._APIService.newCurriculum(curriculum);
  }

  getMoreCurriculum() {
    return this._APIService.getCurriculum(this.page, this.limit);
  }

}
