import { Injectable } from '@angular/core';

import { APIService } from './api.service';
import { User } from '../models/user';

@Injectable()
export class UserService {
  selectedUser: any;
  user: User;

  constructor(private _APIService: APIService) { }

  validateEmail(email) {
    const emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return emailRegex.test(email);
  }

  userActivation(id, token) {
    return this._APIService.activateUserAccount(id, token);
  }

  userSignup(user) {
    return this._APIService.createUserAccount(user);
  }

  userSignin(user) {
    return this._APIService.signinUser(user);
  }

  userInvite(user) {
    return this._APIService.inviteUser(user);
  }

  userResetInit(user) {
    return this._APIService.resetInitUserAccount(user);
}

  userReset(user, id, token) {
    return this._APIService.resetUserAccount(user, id, token);
  }

  userCheckReset(id, token) {
    return this._APIService.checkUserResetToken(id, token);
  }

  getUsers(page, searchParams) {
    return this._APIService.getUsers(page, searchParams);
  }

  getUser(id) {
    return this._APIService.getUser(id);
  }

  getUserInformation(id) {
    return this._APIService.getUserInformation(id);
  }
}
