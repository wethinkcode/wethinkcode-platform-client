import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { APIService } from './api.service';
import { NavService } from './nav.service';

@Injectable()
export class StudentService {
  currentStudent: Object = {};
  students: Array<any> = [];
  attendanceFilter: Array<any> = [];
  examsPassedFilter: Array<any> = [];
  modulesValidatedY1Filter: Array<any> = [];
  modulesValidatedY2Filter: Array<any> = [];
  visibleStudents: any = 0;
  examFilter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  unix1Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  studentStateFilter: Array<any> = [
    {type: 'active', enabled: true},
    {type: 'employed', enabled: true},
    {type: 'terminated', enabled: true},
    {type: 'awol', enabled: true},
    {type: 'inactive', enabled: true},
  ];
  unix2Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  algorithms1Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  algorithms2Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  graphics1Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  graphics2Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  PHPBootcampFilter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  webFilter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  internship1Filter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  CPPBootcampFilter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  CPPFilter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  kernelFilter: Array<any> = [
    {type: 'validated', enabled: true},
    {type: 'not validated', enabled: true}
  ];
  attendanceSort = 'none';
  studentNameSort = 'none';
  studentUsernameSort = 'none';
  examsPassedSort = 'none';
  modulesValidatedY1Sort = 'none';
  modulesValidatedY2Sort = 'none';

  constructor(
    private _APIService: APIService,
    private _navService: NavService) {
    for (let i = 0; i <= 31; i++) {
      this.attendanceFilter.push({day: i, enabled: true});
    }
    for (let i = 0; i <= 20; i++) {
      this.examsPassedFilter.push({value: i, enabled: true});
    }
    for (let i = 0; i <= 20; i++) {
      this.modulesValidatedY1Filter.push({value: i, enabled: true});
    }
    for (let i = 0; i <= 20; i++) {
      this.modulesValidatedY2Filter.push({value: i, enabled: true});
    }
  }

  sortByAttendanceArray(array, type) {
    if (type === 'ascending') {
      array.sort((a, b) => a.attendanceLastMonth - b.attendanceLastMonth);
    } else if (type === 'descending') {
      array.sort((a, b) => b.attendanceLastMonth - a.attendanceLastMonth);
    }
  }

  sortByExamsPassedArray(array, type) {
    if (type === 'ascending') {
      array.sort((a, b) => this.examsPassed(a.exams) - this.examsPassed(b.exams));
    } else if (type === 'descending') {
      array.sort((a, b) => this.examsPassed(b.exams) - this.examsPassed(a.exams));
    }
  }

  sortByModulesValidatedY1Array(array, type) {
    if (type === 'ascending') {
      array.sort((a, b) => a.validatedModulesY1 - b.validatedModulesY1);
    } else if (type === 'descending') {
      array.sort((a, b) => b.validatedModulesY1 - a.validatedModulesY1);
    }
  }

  sortByModulesValidatedY2Array(array, type) {
    if (type === 'ascending') {
      array.sort((a, b) => a.validatedModulesY2 - b.validatedModulesY2);
    } else if (type === 'descending') {
      array.sort((a, b) => b.validatedModulesY2 - a.validatedModulesY2);
    }
  }

  sortByNameArray(array, type) {
    if (type === 'ascending') {
      array.sort((a, b) => { if (a.first_name < b.first_name) { return -1; } if (a.first_name > b.first_name) { return 1; } return 0; });
    } else if (type === 'descending') {
      array.sort((a, b) => { if (b.first_name < a.first_name) { return -1; } if (b.first_name > a.first_name) { return 1; } return 0; });
    }
  }

  sortByUsernameArray(array, type) {
    if (type === 'ascending') {
      array.sort((a, b) => { if (a.username < b.username) { return -1; } if (a.username > b.username) { return 1; } return 0; });
    } else if (type === 'descending') {
      array.sort((a, b) => { if (b.username < a.username) { return -1; } if (b.username > a.username) { return 1; } return 0; });
    }
  }

  toggleAttendanceFilter(array, value) {
    for (const user of array) {
      if (user.attendanceLastMonth === value) {
        if (!user.filters.filterByAttendance) {
          user.filters.filterByAttendance = true;
        } else {
          user.filters.filterByAttendance = false;
        }
        this.setFilterVisible(user);
      }
    }
  }

  toggleStudentNameFilter(array, selectedUser) {
    for (const user of array) {
      if (user === selectedUser) {
        user.filters.filterByName = !user.filters.filterByName;
      }
      this.setFilterVisible(user);
    }
  }

  toggleStateFilter(array, filter) {
    for (const user of array) {
      if (user.state === filter.type) {
        if (!user.filters.filterByState) {
          user.filters.filterByState = true;
        } else {
          user.filters.filterByState = false;
        }
        this.setFilterVisible(user);
      }
    }
  }

  toggleAllFilterName(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByName = false;
      } else if (value === 'hide') {
        user.filters.filterByName = true;
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterUsername(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByUsername = false;
      } else if (value === 'hide') {
        user.filters.filterByUsername = true;
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterState(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByState = false;
        for (const filter of this.studentStateFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByState = true;
        for (const filter of this.studentStateFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterAttendance(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByAttendance = false;
        for (const filter of this.attendanceFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByAttendance = true;
        for (const filter of this.attendanceFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterExamsPassed(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByExamsPassed = false;
        for (const filter of this.examsPassedFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByExamsPassed = true;
        for (const filter of this.examsPassedFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterModulesValidatedY1(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByModulesValidatedY1 = false;
        for (const filter of this.modulesValidatedY1Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByModulesValidatedY1 = true;
        for (const filter of this.modulesValidatedY1Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterModulesValidatedY2(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByModulesValidatedY2 = false;
        for (const filter of this.modulesValidatedY2Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByModulesValidatedY2 = true;
        for (const filter of this.modulesValidatedY2Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterExam(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByExam = false;
        for (const filter of this.examFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByExam = true;
        for (const filter of this.examFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterUnix1(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByUnix1 = false;
        for (const filter of this.unix1Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByUnix1 = true;
        for (const filter of this.unix1Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterUnix2(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByUnix2 = false;
        for (const filter of this.unix2Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByUnix2 = true;
        for (const filter of this.unix2Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterAlgorithms1(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByAlgorithms1 = false;
        for (const filter of this.algorithms1Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByAlgorithms1 = true;
        for (const filter of this.algorithms1Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterAlgorithms2(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByAlgorithms2 = false;
        for (const filter of this.algorithms2Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByAlgorithms2 = true;
        for (const filter of this.algorithms2Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterGraphics1(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByGraphics1 = false;
        for (const filter of this.graphics1Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByGraphics1 = true;
        for (const filter of this.graphics1Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterGraphics2(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByGraphics2 = false;
        for (const filter of this.graphics2Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByGraphics2 = true;
        for (const filter of this.graphics2Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterPHPBootcamp(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByPHPBootcamp = false;
        for (const filter of this.PHPBootcampFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByPHPBootcamp = true;
        for (const filter of this.PHPBootcampFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterWeb(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByWeb = false;
        for (const filter of this.webFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByWeb = true;
        for (const filter of this.webFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterInternship1(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByInternship1 = false;
        for (const filter of this.internship1Filter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByInternship1 = true;
        for (const filter of this.internship1Filter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterCPPBootcamp(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByCPPBootcamp = false;
        for (const filter of this.CPPBootcampFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByCPPBootcamp = false;
        for (const filter of this.CPPBootcampFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterCPP(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByCPP = false;
        for (const filter of this.CPPFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByCPP = true;
        for (const filter of this.CPPFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAllFilterKernel(array, value) {
    for (const user of array) {
      if (value === 'show') {
        user.filters.filterByKernel = false;
        for (const filter of this.kernelFilter) {
          filter.enabled = true;
        }
      } else if (value === 'hide') {
        user.filters.filterByKernel = true;
        for (const filter of this.kernelFilter) {
          filter.enabled = false;
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleStudentUsernameFilter(array, selectedUser) {
    for (const user of array) {
      if (user === selectedUser) {
        user.filters.filterByUsername = !user.filters.filterByUsername;
      }
      this.setFilterVisible(user);
    }
  }

  toggleExamsPassedFilter(array, value) {
    for (const user of array) {
      if (value === this.examsPassed(user.exams)) {
        user.filters.filterByExamsPassed = !user.filters.filterByExamsPassed;
      }
      this.setFilterVisible(user);
    }
  }

  toggleExamFilter(array) {
    for (const user of array) {
      for (const filter of this.examFilter) {
        if (filter.type === 'validated') {
          if (user.validatedCExam && filter.enabled) {
            user.filters.filterByExam = false;
          } else if (user.validatedCExam && !filter.enabled) {
            user.filters.filterByExam = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedCExam && filter.enabled) {
            user.filters.filterByExam = false;
          } else if (!user.validatedCExam && !filter.enabled) {
            user.filters.filterByExam = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleModulesValidatedY1Filter(array, value) {
    for (const user of array) {
      if (value === user.validatedModulesY1) {
        user.filters.filterByModulesValidatedY1 = !user.filters.filterByModulesValidatedY1;
      }
      this.setFilterVisible(user);
    }
  }

  toggleModulesValidatedY2Filter(array, value) {
    for (const user of array) {
      if (value === user.validatedModulesY2) {
        user.filters.filterByModulesValidatedY2 = !user.filters.filterByModulesValidatedY2;
      }
      this.setFilterVisible(user);
    }
  }

  toggleUnix1Filter(array) {
    for (const user of array) {
      for (const filter of this.unix1Filter) {
        if (filter.type === 'validated') {
          if (user.validatedUnix1 && filter.enabled) {
            user.filters.filterByUnix1 = false;
          } else if (user.validatedUnix1 && !filter.enabled) {
            user.filters.filterByUnix1 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedUnix1 && filter.enabled) {
            user.filters.filterByUnix1 = false;
          } else if (!user.validatedUnix1 && !filter.enabled) {
            user.filters.filterByUnix1 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAlgorithms1Filter(array) {
    for (const user of array) {
      for (const filter of this.algorithms1Filter) {
        if (filter.type === 'validated') {
          if (user.validatedAlgorithms1 && filter.enabled) {
            user.filters.filterByAlgorithms1 = false;
          } else if (user.validatedAlgorithms1 && !filter.enabled) {
            user.filters.filterByAlgorithms1 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedAlgorithms1 && filter.enabled) {
            user.filters.filterByAlgorithms1 = false;
          } else if (!user.validatedAlgorithms1 && !filter.enabled) {
            user.filters.filterByAlgorithms1 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleGraphics1Filter(array) {
    for (const user of array) {
      for (const filter of this.graphics1Filter) {
        if (filter.type === 'validated') {
          if (user.validatedGraphics1 && filter.enabled) {
            user.filters.filterByGraphics1 = false;
          } else if (user.validatedGraphics1 && !filter.enabled) {
            user.filters.filterByGraphics1 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedGraphics1 && filter.enabled) {
            user.filters.filterByGraphics1 = false;
          } else if (!user.validatedGraphics1 && !filter.enabled) {
            user.filters.filterByGraphics1 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  togglePHPBootcampFilter(array) {
    for (const user of array) {
      for (const filter of this.PHPBootcampFilter) {
        if (filter.type === 'validated') {
          if (user.validatedPHPBootcamp && filter.enabled) {
            user.filters.filterByPHPBootcamp = false;
          } else if (user.validatedPHPBootcamp && !filter.enabled) {
            user.filters.filterByPHPBootcamp = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedPHPBootcamp && filter.enabled) {
            user.filters.filterByPHPBootcamp = false;
          } else if (!user.validatedPHPBootcamp && !filter.enabled) {
            user.filters.filterByPHPBootcamp = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleWebFilter(array) {
    for (const user of array) {
      for (const filter of this.webFilter) {
        if (filter.type === 'validated') {
          if (user.validatedWeb && filter.enabled) {
            user.filters.filterByWeb = false;
          } else if (user.validatedUWeb && !filter.enabled) {
            user.filters.filterByWeb = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedWeb && filter.enabled) {
            user.filters.filterByWeb = false;
          } else if (!user.validatedWeb && !filter.enabled) {
            user.filters.filterByWeb = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleAlgorithms2Filter(array) {
    for (const user of array) {
      for (const filter of this.algorithms2Filter) {
        if (filter.type === 'validated') {
          if (user.validatedAlgorithms2 && filter.enabled) {
            user.filters.filterByAlgorithms2 = false;
          } else if (user.validatedAlgorithms2 && !filter.enabled) {
            user.filters.filterByAlgorithms2 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedAlgorithms2 && filter.enabled) {
            user.filters.filterByAlgorithms2 = false;
          } else if (!user.validatedAlgorithms2 && !filter.enabled) {
            user.filters.filterByAlgorithms2 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleGraphics2Filter(array) {
    for (const user of array) {
      for (const filter of this.graphics2Filter) {
        if (filter.type === 'validated') {
          if (user.validatedGraphics2 && filter.enabled) {
            user.filters.filterByGraphics2 = false;
          } else if (user.validatedGraphics2 && !filter.enabled) {
            user.filters.filterByGraphics2 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedGraphics2 && filter.enabled) {
            user.filters.filterByGraphics2 = false;
          } else if (!user.validatedGraphics2 && !filter.enabled) {
            user.filters.filterByGraphics2 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleCPPBootcampFilter(array) {
    for (const user of array) {
      for (const filter of this.CPPBootcampFilter) {
        if (filter.type === 'validated') {
          if (user.validatedCPPBootcamp && filter.enabled) {
            user.filters.filterByCPPBootcamp = false;
          } else if (user.validatedCPPBootcamp && !filter.enabled) {
            user.filters.filterByCPPBootcamp = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedCPPBootcamp && filter.enabled) {
            user.filters.filterByCPPBootcamp = false;
          } else if (!user.validatedCPPBootcamp && !filter.enabled) {
            user.filters.filterByCPPBootcamp = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleCPPFilter(array) {
    for (const user of array) {
      for (const filter of this.CPPFilter) {
        if (filter.type === 'validated') {
          if (user.validatedCPP && filter.enabled) {
            user.filters.filterByCPP = false;
          } else if (user.validatedCPP && !filter.enabled) {
            user.filters.filterByCPP = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedCPP && filter.enabled) {
            user.filters.filterByCPP = false;
          } else if (!user.validatedCPP && !filter.enabled) {
            user.filters.filterByCPP = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleKernelFilter(array) {
    for (const user of array) {
      for (const filter of this.kernelFilter) {
        if (filter.type === 'validated') {
          if (user.validatedKernel && filter.enabled) {
            user.filters.filterByKernel = false;
          } else if (user.validatedKernel && !filter.enabled) {
            user.filters.filterByKernel = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedKernel && filter.enabled) {
            user.filters.filterByKernel = false;
          } else if (!user.validatedKernel && !filter.enabled) {
            user.filters.filterByKernel = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleInternship1Filter(array) {
    for (const user of array) {
      for (const filter of this.internship1Filter) {
        if (filter.type === 'validated') {
          if (user.validatedInternship1 && filter.enabled) {
            user.filters.filterByInternship1 = false;
          } else if (user.validatedInternship1 && !filter.enabled) {
            user.filters.filterByInternship1 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedInternship1 && filter.enabled) {
            user.filters.filterByInternship1 = false;
          } else if (!user.validatedInternship1 && !filter.enabled) {
            user.filters.filterByInternship1 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  toggleUnix2Filter(array) {
    for (const user of array) {
      for (const filter of this.unix2Filter) {
        if (filter.type === 'validated') {
          if (user.validatedUnix2 && filter.enabled) {
            user.filters.filterByUnix2 = false;
          } else if (user.validatedUnix2 && !filter.enabled) {
            user.filters.filterByUnix2 = true;
          }
        } else if (filter.type === 'not validated') {
          if (!user.validatedUnix2 && filter.enabled) {
            user.filters.filterByUnix2 = false;
          } else if (!user.validatedUnix2 && !filter.enabled) {
            user.filters.filterByUnix2 = true;
          }
        }
      }
      this.setFilterVisible(user);
    }
  }

  setFilterVisible(user) {
    if (user.filters.filterByAttendance || user.filters.filterByExam || user.filters.filterByUnix1 ||
      user.filters.filterByUnix2 || user.filters.filterByGraphics1 || user.filters.filterByGraphics2 ||
      user.filters.filterByAlgorithms1 || user.filters.filterByAlgorithms2 ||
      user.filters.filterByPHPBootcamp || user.filters.filterByWeb || user.filters.filterByCPPBootcamp ||
      user.filters.filterByCPP || user.filters.filterByKernel || user.filters.filterByName ||
      user.filters.filterByUsername || user.filters.filterByState || user.filters.filterByInternship1 ||
      user.filters.filterByExamsPassed || user.filters.filterByModulesValidatedY1 || user.filters.filterByModulesValidatedY2) {
      if (user.visible$.getValue() === true) {
        this.visibleStudents--;
      }
      user.visible$.next(false);
    } else {
      if (user.visible$.getValue() === false) {
        this.visibleStudents++;
      }
      user.visible$.next(true);
    }
  }

  getStudentsCompliance(page, resultsPerPage) {
    return this._APIService.getStudentsCompliance(page, resultsPerPage);
  }

  getStudentAttendance(id) {
    return this._APIService.getStudentAttendance(id);
  }

  getModules() {
    return this._APIService.getModules(1, 20);
  }

  getStudentData(id) {
    return this._APIService.getStudentData(id);
  }

  getStudentReportsData(year, page, resultsPerPage) {
    return this._APIService.getStudentReportsData(year, page, resultsPerPage);
  }

  getStudentProjects(id, moduleId) {
    return this._APIService.getStudentProjects(id, moduleId);
  }

  isAttendanceCompliant(student): Observable<any> {
    let returnValue = 0;
    const days30 = new Date().getTime() - (30 * 24 * 60 * 60 * 1000);
    let i = 0;
    for (const attendance of student.attendance) {
      i++;
      const accessDate: any = new Date(attendance.access_date).getTime();
      if (accessDate >= days30) {
        returnValue++;
      }
    }
    return Observable.of(returnValue);
  }

  isAttendanceCompliantLastMonth(student): Observable<any> {
    let returnValue = 0;
    const month = (new Date().getMonth() - 1) % 11;
    for (const attendance of student.attendance) {
      const accessDate: any = new Date(attendance.access_date).getMonth();
      if (accessDate === month) {
       returnValue++;
      }
    }
    student.attendanceLastMonth = returnValue;
    return Observable.of(returnValue);
  }

  isModuleValidated(user, module, projects) {
    let smallProjectsValidated = 0;
    let bigProjectsValidated = 0;
    let internship1Validated = 0;
    let internship2Validated = 0;
    let cppBootcampValidated = 0;
    let phpBootcampValidated = 0;
    let socialTechValidated = 0;
    let examValidated = 0;
    let rushesValidated = 0;
    let returnValue = false;
    for (const project of projects) {
      if ((project.slug === 'piscine-php' && module.slug === 'web' && user.year !== 2016 && project.final_mark >= project.pass_percentage) ||
        (project.slug === 'piscine-cpp' && module.slug === 'cpp' && project.final_mark >= project.pass_percentage) ||
        (module.id === project.module_id && project.final_mark >= project.pass_percentage)) {
        switch (project.size) {
          case ('small'):
            smallProjectsValidated++;
            break;
          case ('big'):
            bigProjectsValidated++;
            break;
          case ('internship'):
            if (module.slug === 'internship1') {
              internship1Validated++;
            } else {
              internship2Validated++;
            }
            break;
          case ('bootcamp'):
            if (module.slug === 'piscine-cpp' || (project.slug === 'piscine-cpp' && module.slug === 'cpp')) {
              cppBootcampValidated++;
            } else if (module.slug === 'piscine-php' || (project.slug === 'piscine-php' && module.slug === 'web' && user.year !== 2016)) {
              phpBootcampValidated++;
            }
            break;
          case ('exam'):
            examValidated++;
            break;
          case('rushes'):
            rushesValidated++;
            break;
          case('social-tech-lab'):
            socialTechValidated++;
            break;
          default:
            break;
        }
      }
    }
    switch (user.year) {
      case 2016: {
        switch (module.slug) {
          case 'unix1':
          case 'algo1':
            if ((bigProjectsValidated && smallProjectsValidated) || smallProjectsValidated >= 3) {
              returnValue = true;
            }
            break;
          case 'c-exam':
            if (examValidated >= 1) {
              returnValue = true;
            }
            break;
          case 'graphics1':
          case 'algo2':
            if ((bigProjectsValidated && smallProjectsValidated) || smallProjectsValidated >= 2) {
              returnValue = true;
            }
            break;
          case 'devops':
            if (smallProjectsValidated) {
              returnValue = true;
            }
            break;
          case 'graphics2':
          case 'security':
          case 'ocaml':
          case 'network':
          case 'java-android':
          case 'math':
          case 'swift-ios':
          case 'virus':
          case 'unity':
            if (smallProjectsValidated >= 2) {
              returnValue = true;
            }
            break;
          case 'web': {
            if (bigProjectsValidated || smallProjectsValidated >=  2) {
              returnValue = true;
            }
            break;
          }
          case 'internship1':
            if (internship1Validated) {
              returnValue = true;
            }
            break;
          case 'internship2':
            if (internship2Validated) {
              returnValue = true;
            }
            break;
          case 'social-tech-lab':
            if (socialTechValidated) {
              returnValue = true;
            }
            break;
          case 'rushes':
            if (rushesValidated) {
              returnValue = true;
            }
            break;
          case 'piscine-cpp':
            if (cppBootcampValidated) {
              returnValue = true;
            }
            break;
          case 'piscine-php':
            if (phpBootcampValidated) {
              returnValue = true;
            }
            break;
          case 'cpp':
            if ((bigProjectsValidated && (smallProjectsValidated || cppBootcampValidated)) || (smallProjectsValidated + cppBootcampValidated >= 2)) {
              returnValue = true;
            }
            break;
          case 'unix2':
            if ((bigProjectsValidated && smallProjectsValidated) || smallProjectsValidated >= 2) {
              returnValue = true;
            }
            break;
          case 'kernel':
            if (smallProjectsValidated >= 3) {
              returnValue = true;
            }
            break;
          default:
            break;
        }
        break;
      }
      default: {
        switch (module.slug) {
          case 'devops':
            if (smallProjectsValidated) {
              returnValue = true;
            }
            break;
          case 'unix1':
          case 'algo1':
            if ((bigProjectsValidated && smallProjectsValidated) || (smallProjectsValidated >= 3)) {
              returnValue = true;
            }
            break;
          case 'kernel':
            if (smallProjectsValidated >= 3) {
              returnValue = true;
            }
            break;
          case 'unix2':
          case 'graphics1':
          case 'algo2':
          case 'security':
          case 'ocaml':
          case 'network':
          case 'java-android':
          case 'math':
          case 'virus':
          case 'unity':
          case 'swift-ios':
            if ((bigProjectsValidated && smallProjectsValidated) || (smallProjectsValidated >= 2)) {
              returnValue = true;
            }
            break;
          case 'web':
            if ((bigProjectsValidated && (smallProjectsValidated || phpBootcampValidated)) || (smallProjectsValidated + phpBootcampValidated >= 2)) {
              returnValue = true;
            }
            break;
          case 'cpp':
            if ((bigProjectsValidated && (smallProjectsValidated || cppBootcampValidated)) || (smallProjectsValidated + cppBootcampValidated >= 2)) {
              returnValue = true;
            }
            break;
          case 'c-exam':
            if (examValidated >= 1) {
              returnValue = true;
            }
            break;
          case 'internship1':
            if (internship1Validated) {
              returnValue = true;
            }
            break;
          case 'internship2':
            if (internship2Validated) {
              returnValue = true;
            }
            break;
          case 'social-tech-lab':
            if (socialTechValidated) {
              returnValue = true;
            }
            break;
          case 'rushes':
            if (rushesValidated) {
              returnValue = true;
            }
            break;
          case 'piscine-cpp':
            if (cppBootcampValidated) {
              returnValue = true;
            }
            break;
          case 'piscine-php':
            if (phpBootcampValidated) {
              returnValue = true;
            }
            break;
          case 'graphics2':
            if (smallProjectsValidated >= 2) {
              returnValue = true;
            }
            break;
          default:
            break;
        }
        break;
      }
    }
    return returnValue;
  }

  isEligibleStatus(status, requiredStatus) {
    let returnValue: Boolean = false;
    switch (requiredStatus) {
      case('bootcamp'):
        if (status === 'bootcamp' || status === 'first_year' || status === 'first_internship' || status === 'second_year' || status === 'second_internship') {
          returnValue = true;
        }
        break;
      case('first_year'):
        if (status === 'first_year' || status === 'first_internship' || status === 'second_year' || status === 'second_internship') {
          returnValue = true;
        }
        break;
      case('first_internship'):
        if (status === requiredStatus || status === 'second_year' || status === 'second_internship') {
          returnValue = true;
        }
        break;
      case('second_year'):
        if (status === requiredStatus || status === 'second_internship') {
          returnValue = true;
        }
        break;
      case('second_internship'):
        if (status === requiredStatus) {
          returnValue = true;
        }
        break;
    }
    return Observable.of(returnValue);
  }

  isMilestoneCompliant(milestoneToCheck, milestones) {
    let returnValue = false;
    for (const milestone of milestones) {
      if (milestone.title === milestoneToCheck) {
        if (milestone.is_validated === true) {
          returnValue = true;
        }
      }
    }
    return Observable.of(returnValue);
  }

  examsPassed(exams) {
    let count = 0;
    for (const exam of exams) {
      if (exam.grade >= 75 && exam.exam_id === 510) {
        count++;
      }
    }
    return count;
  }
}
