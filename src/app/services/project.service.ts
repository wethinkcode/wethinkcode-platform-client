import { Injectable } from '@angular/core';

import { Project } from '../models/project';

import { APIService } from './api.service';
import { ProjectInstance } from '../models/project-instance';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ProjectService {
  selectedProject: Project;
  projects: Array<Project> = [];
  projectInstance: ProjectInstance;
  projectInstances: Array<ProjectInstance> = [];
  page: Number = 1;
  limit: Number = 20;

  constructor(
    private _APIService: APIService
  ) {
    this.projectInstance = new ProjectInstance({});
  }

  getMoreProjects(page, limit) {
    return this._APIService.getProjects(page, limit);
  }

  getMoreProjectInstances(projectId, page, limit) {
    return this._APIService.getProjectInstances(projectId, page, limit);
  }

  getProject(id) {
    return this._APIService.getProject(id);
  }

  newProject(project) {
    return this._APIService.newProject(project);
  }

  newProjectInstance(projectInstance) {
    return this._APIService.newProjectInstance(this.selectedProject, projectInstance);
  }

}
