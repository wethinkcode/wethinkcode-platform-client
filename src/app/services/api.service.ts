import { Injectable, isDevMode } from '@angular/core';
import { User } from '../models/user';
import { RequestOptions, Headers, Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class APIService {
  token: String = '';
  host = 'https://wethinkcode-api-production.herokuapp.com';

  constructor(private _http: Http) {
    this.fetchToken();
  }

  public fetchToken() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
    }
  }

  // User Tasks
  public createUserAccount(user: User) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this._http.post('/users', user, options)
      .map(response => response.json());
  }

  public inviteUser(user: User) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/users/invite', user, options)
      .map(response => response.json());
  }

  public activateUserAccount(id, token) {
    const body = {};
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token });
    const options = new RequestOptions({ headers: headers });
    return this._http.patch(this.host + '/users/auth/activate/' +
      id, body, options).map(response => response.json());
  }

  public resetInitUserAccount(user: User) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/users/auth/reset', user, options)
      .map(response => response.json());
  }

  public resetUserAccount(user: User, id: Number, token: String) {
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token });
    const options = new RequestOptions({ headers: headers });
    return this._http.patch(this.host + '/users/auth/reset/' +
      id, user, options).map(response => response.json());
  }

  public signinUser(user: User) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/users/auth', user, options)
      .map(response => response.json());
  }

  public getUsers(page: Number, searchParams: any) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/users?page=' + page +
      '&search=' + searchParams, options).map(response => response.json());
  }

  public getBootcampUsers(bootcampId: Number, page: Number, limit: Number) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/bootcamps/' + bootcampId + '/users?page=' + page +
      '&results_per_page=' + limit, options).map(response => response.json());
  }

  public getUser(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/users/' + id, options)
      .map(response => response.json());
  }

  public getUserQuickLinks(userId: Number) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/users/' + userId +
      '/quicklinks', options).map(response => response.json());
  }

  public newUserQuicklink(userId, toolId) {
    if (!this.token) {
      this.fetchToken();
    }
    const body = {toolId: toolId};
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/users/' + userId +
      '/quicklinks', body, options).map(response => response.json());
  }

  public newBootcamp(bootcamp) {
    if (!this.token) {
      this.fetchToken();
    }
    const body = bootcamp;
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/bootcamps', body, options).map(response => response.json());
  }

  public getCampuses(page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/campuses?page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getStaff(page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/staff?page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public subscribeUserToBootcamp(bootcampId, user, type) {
    const body = {users: []};
    body.users.push(user);
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/bootcamps/' + bootcampId + '/users?role=' + type, body, options).map(response => response.json());
  }

  public unsubscribeUserFromBootcamp(bootcampId, user, type) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.delete(this.host + '/bootcamps/' + bootcampId + '/users?role=' + type + '&user_id=' + user.id, options).map(response => response.json());
  }

  public getBootcampStaff(page, resultsPerPage, bootcampId) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/bootcamps/' + bootcampId + '/users?role=staff&page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getBootcamperProjects(bootcamperId, page, limit) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/bootcampers/' + bootcamperId + '/projects?role=staff&page=' + page +
      '&results_per_page=' + limit, options).map(response => response.json());
  }

  public getCurriculum(page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/curriculum?page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getBootcamps(page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/bootcamps?page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getCohorts(page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/cohorts?page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getBootcamp(bootcampId) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/bootcamps/' + bootcampId, options).map(response => response.json());
  }


  public getCohort(cohortId) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/cohorts/' + cohortId, options).map(response => response.json());
  }

  public deleteUserQuickLink(userId: Number, toolId: Number) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.delete(this.host + '/users/' + userId +
      '/quicklinks?tool_id=' + toolId, options).map(response => response.json());
  }

  // Validation tasks
  public validateToken() {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token});
    const options = new RequestOptions({headers: headers});
    return this._http.get(this.host + '/users/auth', options).
    map(response => response.json());
  }

  public checkUserResetToken(id: Number, token: String) {
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/users/auth/reset/' + id, options)
      .map(response => response.json());
  }

  // Tools
  public getUserTools(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/tools/' + id, options)
      .map(response => response.json());
  }

  getAllTools() {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/tools', options)
      .map(response => response.json());
  }

  public getUserInformation(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/users/' + id +
      '/information', options).map(response => response.json());
  }

  // Student Tools
  public getStudentsCompliance(page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/students/compliance?page=' + page +
      '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getStudentAttendance(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/students/' + id +
      '/attendance', options).map(response => response.json());
  }

  public getStudentModules(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/students/' + id +
      '/modules', options).map(response => response.json());
  }

  public getModules(page, limit) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/modules?page=' + page + '&results_per_page=' + limit, options)
      .map(response => response.json());
  }

  public getProjects(page, limit) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/projects?page=' + page + '&results_per_page=' + limit, options)
      .map(response => response.json());
  }

  public getProject(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/projects/' + id, options)
      .map(response => response.json());
  }

  public getMilestones(page, limit) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/milestones?page=' + page + '&results_per_page=' + limit, options)
      .map(response => response.json());
  }

  public getProjectInstances(projectId, page, limit) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/projects/' + projectId + '/instances?page=' + page + '&results_per_page=' + limit, options)
      .map(response => response.json());
  }

  public getStudentData(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/students/' + id, options)
      .map(response => response.json());
  }

  public getStudentProjects(id, moduleId) {
    if (!this.token) {
      this.fetchToken();
    }
    let queryParam = '';
    if (moduleId) {
      queryParam = '?module=' + moduleId;
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/students/' + id + '/projects' +
      queryParam, options).map(response => response.json());
  }

  public getStudentReportsData(year, page, resultsPerPage) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/reports?year=' + year +
      '&page=' + page + '&results_per_page=' + resultsPerPage, options).map(response => response.json());
  }

  public getUserNotes(id) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.get(this.host + '/users/' + id + '/notes', options)
      .map(response => response.json());
  }

  public newUserNote(note) {
    if (!this.token) {
      this.fetchToken();
    }
    const body = {
      user_id: note.user.id,
      note_taking_user: note.noteTakingUser.id,
      title: note.title,
      content: note.content,
      priority: note.priority
    };
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/users/notes', body, options)
      .map(response => response.json());
  }

  public newCampus(campus) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/campuses', campus, options)
      .map(response => response.json());
  }

  public newCohort(cohort) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/cohorts', cohort, options)
      .map(response => response.json());
  }

  public newCurriculum(curriculum) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/curriculum', curriculum, options)
      .map(response => response.json());
  }

  public newModule(module) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/modules', module, options)
      .map(response => response.json());
  }

  public newProject(project) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/projects', project, options)
      .map(response => response.json());
  }

  public newProjectInstance(project, projectInstance) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + '/projects/' + project.id + '/instances', projectInstance, options)
      .map(response => response.json());
  }

  public updateUserNote(note) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.patch(this.host + '/users/' + note.noteTakingUser +
      '/notes/' + note.id, note, options).map(response => response.json());
  }

  public decideBootcamper(bootcampId, bootcamperId, type) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.patch(this.host + '/bootcampers/' + bootcamperId + '/decisions' , {type: type, bootcampId: bootcampId}, options).map(response => response.json());
  }

  public updateUserBootcamp(bootcampId, bootcamperId, value) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.patch(this.host + '/users/' + bootcamperId + '/bootcamps/' + bootcampId, value, options).map(response => response.json());
  }

  public uploadCSV(url, object) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(this.host + url , object, options)
      .map(response => response.json());
  }

  public finalizeBootcamp(bootcampId) {
    if (!this.token) {
      this.fetchToken();
    }
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
    const options = new RequestOptions({ headers: headers });
    return this._http.patch(this.host + '/bootcamps/' + bootcampId + '/finalize',{  }, options).map(response => response.json());
  }
}
