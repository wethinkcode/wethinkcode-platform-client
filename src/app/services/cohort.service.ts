import { Injectable } from '@angular/core';

import { Cohort } from '../models/cohort';
import {APIService} from "./api.service";

@Injectable()
export class CohortService {
  cohorts: Array<Cohort> = [];
  selectedCohort: Cohort;

  constructor(
    private _APIService: APIService
  ) { }

  getCohorts(page, limit) {
    return this._APIService.getCohorts(page, limit);
  }

  getCohort(cohortId) {
    return this._APIService.getCohort(cohortId);
  }

}
