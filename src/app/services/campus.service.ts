import { Injectable } from '@angular/core';

import { APIService } from './api.service';

import { Campus } from '../models/campus';

@Injectable()
export class CampusService {
  loadingCampuses: Boolean = false;
  limit = 20;
  page = 1;
  campuses: Array<Campus>;

  constructor(
    private _APIService: APIService
  ) {
    this.campuses = [];
  }

  newCampus(campus) {
    return this._APIService.newCampus(campus);
  }

  getMoreCampuses() {
    return this._APIService.getCampuses(this.page, this.limit);
  }

  getAllCampuses() {
    this.loadingCampuses = true;
    this.limit = 100;
    return new Promise((resolve, reject) => {
      this.getMoreCampuses().subscribe(res => {
        this.loadingCampuses = false;
        this.limit = 20;
        if (res.status !== 'success') {
          reject(res);
        } else {
          for (const campus of res.data) {
            this.campuses.push(campus);
          }
          resolve(res);
        }
      });
    });
  }
}
