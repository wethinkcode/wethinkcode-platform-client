import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { APIService } from './api.service';

@Injectable()
export class ToolService {
  loading: Boolean = true;
  administratorTools: Array<any> = [];
  staffTools: Array<any> = [];
  corporateSponsorTools: Array<any> = [];
  studentTools: Array<any> = [];
  applicantTools: Array<any> = [];
  globalTools: Array<any> = [];
  allTools: Array<any> = [];

  constructor(
    public authenticationService: AuthenticationService,
    private _APIService: APIService
  ) { }

  getUserTools() {
    this.authenticationService.getUserId().then(userId => {
      this._APIService.getUserTools(userId).subscribe(res => {
        this.loading = false;
        this.globalTools = [];
        this.administratorTools = [];
        this.corporateSponsorTools = [];
        this.staffTools = [];
        this.studentTools = [];
        this.applicantTools = [];
        if (res.code === 'S0002') {
          for (const tool of res.data) {
            tool.name = tool.name.replace(/ /g, '\n');
            switch (tool.type) {
              case 'global': this.globalTools.push(tool); break;
              case 'administrator': this.administratorTools.push(tool); break;
              case 'corporate_sponsor': this.corporateSponsorTools.push(tool); break;
              case 'staff': this.staffTools.push(tool); break;
              case 'student': this.studentTools.push(tool); break;
              case 'applicant': this.applicantTools.push(tool); break;
            }
          }
        }
      });
    });
  }

  getAllTools() {
    return this._APIService.getAllTools();
  }

}
