import { Injectable } from '@angular/core';

import { User } from '../models/user';

import { APIService } from './api.service';

@Injectable()
export class StaffService {
  limit: Number = 20;
  page: any = 1;
  staff: Array<User> = [];
  bootcampStaffPage: any = 1;

  constructor(
    private _APIService: APIService
  ) {
  }

  subscribeToBootcamp(bootcampId, user) {
    return this._APIService.subscribeUserToBootcamp(bootcampId, user, 'staff');
  }

  unsubscribeFromBootcamp(bootcampId, user) {
    return this._APIService.unsubscribeUserFromBootcamp(bootcampId, user, 'staff');
  }

  getBootcampStaff(bootcampId) {
    return this._APIService.getBootcampStaff(this.bootcampStaffPage, this.limit, bootcampId);
  }

  getMoreStaff() {
    return this._APIService.getStaff(this.page, this.limit);
  }

}
