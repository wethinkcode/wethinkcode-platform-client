import { Injectable } from '@angular/core';

import { Module } from '../models/module';

import { APIService } from './api.service';

@Injectable()
export class ModulesService {
  page = 1;
  limit = 20;
  modules: Array<Module> = [];

  constructor(
    private _APIService: APIService
  ) { }

  getMoreModules() {
    return this._APIService.getModules(this.page, this.limit);
  }

  newModule(module) {
    return this._APIService.newModule(module);
  }


}
