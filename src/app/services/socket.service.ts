import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { APIService } from './api.service';
import { NotificationService } from './notification.service';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class SocketService {
  private socket;

  constructor(
    private _APIService: APIService,
    private _notificationService: NotificationService,
    private _authenticationService: AuthenticationService
    ) {
    this.socket = io('https://wethinkcode-autobot-production.herokuapp.com');
    this.socket.on('connection-established', data => {
    });
    this.socket.on('process-started', data => {
      this._authenticationService.getUserId().then(id => {
        if (data.user && data.user.id === id) {
          this._notificationService.newNotification({type: 'once-off', content: data.msg});
        }
      });
    });
    this.socket.on('process-completed', data => {
      this._authenticationService.getUserId().then(id => {
        if (data.user && data.user.id === id) {
          this._notificationService.newNotification({type: 'once-off', content: data.msg});
        }
      });
    });
  }

}
