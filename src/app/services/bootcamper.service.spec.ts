import { TestBed, inject } from '@angular/core/testing';

import { BootcamperService } from './bootcamper.service';

describe('BootcamperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BootcamperService]
    });
  });

  it('should be created', inject([BootcamperService], (service: BootcamperService) => {
    expect(service).toBeTruthy();
  }));
});
