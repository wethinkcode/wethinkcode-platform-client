import { Injectable } from '@angular/core';
import {APIService} from "./api.service";

@Injectable()
export class FileService {

  constructor(
    private _APIService: APIService
  ) { }

  uploadCSV(url, object) {
    return this._APIService.uploadCSV(url, object);
  }

}
