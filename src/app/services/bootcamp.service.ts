import { Injectable } from '@angular/core';

import { Bootcamp } from '../models/bootcamp';

import { APIService } from './api.service';

@Injectable()
export class BootcampService {
  bootcamps: Array<Bootcamp> = [];
  selectedBootcamp: Bootcamp;

  constructor(
    private _APIService: APIService,
  ) { }

  getBootcamps(page, limit) {
    return this._APIService.getBootcamps(page, limit);
  }

  newBootcamp(bootcamp) {
    return this._APIService.newBootcamp(bootcamp);
  }

  getBootcamp(bootcampId) {
    return this._APIService.getBootcamp(bootcampId);
  }

  getMoreUsers(page, limit) {
    return this._APIService.getBootcampUsers(this.selectedBootcamp.id, page, limit);
  }

  decideBootcamper(bootcamperId, type) {
    return this._APIService.decideBootcamper(this.selectedBootcamp.id, bootcamperId, type);
  }

  updateUserBootcamp(bootcamperId, value) {
    return this._APIService.updateUserBootcamp(this.selectedBootcamp.id, bootcamperId, value);
  }

  finalizeBootcamp(bootcampId) {
    return this._APIService.finalizeBootcamp(bootcampId);
  }
}
