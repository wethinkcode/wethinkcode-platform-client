import { Injectable } from '@angular/core';
import { Milestone } from '../models/milestone';

import { APIService } from './api.service';

@Injectable()
export class MilestoneService {
  page = 1;
  limit = 20;
  milestones: Array<Milestone>;

  constructor(
    private _APIService: APIService
  ) { }

  getMoreMilestones() {
    return this._APIService.getMilestones(this.page, this.limit);
  }
}
