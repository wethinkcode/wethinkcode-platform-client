import { Injectable } from '@angular/core';

import { APIService } from './api.service';

import { Note } from '../models/note';

@Injectable()
export class NoteService {
  activeNote: Note;
  notes: Array<Note> = [];

  constructor(
    private _APIService: APIService
  ) {
    this.activeNote = new Note('', new Date());
  }

  getUserNotes(id) {
    return this._APIService.getUserNotes(id);
  }

  newUserNote() {
    return this._APIService.newUserNote(this.activeNote);
  }

  updateUserNote() {
    return this._APIService.updateUserNote(this.activeNote);
  }

}
