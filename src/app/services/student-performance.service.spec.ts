import { TestBed, inject } from '@angular/core/testing';

import { StudentPerformanceService } from './student-performance.service';

describe('StudentPerformanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentPerformanceService]
    });
  });

  it('should be created', inject([StudentPerformanceService], (service: StudentPerformanceService) => {
    expect(service).toBeTruthy();
  }));
});
