import { Injectable } from '@angular/core';
import {APIService} from './api.service';

@Injectable()
export class BootcamperService {

  constructor(
    private _APIService: APIService
  ) { }

  getBootcamperProjects(bootcamperId, page, limit) {
    return this._APIService.getBootcamperProjects(bootcamperId, page, limit);
  }

}
