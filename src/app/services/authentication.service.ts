import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import { APIService } from './api.service';

@Injectable()
export class AuthenticationService {
  isSignedIn$: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  role: any;
  id: Number;

  constructor(
    private _router: Router,
    private _APIService: APIService,
  ) {}

  public invalidateUser() {
    localStorage.clear();
    setTimeout(() => {this._router.navigate(['/signin'], { queryParams: { invalid: '1', ref: this._router.url} }); }, 3000);
  }

  getUserId() {
    return new Promise((resolve, reject) => {
      if (this.id) {
        resolve(this.id);
      } else {
        if (localStorage.getItem('token')) {
          this._APIService.validateToken().subscribe(res => {
            if (res.code === 'S0002') {
              resolve(this.id);
            } else {
              reject();
            }
          });
        } else {
          this.invalidateUser();
          reject();
        }
      }
    });
  }

}
