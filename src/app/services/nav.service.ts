import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { APIService } from './api.service';
import { AuthenticationService } from './authentication.service';
import { NotificationService } from './notification.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class NavService {
  menuToggled;
  dashboardMenuToggled: Boolean = false;
  dashboardLoading: BehaviorSubject<Boolean>;
  dashboardStatus: String;
  quickLinksMenuToggled: Boolean = false;
  screenSide: String = '';

  notesSliderToggled: Boolean = false;
  editUserInformationSliderToggled: Boolean = false;
  isNewCampusSliderToggled: Boolean = false;
  isNewCurriculumSliderToggled: Boolean = false;
  isNewModuleSliderToggled: Boolean = false;
  isNewProjectSliderToggled: Boolean = false;
  isNewProjectInstanceSliderToggled: Boolean = false;
  isNewUserSliderToggled: Boolean = false;
  isNewBootcampSliderToggled: Boolean = false;
  isNewCohortSliderToggled: Boolean = false;
  isManageStaffVotersSliderToggled: Boolean = false;
  isFinalizeBootcampSliderToggled: Boolean = false;

  isSequenceToggled: Boolean = true;

  isDashboardPage: Boolean = false;
  isDashboardNavSticky: Boolean = false;
  isPerformanceReportTableSticky: Boolean = false;

  searchInput: String = '';
  searchFocused: Boolean = false;

  pages = [
    { title: 'WeThinkCode_ | Sign Up', url: '/signup', name: 'Signup'},
    { title: 'WeThinkCode_ | Sign In', url: '/signin', name: 'Signin'},
  ];

  constructor(
    private _router: Router,
    private _APIService: APIService,
    private _authenticationService: AuthenticationService,
    private _notificationService: NotificationService
  ) {
    this.dashboardLoading = new BehaviorSubject(true);
  }

  getUserQuickLinks() {
    return this._APIService.getUserQuickLinks(this._authenticationService.id);
  }

  addQuickLink(userId, toolId) {
    return this._APIService.newUserQuicklink(userId, toolId);
  }

  deleteQuickLink(userId, toolId) {
    return this._APIService.deleteUserQuickLink(userId, toolId);
  }

  redirectUser(url, delay, isRef) {
    if (isRef) {
      setTimeout(() => {
        this._router.navigate([url], {
          queryParams: {
            ref: this._router.url
          }
        });
      }, delay);
    } else {
      setTimeout(() => {
        this._router.navigate([url]);
      }, delay);
    }
  }

  toggleDashboardMenu() {
    this.dashboardMenuToggled = !this.dashboardMenuToggled;
  }

  toggleQuickLinksMenu() {
    this.quickLinksMenuToggled = !this.quickLinksMenuToggled;
  }

}
