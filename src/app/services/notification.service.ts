import { Injectable } from '@angular/core';
import { Notification } from '../models/notification';

@Injectable()
export class NotificationService {
  splicePosition = 0;
  notifications: Array<Notification> = [];

  constructor() { }

  newNotification(notification: any) {
    if (notification.type === 'once-off') {
      const newNotification = new Notification(notification.content);
      this.notifications.push(newNotification);
      newNotification.position = this.splicePosition;
      this.splicePosition++;
      setTimeout(function(){
        this.notifications.splice(newNotification.position, 1);
        for (let i = newNotification.position; i < this.notifications.length; i++) {
          this.notifications[i].position--;
        }
        this.splicePosition--;
      }.bind(this), 4900);
    }
  }
}
