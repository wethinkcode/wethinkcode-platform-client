import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { APIService } from '../services/api.service';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    public apiService: APIService,
    public authenticationService: AuthenticationService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('token')) {
      this.apiService.validateToken().subscribe(res => {
        if (res.code !== 'S0002') {
          this.authenticationService.invalidateUser();
        } else {
          this.authenticationService.role = res.data.token.role;
          this.authenticationService.id = res.data.token.id;
        }
      });
      return true;
    } else {
      this._router.navigate(['/signin'], { queryParams: { invalid: '1' }});
      return false;
    }
  }
}
