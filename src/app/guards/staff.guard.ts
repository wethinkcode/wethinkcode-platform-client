import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { APIService } from '../services/api.service';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class StaffGuard implements CanActivate {
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _APIService: APIService,
    public authenticationService: AuthenticationService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('token') || !this.authenticationService.role) {
      this._APIService.validateToken().subscribe(res => {
        if ((res.code !== 'S0002')) {
          this.authenticationService.invalidateUser();
          return false;
        } else {
          this.authenticationService.role = res.data.token.role;
          if (this.authenticationService.role !== 'staff' && this.authenticationService.role !== 'administrator') {
            this._router.navigate(['/page-not-found']);
            return false;
          }
        }
      });
      return true;
    }
  }
}
