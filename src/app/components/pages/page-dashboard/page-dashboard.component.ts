import {Component, HostListener, OnInit} from '@angular/core';
import { NavService } from '../../../services/nav.service';
import { UserService } from '../../../services/user.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { SocketService } from "../../../services/socket.service";

@Component({
  selector: 'app-page-dashboard',
  templateUrl: './page-dashboard.component.html',
  styleUrls: ['./page-dashboard.component.scss']
})
export class PageDashboardComponent implements OnInit {
  triggeredDashboardNav = false;
  triggeredPerformanceReportTableSticky = false;

  constructor(
    public navService: NavService,
    public userService: UserService,
    public authenticationService: AuthenticationService,
    public socketService: SocketService
  ) {
    this.navService.isDashboardPage = true;
  }

  ngOnInit() {
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    const studentPerformanceReportTable = document.getElementById('student-reports-table');
    if (studentPerformanceReportTable && window.pageYOffset >= studentPerformanceReportTable.getBoundingClientRect().top && !this.triggeredPerformanceReportTableSticky) {
      this.navService.isPerformanceReportTableSticky = true;
    } else if (studentPerformanceReportTable && window.pageYOffset < studentPerformanceReportTable.getBoundingClientRect().top && !this.triggeredPerformanceReportTableSticky) {
      this.navService.isPerformanceReportTableSticky = false;
    }
    if (window.pageYOffset > 1 && !this.triggeredDashboardNav) {
      this.triggeredDashboardNav = true;
      this.navService.isDashboardNavSticky = true;
    } else if (window.pageYOffset <= 1 && this.triggeredDashboardNav) {
      this.triggeredDashboardNav = false;
      this.navService.isDashboardNavSticky = false;
    }
  }
}
