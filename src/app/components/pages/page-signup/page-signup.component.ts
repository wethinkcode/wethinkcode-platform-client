import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '../../../services/user.service';
import { NavService } from '../../../services/nav.service';

import { Error } from '../../../models/error';
import { User } from '../../../models/user';

@Component({
  selector: 'app-page-signup',
  templateUrl: './page-signup.component.html',
  styleUrls: ['./page-signup.component.scss']
})
export class PageSignupComponent implements OnInit {
  error: Error;
  user: User;
  loading: Boolean;
  failure: Boolean;
  success: Boolean;
  successMessage: String;
  isActivation: Boolean;

  constructor(
    public userService: UserService,
    public navService: NavService,
    private _activatedRoute: ActivatedRoute
  ) {
    this.error = new Error('', '', '', 10);
    this.user = new User({});
    this.navService.isDashboardPage = false;
  }

  ngOnInit() {
    const params = this._activatedRoute.snapshot.queryParams;
    if (params['activate'] === 'true' && params['token'] && params['id']) {
      this.loading = true;
      this.isActivation = true;
      this.userService.userActivation(params['id'], params['token']).subscribe(res => {
        this.loading = false;
        if (res.code === 'S0008') {
          this.successMessage = 'Your account has been activated successfully. Please sign in to continue.';
          this.success = true;
        } else {
          this.failure = true;
          this.error = new Error(res.code, res.message, 'signup', 9);
        }
      });
    }

  }

  keyhook(key) {
    if (key.keyCode === 13) {
      this.submitSignup();
    }
  }

  clearError() {
    this.error = new Error('', '', '', 10);
  }

  submitSignup() {
    if (!this.user.firstName) {
      this.error = new Error('C0001', 'Your first name is required.', 'signup', 9);
    } else if (!this.user.lastName) {
      this.error = new Error('C0002', 'Your last name is required.', 'signup', 9);
    } else if (!this.user.email) {
      this.error = new Error('C0003', 'Your email address is required.', 'signup', 9);
    } else if (!this.userService.validateEmail(this.user.email)) {
      this.error = new Error('C0004', 'You have entered an invalid email address.', 'signup', 9);
    } else if (!this.user.password) {
      this.error = new Error('C0005', 'A new password is required.', 'signup', 9);
    } else if (this.user.password.length <= 6) {
      this.error = new Error('C0006', 'Your password should contain at least 6 characters.', 'signup', 9);
    } else if (!this.user.passwordRepeat || this.user.passwordRepeat !== this.user.password) {
      this.error = new Error('C0007', 'Your passwords do not match.', 'signup', 9);
    } else {
      this.loading = true;
      this.userService.userSignup(this.user).subscribe( res => {
        this.loading = false;
        if (res.code !== 'S0010') {
          this.failure = true;
          this.error = new Error(res.code, res.message, 'signup', 9);
        } else {
          this.success = true;
          this.successMessage = 'Account created successfully. Check your email inbox to continue.';
        }
      });
    }
  }

}
