import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { UserService } from '../../../services/user.service';

import { Error } from '../../../models/error';
import { User } from '../../../models/user';
import {NavService} from "../../../services/nav.service";
import {AuthenticationService} from "../../../services/authentication.service";

@Component({
  selector: 'app-page-signin',
  templateUrl: './page-signin.component.html',
  styleUrls: ['./page-signin.component.scss']
})
export class PageSigninComponent implements OnInit {
  error: Error;
  user: User;
  loading: Boolean;
  failure: Boolean;
  success: Boolean;
  successMessage: String;
  isResetInit: Boolean;
  isReset: Boolean;
  isSetPassword: Boolean;

  constructor(
    public userService: UserService,
    public navService: NavService,
    public authenticationService: AuthenticationService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.error = new Error('', '', '', 10);
    this.user = new User({});
    this.navService.isDashboardPage = false;
  }

  keyhook(key) {
    if (key.keyCode === 13) {
      this.submitSignin();
    }
  }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.failure = true;
      this.error = new Error('', 'You\'re already signed in. Redirecting you ro your dashboard.', 'signin', 9);
      setTimeout(() => {this._router.navigate(['/dashboard']); }, 3000);
    }
    const params = this._activatedRoute.snapshot.queryParams;
    if (params['reset-init'] === 'true') {
      this.isResetInit = true;
    }
    if (params['reset'] === 'true' && params['token'] && params['id']) {
      this.loading = true;
      this.userService.userCheckReset(params['id'], params['token']).subscribe(res => {
        this.loading = false;
        if (res.code === 'S0005') {
          this.isReset = true;
        } else {
          this.failure = true;
          this.error = new Error(res.code, res.message, 'signup', 9);
        }
      });
    }
  }

  clearError() {
    this.error = new Error('', '', '', 10);
  }

  submitSignin() {
    if (!this.user.identifier) {
      this.error = new Error('C0001', 'Your username/email address is required.', 'signin', 9);
    } else if (!this.user.password) {
      this.error = new Error('C0002', 'A password is required.', 'signup', 9);
    } else {
      this.loading = true;
      this.userService.userSignin(this.user).subscribe(res => {
        this.loading = false;
        if (res.code !== 'S0008') {
          this.failure = true;
          this.error = new Error(res.code, res.message, 'signin', 9);
        } else {
          this.success = true;
          this.successMessage = 'Signin success. We\'re redirecting you to your dashboard.';
          localStorage.setItem('token', res.data.token);
          this.navService.redirectUser('dashboard', 3000, false);
        }
      });
    }
  }

  resetInitSubmit() {
    if (!this.user.identifier) {
      this.error = new Error('C0001', 'Your username/email address is required.', 'signin', 9);
    } else {
      this.loading = true;
      this.userService.userResetInit(this.user).subscribe( res => {
        this.loading = false;
        if (res.code !== 'S0007') {
          this.failure = true;
          this.error = new Error(res.code, res.message, 'signup', 9);
        } else {
          this.success = true;
          this.successMessage = res.message;
        }
      });
    }
  }

  resetSubmit() {
    const params = this._activatedRoute.snapshot.queryParams;
    if (!this.user.password) {
      this.error = new Error('C0004', 'A new password is required.', 'signup', 9);
    } else if (this.user.password.length <= 6) {
      this.error = new Error('C0005', 'Your password should contain at least 6 characters.', 'signup', 9);
    } else if (!this.user.passwordRepeat || this.user.passwordRepeat !== this.user.password) {
      this.error = new Error('C0006', 'Your passwords do not match.', 'signup', 9);
    } else {
      this.loading = true;
      this.userService.userReset(this.user, params['id'], params['token']).subscribe( res => {
        this.loading = false;
        if (res.code !== 'S0010') {
          this.failure = true;
          this.error = new Error(res.code, res.message, 'signup', 9);
        } else {
          this.success = true;
          this.successMessage = res.message;
        }
      });
    }
  }

}
