import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementEditUserInformationComponent } from './element-edit-user-information.component';

describe('ElementEditUserInformationComponent', () => {
  let component: ElementEditUserInformationComponent;
  let fixture: ComponentFixture<ElementEditUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementEditUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementEditUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
