import { Component, OnInit } from '@angular/core';
import { ElementSlider } from '../../../abstract-classes/element-slider';
import { Router } from '@angular/router';

import { Cohort } from '../../../models/cohort';
import { Campus } from '../../../models/campus';
import { Error } from '../../../models/error';

import { CampusService } from '../../../services/campus.service';
import { NotificationService } from '../../../services/notification.service';
import { NavService } from '../../../services/nav.service';
import { APIService } from '../../../services/api.service';
import { CohortService } from '../../../services/cohort.service';

@Component({
  selector: 'app-element-slider-new-cohort',
  templateUrl: './element-slider-new-cohort.component.html',
  styleUrls: ['./element-slider-new-cohort.component.scss']
})
export class ElementSliderNewCohortComponent extends ElementSlider implements OnInit {
  cohort: Cohort = new Cohort({});

  constructor(
    private _router: Router,
    private _notificationService: NotificationService,
    private _APIService: APIService,
    public campusService: CampusService,
    public navService: NavService,
    public cohortService: CohortService
  ) {
    super();

    this.cohort.campus = new Campus({id: -1});
  }

  ngOnInit() {
    this._router.events.subscribe(event => {
      this.campusService.page = 1;
      this.campusService.campuses = [];
    });
    this.campusService.getAllCampuses().catch(err => {
      this.failure = true;
      this.resultMessage = 'We could not fetch the milestones you requested';
      this._notificationService.newNotification({type: 'once-off', content: this.resultMessage, classification: 'error' });
    });
  }

  setCampus(event) {
    for (const campus of this.campusService.campuses) {
      if (campus.id === parseInt(event)) {
        this.cohort.campus = campus;
      }
    }
  }

  setStartDate(event) {
    this.cohort.startDate = event;
  }

  resetCohort() {
    this.navService.isNewCohortSliderToggled = false;
    this.loading = true;
    setTimeout(() => {
      this.cohort = new Cohort({});
      this.cohort.campus = new Campus({id: -1});
      this.resultMessage = '';
      this.success = false;
      this.failure = false;
      this.loading = false;
    }, 1000);
  }

  saveCohort() {
    if (!this.cohort.title) {
      this.error = new Error('C0001', 'Cohorts require a title.', 'Cohorts - New cohort slider', 9);
    } else if (this.cohort.campus.id === -1) {
      this.error = new Error('C0002', 'Cohorts require a campus.', 'Cohorts - New cohort slider', 9);
    } else if (!this.cohort.startDate) {
      this.error = new Error('C0003', 'Cohorts require a start date.', 'Cohorts - New cohort slider', 9);
    } else {
      this.loading = true;
      this._APIService.newCohort(this.cohort).subscribe(res => {
        this.loading = false;
        if (res.status !== 'success') {
          this.failure = true;
        } else {
          this.cohort.id = res.data.id;
          this.cohortService.cohorts.push(this.cohort);
          this.success = true;
        }
        this.resultMessage = res.message;
      });
    }
  }


}
