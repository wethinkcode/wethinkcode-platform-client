import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewCohortComponent } from './element-slider-new-cohort.component';

describe('ElementSliderNewCohortComponent', () => {
  let component: ElementSliderNewCohortComponent;
  let fixture: ComponentFixture<ElementSliderNewCohortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewCohortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewCohortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
