import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentPerformanceInternship2Component } from './element-student-performance-internship2.component';

describe('ElementStudentPerformanceInternship2Component', () => {
  let component: ElementStudentPerformanceInternship2Component;
  let fixture: ComponentFixture<ElementStudentPerformanceInternship2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentPerformanceInternship2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentPerformanceInternship2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
