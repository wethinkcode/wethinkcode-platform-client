import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-element-student-performance-internship2',
  templateUrl: './element-student-performance-internship2.component.html',
  styleUrls: ['./element-student-performance-internship2.component.scss']
})
export class ElementStudentPerformanceInternship2Component implements OnInit {
  @Input() student: any;
  studentInternship2Data = null;

  constructor() { }

  ngOnInit() {
    this.student.event.subscribe(event => {
      if (event === 'getInternship2Data' && this.studentInternship2Data === null) {
        this.getStudentInternship2Data();
      }
    });
  }

  getStudentInternship2Data() {

  }
}
