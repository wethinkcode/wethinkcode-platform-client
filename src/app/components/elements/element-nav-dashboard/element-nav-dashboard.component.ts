import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NavService } from '../../../services/nav.service';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-element-nav-dashboard',
  templateUrl: './element-nav-dashboard.component.html',
  styleUrls: ['./element-nav-dashboard.component.scss']
})
export class ElementNavDashboardComponent implements OnInit {

  constructor(
    private _elementRef: ElementRef,
    public navService: NavService,
    public authenticationService: AuthenticationService,
    private _activatedRoute: ActivatedRoute
  ) {
  }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.navService.searchFocused = false;
    }
  }

  ngOnInit() {
    this._activatedRoute.queryParams
      .subscribe(params => {
        if (params.search) {
          this.navService.searchInput = decodeURI(params.search);
        }
      });
  }

  toggleMenu() {
    this.navService.dashboardMenuToggled = !this.navService.dashboardMenuToggled;
  }

}
