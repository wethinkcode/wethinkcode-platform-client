import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementProjectInstancesComponent } from './element-project-instances.component';

describe('ElementProjectInstancesComponent', () => {
  let component: ElementProjectInstancesComponent;
  let fixture: ComponentFixture<ElementProjectInstancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementProjectInstancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementProjectInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
