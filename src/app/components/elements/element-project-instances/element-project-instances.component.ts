import {Component, Input, OnInit} from '@angular/core';

import { Project } from '../../../models/project';

import { NavService } from '../../../services/nav.service';
import {ProjectService} from "../../../services/project.service";
import {ProjectInstance} from "../../../models/project-instance";
import {Router} from "@angular/router";

@Component({
  selector: 'app-element-project-instances',
  templateUrl: './element-project-instances.component.html',
  styleUrls: ['./element-project-instances.component.scss']
})
export class ElementProjectInstancesComponent implements OnInit {
  @Input() project: Project;
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: String = '';
  limit: Number = 20;
  page: Number = 1;

  constructor(
    public navService: NavService,
    public projectService: ProjectService,
    private _router: Router
  ) { }

  ngOnInit() {
    this._router.events.subscribe(event => {
      this.projectService.projectInstances = [];
    });
    this.projectService.getMoreProjectInstances(this.projectService.selectedProject.id, this.page, this.limit).subscribe( res => {
      this.loading = false;
      if (res.code !== 'S0003') {
        this.componentFailure = true;
      } else {
        if (res.data) {
          for (const instance of res.data) {
            const newInstance = new ProjectInstance(
              {
                title: instance.title,
                projectId: instance.project_id,
                id: instance.id,
                validationGrade: instance.validation_grade,
                startDate: instance.start_date,
                endDate: instance.end_date}
              );
            this.projectService.projectInstances.push(newInstance);
          }
        }
      }
      this.resultMessage = res.message;
    });
  }

}
