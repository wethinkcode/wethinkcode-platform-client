import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-element-sequence',
  templateUrl: './element-sequence.component.html',
  styleUrls: ['./element-sequence.component.scss']
})
export class ElementSequenceComponent implements OnInit {
  @Input() steps: any;
  @Output() updated: EventEmitter<any> = new EventEmitter();
  toggled: Boolean = true;

  constructor(
    public navService: NavService
  ) {
  }

  ngOnInit() {
  }

  changeStep(newStep) {
    for (const step of this.steps) {
      if (step.title === newStep.title && !step.is_current) {
        this.updated.emit(newStep);
        step.is_current = true;
      } else {
        step.is_current = false;
      }
    }
  }
}
