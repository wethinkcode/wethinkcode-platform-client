import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSequenceComponent } from './element-sequence.component';

describe('ElementSequenceComponent', () => {
  let component: ElementSequenceComponent;
  let fixture: ComponentFixture<ElementSequenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSequenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
