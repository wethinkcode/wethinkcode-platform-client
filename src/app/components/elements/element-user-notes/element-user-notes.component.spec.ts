import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementUserNotesComponent } from './element-user-notes.component';

describe('ElementUserNotesComponent', () => {
  let component: ElementUserNotesComponent;
  let fixture: ComponentFixture<ElementUserNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementUserNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementUserNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
