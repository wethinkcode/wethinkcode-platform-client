import {Component, Input, OnInit} from '@angular/core';

import { User } from '../../../models/user';
import { Note } from '../../../models/note';

import { UserService } from '../../../services/user.service';
import {AuthenticationService} from '../../../services/authentication.service';
import { NotificationService } from '../../../services/notification.service';
import { NavService } from '../../../services/nav.service';
import { NoteService } from '../../../services/note.service';

@Component({
  selector: 'app-element-user-notes',
  templateUrl: './element-user-notes.component.html',
  styleUrls: ['./element-user-notes.component.scss']
})
export class ElementUserNotesComponent implements OnInit {
  @Input() user;
  loading: Boolean = true;

  componentFailure: Boolean = false;
  componentFailureMessage: String = '';


  constructor(
    private _userService: UserService,
    private _authenticationService: AuthenticationService,
    private _notificationService: NotificationService,
    public notesService: NoteService,
    public navService: NavService
  ) { }

  ngOnInit() {
    if (this._userService.selectedUser.id) {
      this.notesService.getUserNotes(this._userService.selectedUser.id).subscribe(res => {
        this.loading = false;
        if (res.code === 'S0004') {
          for (const note of res.data) {
            const newNote = new Note(note.content, new Date(note.date));
            newNote.priority = note.priority;
            newNote.id = note.id;
            newNote.noteTakingUser.firstName = note.note_taking_user_first_name;
            newNote.noteTakingUser.lastName = note.note_taking_user_last_name;
            newNote.noteTakingUser.username = note.note_taking_user_username;
            newNote.noteTakingUser.profileImage = note.note_taking_user_profile_image;
            newNote.title = note.title;
            this.notesService.notes.push(newNote);
          }
        } else {
          this.componentFailure = true;
          this.componentFailureMessage =  'Could not fetch notes. Please try again.';
          this._notificationService.newNotification({type: 'once-off', content: res.message});
        }
      });
    } else {
      this.loading = false;
      this.componentFailure = true;
      this.componentFailureMessage =  'Could not fetch notes. Please try again.';
      this._notificationService.newNotification({type: 'once-off', content: this.componentFailureMessage});
    }
  }

}
