import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementsAttendanceComponent } from './elements-attendance.component';

describe('ElementsAttendanceComponent', () => {
  let component: ElementsAttendanceComponent;
  let fixture: ComponentFixture<ElementsAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementsAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementsAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
