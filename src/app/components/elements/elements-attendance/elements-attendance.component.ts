import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-elements-attendance',
  templateUrl: './elements-attendance.component.html',
  styleUrls: ['./elements-attendance.component.scss']
})
export class ElementsAttendanceComponent implements OnInit {
  @Input() steps: any;

  constructor() { }

  ngOnInit() {
  }

}
