import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentPerformanceYear2Component } from './element-student-performance-year2.component';

describe('ElementStudentPerformanceYear2Component', () => {
  let component: ElementStudentPerformanceYear2Component;
  let fixture: ComponentFixture<ElementStudentPerformanceYear2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentPerformanceYear2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentPerformanceYear2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
