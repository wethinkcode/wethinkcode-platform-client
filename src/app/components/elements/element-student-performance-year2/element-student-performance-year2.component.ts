import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-element-student-performance-year2',
  templateUrl: './element-student-performance-year2.component.html',
  styleUrls: ['./element-student-performance-year2.component.scss']
})
export class ElementStudentPerformanceYear2Component implements OnInit {
  @Input() student: any;
  studentYear2Data = null;

  constructor() { }

  ngOnInit() {
    this.student.event.subscribe(event => {
      if (event === 'getYear2Data' && this.studentYear2Data === null) {
        this.getStudentYear2Data();
      }
    });
  }

  getStudentYear2Data() {

  }
}
