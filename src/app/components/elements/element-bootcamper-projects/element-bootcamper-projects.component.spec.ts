import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementBootcamperProjectsComponent } from './element-bootcamper-projects.component';

describe('ElementBootcamperProjectsComponent', () => {
  let component: ElementBootcamperProjectsComponent;
  let fixture: ComponentFixture<ElementBootcamperProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementBootcamperProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementBootcamperProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
