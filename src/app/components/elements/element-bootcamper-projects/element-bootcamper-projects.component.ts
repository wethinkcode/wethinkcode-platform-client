import {Component, Input, OnInit} from '@angular/core';
import { User } from '../../../models/user';
import { Project } from '../../../models/project';
import { BootcamperService } from '../../../services/bootcamper.service';

@Component({
  selector: 'app-element-bootcamper-projects',
  templateUrl: './element-bootcamper-projects.component.html',
  styleUrls: ['./element-bootcamper-projects.component.scss']
})
export class ElementBootcamperProjectsComponent implements OnInit {
  @Input() bootcamper: User;
  loading: Boolean = true;
  projects: Array<Project> = [];
  componentFailure: Boolean = false;
  resultMessage: String = '';
  page: any = 1;
  limit: Number = 40;

  constructor(
    public bootcamperService: BootcamperService
  ) { }

  ngOnInit() {
    this.bootcamperService.getBootcamperProjects(this.bootcamper.id, this.page, this.limit).subscribe(res => {
      this.loading = false;
      if (res.status !== 'success') {
        this.componentFailure = true;
        this.resultMessage = res.message;
      } else {
        this.page++;
        if (res.data && res.data.length) {
          for (const project of res.data) {
            this.projects.push(new Project(project));
          }
        }
      }
    });
  }

}
