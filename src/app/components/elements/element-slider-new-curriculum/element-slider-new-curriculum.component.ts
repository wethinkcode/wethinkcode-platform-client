import { Component, OnInit } from '@angular/core';

import { Curriculum } from '../../../models/curriculum';

import { NavService } from '../../../services/nav.service';
import { CurriculumService } from '../../../services/curriculum.service';
import { CampusService } from '../../../services/campus.service';
import { NotificationService } from '../../../services/notification.service';

import { Error } from '../../../models/error';
import { Campus } from '../../../models/campus';
import { Router } from '@angular/router';

@Component({
  selector: 'app-element-slider-new-curriculum',
  templateUrl: './element-slider-new-curriculum.component.html',
  styleUrls: ['./element-slider-new-curriculum.component.scss']
})
export class ElementSliderNewCurriculumComponent implements OnInit {
  curriculum: Curriculum;
  campuses: Array<Campus> = [];
  campusesLoading: Boolean = true;
  loading: Boolean = false;
  success: Boolean = false;
  successMessage: String = '';
  failure: Boolean = false;
  failureMessage: String = '';
  error: Error;


  constructor(
    private _notificationService: NotificationService,
    private _router: Router,
    public navService: NavService,
    public campusService: CampusService,
    public curriculumService: CurriculumService,
  ) {
    this.curriculum = new Curriculum({});
    this.error = new Error('', '', '', 10);
  }

  clearError() {
    delete this.error;
  }

  ngOnInit() {
    this._router.events.subscribe(event => {
      this.curriculumService.curriculum = [];
    });
    this.curriculum.campuses.push(new Campus({id: -1}));
    this.getCampuses().catch(error => {
      this.failure = true;
      this.failureMessage = error.message;
      this._notificationService.newNotification({type: 'once-off', content: 'Could not retrieve campuses.', classification: 'error' });
    });
  }

  setCampus(campusId, index) {
    for (const campus of this.campuses) {
      if (campus.id === parseInt(campusId)) {
        this.curriculum.campuses[index] = campus;
      }
    }
  }

  getCampuses() {
    this.loading = true;
    this.campusesLoading = true;
    return new Promise((resolve, reject) => {
      this.campusService.getMoreCampuses().subscribe(result => {
        this.campusesLoading = false;
        this.loading = false;
        if (result.code !== 'S0002') {
          reject(result);
        } else {
          for (const campus of result.data) {
            this.campuses.push(new Campus(campus));
          }
          resolve(result);
        }
      });
    });
  }

  submitNewCurriculum() {
    if (!this.curriculum.title) {
      this.error = new Error('C0001', 'A curriculum title is required.', 'Curriculum - Add curriculum', 9);
    } else {
       this.loading = true;
       this.curriculumService.newCurriculum(this.curriculum).subscribe(res => {
        this.loading = false;
        if (res.code !== 'S0003') {
          this.failure = true;
          this.failureMessage = res.message;
        } else {
          this.curriculumService.curriculum.unshift(this.curriculum);
          this.success = true;
          this.successMessage = 'Campus added successfully.';
        }
      });
    }
  }

  clearNewCurriculum() {
    this.navService.isNewCurriculumSliderToggled = false;
    this.loading = true;
    setTimeout(() => {
      this.curriculum = new Curriculum({});
      this.curriculum.campuses.push(new Campus({id: -1}));
      this.campusesLoading = true;
      this.campuses = [];
      this.getCampuses().catch(error => {
        this.failure = true;
        this.failureMessage = error.message;
        this._notificationService.newNotification({type: 'once-off', content: 'Could not retrieve campuses.', classification: 'error' });
      });
      this.successMessage = '';
      this.success = false;
      this.failureMessage = '';
      this.failure = false;
      this.loading = false;
    }, 1000);
  }

}
