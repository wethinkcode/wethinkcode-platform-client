import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewCurriculumComponent } from './element-slider-new-curriculum.component';

describe('ElementSliderNewCurriculumComponent', () => {
  let component: ElementSliderNewCurriculumComponent;
  let fixture: ComponentFixture<ElementSliderNewCurriculumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewCurriculumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewCurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
