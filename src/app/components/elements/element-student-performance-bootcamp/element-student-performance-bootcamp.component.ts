import {Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-element-student-performance-bootcamp',
  templateUrl: './element-student-performance-bootcamp.component.html',
  styleUrls: ['./element-student-performance-bootcamp.component.scss']
})
export class ElementStudentPerformanceBootcampComponent implements OnInit {
  @Input() student: any;
  studentBootcampData = null;

  constructor() { }

  ngOnInit() {
    this.student.event.subscribe(event => {
      if (event === 'getBootcampData' && this.studentBootcampData === null) {
        this.getStudentBootcampData();
      }
    });
  }

  getStudentBootcampData() {
  }

}
