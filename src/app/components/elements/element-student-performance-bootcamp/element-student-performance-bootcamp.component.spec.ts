import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentPerformanceBootcampComponent } from './element-student-performance-bootcamp.component';

describe('ElementStudentPerformanceBootcampComponent', () => {
  let component: ElementStudentPerformanceBootcampComponent;
  let fixture: ComponentFixture<ElementStudentPerformanceBootcampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentPerformanceBootcampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentPerformanceBootcampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
