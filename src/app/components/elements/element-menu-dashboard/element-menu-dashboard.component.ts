import { Component, OnInit } from '@angular/core';
import { NavService } from '../../../services/nav.service';
import { APIService } from '../../../services/api.service';
import { ToolService } from '../../../services/tool.service';

@Component({
  selector: 'app-element-menu-dashboard',
  templateUrl: './element-menu-dashboard.component.html',
  styleUrls: ['./element-menu-dashboard.component.scss']
})
export class ElementMenuDashboardComponent implements OnInit {

  constructor(
    public navService: NavService,
    public _apiService: APIService,
    public toolService: ToolService,
  ) {
  }

  ngOnInit() {
    this.toolService.loading = true;
    this.toolService.getUserTools();
  }

}
