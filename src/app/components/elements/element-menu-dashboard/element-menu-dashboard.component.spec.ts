import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementMenuDashboardComponent } from './element-menu-dashboard.component';

describe('ElementMenuDashboardComponent', () => {
  let component: ElementMenuDashboardComponent;
  let fixture: ComponentFixture<ElementMenuDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementMenuDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementMenuDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
