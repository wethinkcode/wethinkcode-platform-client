import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementQuickLinksDashboardComponent } from './element-quick-links-dashboard.component';

describe('ElementQuickLinksDashboardComponent', () => {
  let component: ElementQuickLinksDashboardComponent;
  let fixture: ComponentFixture<ElementQuickLinksDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementQuickLinksDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementQuickLinksDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
