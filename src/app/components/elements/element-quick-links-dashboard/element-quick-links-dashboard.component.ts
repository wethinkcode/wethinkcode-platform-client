import {Component, ElementRef, HostListener, OnInit} from '@angular/core';

import { NavService } from '../../../services/nav.service';
import { NotificationService } from '../../../services/notification.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { APIService } from '../../../services/api.service';
import { ToolService } from '../../../services/tool.service';

@Component({
  selector: 'app-element-quick-links-dashboard',
  templateUrl: './element-quick-links-dashboard.component.html',
  styleUrls: ['./element-quick-links-dashboard.component.scss']
})
export class ElementQuickLinksDashboardComponent implements OnInit {
  loading: Boolean = true;
  quickLinks: Array<any> = [];
  componentFailure: Boolean = true;
  resultMessage: String = '';
  error: Boolean = false;

  constructor(
    public navService: NavService,
    public toolService: ToolService,
    private _APIService: APIService,
    private _notificationService: NotificationService,
    private _authenticationService: AuthenticationService,
    private _eref: ElementRef
  ) { }

  isActiveQuickLink(toolName) {
    let isActive = false;
    for (const activeTool of this.quickLinks) {
      if (activeTool.name === toolName) {
        isActive = true;
      }
    }
    return isActive;
  }

  addRemoveQuickLink(tool) {
    if (this.isActiveQuickLink(tool.name)) {
      let i = 0;
      for (const activeTool of this.quickLinks) {
        if (activeTool.name === tool.name) {
          this.quickLinks.splice(i, 1);
          this.deleteQuickLink(tool.id);
        } else {
        }
        i++;
      }
    } else {
      this.quickLinks.push(tool);
      this.newQuickLink(tool.id);
    }
  }

  newQuickLink(toolId) {
    this._authenticationService.getUserId().then(userId => {
      this._APIService.newUserQuicklink(userId, toolId).subscribe(res2 => {
        if (res2.code === 'S0004') {
          this._notificationService.newNotification({
            type: 'once-off',
            content: 'Quick link added Successfully.',
            classification: 'success'
          });
        } else {
          this._notificationService.newNotification({
            type: 'once-off',
            content: 'Could not add quick link.',
            classification: 'error'
          });
        }
      });
    });
  }

  deleteQuickLink(toolId) {
    this._authenticationService.getUserId().then(userId => {
      this._APIService.deleteUserQuickLink(this._authenticationService.id, toolId).subscribe(res2 => {
        if (res2.code === 'S0004') {
          this._notificationService.newNotification({
            type: 'once-off',
            content: 'Quick link removed Successfully.',
            classification: 'success'
          });
        } else {
          this._notificationService.newNotification({
            type: 'once-off',
            content: 'Could not remove quick link.',
            classification: 'error'
          });
        }
      });
    });
  }

  ngOnInit() {
    this.quickLinks = [];
    this._authenticationService.getUserId().then(res => {
      this.navService.getUserQuickLinks().subscribe(res2 => {
        this.loading = false;
        if (res2.code === 'S0003' || res2.code === 'S0006' || res2.code === 'S0002') {
          if (res2.data.length) {
            for (const tool of res2.data) {
              tool.name = tool.name.replace(/ /g, '\n');
              this.quickLinks.push(tool);
            }
          }
        } else {
          this.componentFailure = true;
          this.resultMessage = 'Could not retrieve quick links.';
          this._notificationService.newNotification({type: 'once-off', content: this.resultMessage, classification: 'error' });
        }
      });
    }).catch(error => {
      this.componentFailure = true;
      this.resultMessage = 'Could not retrieve user ID.';
      this._notificationService.newNotification({type: 'once-off', content: this.resultMessage, classification: 'error' });
    });
  }

}
