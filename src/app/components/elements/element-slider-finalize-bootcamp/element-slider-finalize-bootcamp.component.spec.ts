import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderFinalizeBootcampComponent } from './element-slider-finalize-bootcamp.component';

describe('ElementSliderFinalizeBootcampComponent', () => {
  let component: ElementSliderFinalizeBootcampComponent;
  let fixture: ComponentFixture<ElementSliderFinalizeBootcampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderFinalizeBootcampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderFinalizeBootcampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
