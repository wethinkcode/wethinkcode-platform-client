import {Component, Input, OnInit} from '@angular/core';

import { NavService } from '../../../services/nav.service';

import { ElementSlider } from '../../../abstract-classes/element-slider';
import { BootcampService } from '../../../services/bootcamp.service';

@Component({
  selector: 'app-element-slider-finalize-bootcamp',
  templateUrl: './element-slider-finalize-bootcamp.component.html',
  styleUrls: ['./element-slider-finalize-bootcamp.component.scss']
})
export class ElementSliderFinalizeBootcampComponent extends ElementSlider  implements OnInit {

  constructor(
    public navService: NavService,
    public bootcampService: BootcampService
  ) {
    super();
  }

  ngOnInit() {
  }

  resetSlider() {
    this.success = false;
    this.failure = false;
    this.navService.isFinalizeBootcampSliderToggled = false;
  }

  finalizeBootcamp() {
    this.loading = true;
    this.bootcampService.finalizeBootcamp(this.bootcampService.selectedBootcamp.id).subscribe(res => {
      this.loading = false;
      this.resultMessage = res.message;
      if (res.status !== 'success') {
        this.failure = true;
      } else {
        this.success = true;
      }
    });
  }

}
