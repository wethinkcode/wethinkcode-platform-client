import { Component, Input, OnInit } from '@angular/core';
import { StudentService } from '../../../services/student.service';

@Component({
  selector: 'app-element-student-performance-attendance',
  templateUrl: './element-student-performance-attendance.component.html',
  styleUrls: ['./element-student-performance-attendance.component.scss']
})
export class ElementStudentPerformanceAttendanceComponent implements OnInit {
  loading: Boolean = true;
  @Input() student: any;

  monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'];
  currentMonth;
  secondMonth: any;
  thirdMonth: any;
  fourthMonth: any;

  studentAttendanceData: any = {
    chartData: [
      { data: [0, 0, 0, 0], label: 'Attendance'},
    ],
  };

  constructor(private _studentService: StudentService) {
  }

  mod(n, m) {
    return ((n % m) + m) % m;
  }

  ngOnInit() {
    this.currentMonth = new Date().getMonth();
    this.secondMonth = this.mod(this.currentMonth - 1, 12);
    this.thirdMonth = this.mod(this.currentMonth - 2, 12);
    this.fourthMonth = this.mod(this.currentMonth - 3, 12);
    this.studentAttendanceData = {
      chartOptions: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: 31,
              stepSize: 31
            }
          }]
        }
      },
      chartData: [
        { data: [0, 0, 0, 0], label: 'Attendance'},
      ],
      chartLabels: [
        this.monthNames[this.fourthMonth],
        this.monthNames[this.thirdMonth],
        this.monthNames[this.secondMonth],
        'Current Month',
      ],
      chartColors: [{borderColor: ['#016c90'], backgroundColor: ['rgba(1,108,144,0.1)']}]
    };
    if (this.student.initialEvent) {
      this.student.initialEvent.subscribe(event => {
        if (event === 'getAttendanceData') {
          this.getStudentAttendanceData();
        }
      });
    } else if (this.student.event) {
      this.student.event.subscribe(event => {
        if (event === 'getAttendanceData') {
          this.getStudentAttendanceData();
        }
      });
    }
  }

  getStudentAttendanceData() {
    this.loading = true;
    this._studentService.getStudentAttendance(this.student.id).subscribe(res => {
      this.loading = false;
      if (res.code === 'S0003') {
        let currentAttendance = 0;
        let secondAttendance = 0;
        let thirdAttendance = 0;
        let fourthAttendance = 0;
        const chartData = [];
        if (res.data) {
          for (const attendance of res.data) {
            switch (new Date(attendance.access_date).getMonth()) {
              case this.currentMonth:
                currentAttendance++;
                break;
              case this.secondMonth:
                secondAttendance++;
                break;
              case this.thirdMonth:
                thirdAttendance++;
                break;
              case this.fourthMonth:
                fourthAttendance++;
                break;
              default:
                break;
            }
          }
          chartData.push(fourthAttendance);
          chartData.push(thirdAttendance);
          chartData.push(secondAttendance);
          chartData.push(currentAttendance);
          this.studentAttendanceData.chartData = chartData;
        }
      }
    });
  }

}
