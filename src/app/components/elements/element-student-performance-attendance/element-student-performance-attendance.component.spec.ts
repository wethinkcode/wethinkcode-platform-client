import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentPerformanceAttendanceComponent } from './element-student-performance-attendance.component';

describe('ElementStudentPerformanceAttendanceComponent', () => {
  let component: ElementStudentPerformanceAttendanceComponent;
  let fixture: ComponentFixture<ElementStudentPerformanceAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentPerformanceAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentPerformanceAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
