import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentModulesComponent } from './element-student-modules.component';

describe('ElementStudentModulesComponent', () => {
  let component: ElementStudentModulesComponent;
  let fixture: ComponentFixture<ElementStudentModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
