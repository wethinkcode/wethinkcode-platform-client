import {AfterContentInit, Component, Input, OnInit} from '@angular/core';

import { StudentService } from '../../../services/student.service';
import { NotificationService } from '../../../services/notification.service';
import {UserService} from "../../../services/user.service";
import {Module} from "../../../models/module";
import {Project} from "../../../models/project";
import {Cohort} from "../../../models/cohort";

@Component({
  selector: 'app-element-student-modules',
  templateUrl: './element-student-modules.component.html',
  styleUrls: ['./element-student-modules.component.scss']
})
export class ElementStudentModulesComponent implements OnInit, AfterContentInit {
  loading: Boolean = false;
  modules: Array<any> = [];
  componentFailure: Boolean = false;
  componentFailureReason: String = '';

  constructor(
    public userService: UserService,
    private _studentService: StudentService,
    private _notificationService: NotificationService
  ) { }

  ngAfterContentInit() {
  }

  ngOnInit() {
    this.loading = true;
    this._studentService.getStudentData(this.userService.selectedUser.id).subscribe(getStudentDataResult => {
      if (getStudentDataResult.cohort) {
        this.userService.selectedUser.cohort = new Cohort(getStudentDataResult.cohort);
        this.userService.selectedUser.startDate = new Date(getStudentDataResult.cohort.start_date);
      }
      if (getStudentDataResult.code !== 'S0003') {
        this.loading = false;
        this.componentFailure = true;
        this.componentFailureReason = 'Could not get student data.';
        this._notificationService.newNotification( {type: 'once-off', content: this.componentFailureReason});
      } else {
        if (getStudentDataResult.data.level) {
          this.userService.selectedUser.level = getStudentDataResult.data.level;
        }
        if (getStudentDataResult.data.year) {
          this.userService.selectedUser.year = getStudentDataResult.data.year;
        }
        if (getStudentDataResult.data.status) {
          this.userService.selectedUser.status = getStudentDataResult.data.status;
        }
        this._studentService.getModules().subscribe(getModulesResult => {
          this.loading = false;
          if (getModulesResult.code !== 'S0002') {
            this.componentFailure = true;
            this.componentFailureReason = 'Could not get modules.';
            this._notificationService.newNotification( {type: 'once-off', content: this.componentFailureReason});
          } else {
            this.loading = false;
            for (const module of getModulesResult.data) {
              this.userService.selectedUser.modules.push(new Module(module));
            }
            this._studentService.getStudentProjects(this.userService.selectedUser.id, null).subscribe(getStudentProjectsResult => {
              if (getStudentProjectsResult.code !== 'S0003') {
                this.componentFailure = true;
                this.componentFailureReason = 'Could not get projects.';
                this._notificationService.newNotification( {type: 'once-off', content: this.componentFailureReason});
              } else {
                for (const module of this.userService.selectedUser.modules) {
                  module.projects = [];
                  for (const project of getStudentProjectsResult.data) {
                    if (project.module_id === module.id ||
                      (project.slug === 'piscine-php' && module.slug === 'web' && this.userService.selectedUser.year !== 2016) ||
                      (project.slug === 'piscine-cpp' && module.slug === 'cpp')) {
                      module.projects.push(new Project(project));
                    }
                  }
                  this.userService.selectedUser.getModulesValidated(module);
                }
              }
            });
          }
        });
      }
    });
  }

  getModuleValidation() {
    for (const module of this.userService.selectedUser.modules) {
      if (this._studentService.isModuleValidated(this.userService.selectedUser, module, module.projects)) {
        module.isValidated = true;
      }
    }
  }

  getAllProjects() {
    this._studentService.getStudentProjects(this.userService.selectedUser.id, null).subscribe(res => {
      if (res.code === 'S0003') {
        for (const module of this.userService.selectedUser.modules) {
          if (!module.projects) {
            module.projects = [];
            for (const project of res.data) {
              if (project.module_id === module.id) {
                module.projects.push(project);
              }
            }
          }
        }
        this.getModuleValidation();
      }
    });
  }

  getProjects(module) {
    if (!module.projects) {
      module.projectsLoading = true;
      this._studentService.getStudentProjects(this.userService.selectedUser.id, module.id).subscribe(res => {
        module.projectsLoading = false;
        if (res.code !== 'S0003') {
          this.componentFailure = true;
          this.componentFailureReason = 'Could not get projects for ' + module.title;
          this._notificationService.newNotification({type: 'once-off', content: this.componentFailureReason});
        } else {
          module.projects = [];
          for (const project of res.data) {
            module.projects.push(project);
          }
        }
      });
    }
  }
}
