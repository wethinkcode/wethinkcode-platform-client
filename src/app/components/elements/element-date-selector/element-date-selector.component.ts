import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-element-date-selector',
  templateUrl: './element-date-selector.component.html',
  styleUrls: ['./element-date-selector.component.scss']
})
export class ElementDateSelectorComponent implements OnInit {
  loading: Boolean = false;
  isActive: Boolean = false;
  dateString: BehaviorSubject<String> = new BehaviorSubject('');
  @Input() placeholder: String = 'Date';
  @Output() update = new EventEmitter<any>();
  activeDate: Date = new Date();
  selectedDate: Date;
  dateTable: Array<Array<any>> = [];
  interval: any;
  increment: any = 300;
  incrementActivated = true;

  constructor(
    private _elementRef: ElementRef
  ) {
  }

  @HostListener('document:click', ['$event.path'])
  public onGlobalClick(targetElementPath: Array<any>) {
    const elementRefInPath = targetElementPath.find(e => e === this._elementRef.nativeElement);
    if (!elementRefInPath) {
      this.isActive = false;
    }
  }

  alterHour(value) {
      this.interval = setInterval(() => {
        this.activeDate.setHours(this.activeDate.getHours() + value);
      }, this.increment);
  }

  alterMinute(value) {
    this.interval = setInterval(() => {
      this.activeDate.setMinutes(this.activeDate.getMinutes() + value);
    }, this.increment);
  }

  clearInterval() {
    if (this.interval) {
      window.clearInterval(this.interval);
      this.interval = null;
      this.increment = 300;
    }
  }

  ngOnInit() {
    this.buildDateTable();
  }

  daysInMonth(year, month) {
    return new Date(year, month + 1, 0).getDate();
  }

  isToday(day) {
    if (this.activeDate.getMonth() === new Date().getMonth() && day === new Date().getDate()) {
      return true;
    }
    return false;
  }

  alterMonth(type) {
    this.loading = true;
    if (type === 'next') {
      if (this.activeDate.getMonth() !== 11) {
        this.activeDate.setMonth(this.activeDate.getMonth() + 1);
      } else {
        this.activeDate.setFullYear(this.activeDate.getFullYear() + 1);
        this.activeDate.setMonth(0);
      }
    } else if (type === 'prev') {
      if (this.activeDate.getMonth() !== 0) {
        this.activeDate.setMonth(this.activeDate.getMonth() - 1);
      } else {
        this.activeDate.setFullYear(this.activeDate.getFullYear() - 1);
        this.activeDate.setMonth(11);
      }
    }
    this.buildDateTable();
    this.loading = false;
  }

  buildDateTable() {
    const firstDay = new Date(this.activeDate.getFullYear() + '-' + (this.activeDate.getMonth() + 1) + '-01').getDay();
    const daysInMonth = this.daysInMonth(this.activeDate.getFullYear(), this.activeDate.getMonth());
    let currentDay = 1;
    const weekArray = [];
    for (let week = 0; week < 5; week++) {
      const dayArray = [];
      if (week === 0) {
        let i = 0;
        while (i < firstDay) {
          dayArray.push({});
          i++;
        }
        while (i < 7) {
          dayArray.push({day: currentDay, selectable: true, date: new Date(this.activeDate.getFullYear() + '-' +
            (this.activeDate.getMonth() + 1) + '-' + currentDay), isToday: this.isToday(currentDay), selected: false});
          currentDay++;
          i++;
        }
      } else {
        for (let day = 0; day < 7; day++) {
          dayArray.push({day: currentDay, selectable: true, date: new Date(this.activeDate.getFullYear() + '-' +
            (this.activeDate.getMonth() + 1) + '-' + currentDay), isToday: this.isToday(currentDay), selected: false});
          if (currentDay >= daysInMonth) {
            break;
          }
          currentDay++;
        }
      }
      weekArray.push(dayArray);
    }
    this.dateTable = weekArray;
  }

  setSelected(selectedDay) {
    for (const week of this.dateTable) {
      for (const day of week) {
        if (day.day === selectedDay && day.selectable) {
          day.selected = true;
          this.getDateString(day.day);
          this.selectedDate = day.date;
        } else {
          day.selected = false;
        }
      }
    }
  }

  saveDate() {
    if (this.selectedDate) {
      this.update.emit(this.selectedDate);
      this.isActive = false;
    }
  }

  getDateString(day) {
    this.dateString.next(day + ' ' + this.getActiveMonth() + ' ' + this.activeDate.getFullYear());
  }

  getActiveMonth() {
    const monthArray = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    return monthArray[this.activeDate.getMonth()];
  }
}
