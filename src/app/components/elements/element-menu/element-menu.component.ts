import {Component, OnInit, ViewChild} from '@angular/core';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-element-menu',
  templateUrl: './element-menu.component.html',
  styleUrls: ['./element-menu.component.scss']
})
export class ElementMenuComponent implements OnInit {

  constructor(public navService: NavService) {}

  ngOnInit() {
  }

}
