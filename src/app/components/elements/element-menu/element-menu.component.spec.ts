import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementMenuComponent } from './element-menu.component';

describe('ElementMenuComponent', () => {
  let component: ElementMenuComponent;
  let fixture: ComponentFixture<ElementMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
