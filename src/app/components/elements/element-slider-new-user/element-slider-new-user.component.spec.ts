import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewUserComponent } from './element-slider-new-user.component';

describe('ElementSliderNewUserComponent', () => {
  let component: ElementSliderNewUserComponent;
  let fixture: ComponentFixture<ElementSliderNewUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
