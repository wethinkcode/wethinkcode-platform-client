import { Component, OnInit } from '@angular/core';
import { ElementSlider } from '../../../abstract-classes/element-slider';
import { User } from '../../../models/user';
import { NavService } from '../../../services/nav.service';
import { ToolService } from '../../../services/tool.service';
import { Error } from '../../../models/error';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-element-slider-new-user',
  templateUrl: './element-slider-new-user.component.html',
  styleUrls: ['./element-slider-new-user.component.scss']
})
export class ElementSliderNewUserComponent extends ElementSlider implements OnInit {
  tools: Array<any> = [];
  user: User;

  constructor(
    public navService: NavService,
    public userService: UserService,
    public toolService: ToolService
  ) {
    super();
    this.user = new User({});
  }

  ngOnInit() {
    this.getTools();
  }

  setUserRole(value) {
    this.user.role = value;
  }

  clickHookTools(selectedTool) {
    for (const tool of this.tools) {
      if (tool.id === selectedTool.id) {
        tool.selected = !tool.selected;
        if (!tool.selected) {
          let i = 0;
          while (i < this.user.tools.length) {
            if (this.user.tools[i].id === tool.id) {
              break;
            }
            i++;
          }
          this.user.tools.splice(i, 1);
        } else if (!this.user.tools.includes({id: tool.id}) && tool.selected ) {
          this.user.tools.push({id: tool.id});
        }
      }
    }
  }

  submitNewUser() {
    if (!this.user.firstName) {
      this.error = new Error('C0001', 'A user first name is required.', 'New user slider - New user', 9);
    } else if (!this.user.lastName) {
      this.error = new Error('C0002', 'A user last name is required.', 'New user slider - New user', 9);
    } else if (!this.user.username) {
      this.error = new Error('C0003', 'A username is required.', 'New user slider - New user', 9);
    } else if (!this.user.email) {
      this.error = new Error('C0004', 'A user email address is required.', 'New user slider - New user', 9);
    } else if (!this.userService.validateEmail(this.user.email)) {
      this.error = new Error('C0005', 'You have entered an invalid email address.', 'signup', 9);
    } else if (!this.user.role) {
      this.error = new Error('C0006', 'A user role is required.', 'New user slider - New user', 9);
    } else {
      if (!this.user.tools.length) {
        this.error = new Error('C0007', 'At least one user tool is required.', 'New user slider - New user', 9);
      } else {
        this.loading = true;
        this.userService.userInvite(this.user).subscribe(res => {
          this.loading = false;
          if (res.status !== 'success') {
            this.failure = true;
            this.resultMessage = res.message;
          } else {
            this.success = true;
            this.resultMessage = res.message;
          }
        });
      }
    }
  }

  getTools() {
    this.toolService.getAllTools().subscribe(res => {
      this.tools = [];
      if (res.code === 'S0002') {
        for (const tool of res.data) {
          tool.selected = false;
          this.tools.push(tool);
        }
      }
    });
  }

  clearForm() {
    this.failure = false;
    this.success = false;
    this.user = new User({});
    for (const tool of this.tools) {
      tool.selected = false;
    }
  }

  closeForm() {
    this.clearForm();
    this.navService.isNewUserSliderToggled = false;
  }
}
