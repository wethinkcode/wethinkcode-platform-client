import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewBootcampComponent } from './element-slider-new-bootcamp.component';

describe('ElementSliderNewBootcampComponent', () => {
  let component: ElementSliderNewBootcampComponent;
  let fixture: ComponentFixture<ElementSliderNewBootcampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewBootcampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewBootcampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
