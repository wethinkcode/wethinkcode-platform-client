import { Component, OnInit } from '@angular/core';

import { NavService } from '../../../services/nav.service';
import { BootcampService } from '../../../services/bootcamp.service';

import { Bootcamp } from '../../../models/bootcamp';
import { Error } from '../../../models/error';

import { ElementSlider } from '../../../abstract-classes/element-slider';

@Component({
  selector: 'app-element-slider-new-bootcamp',
  templateUrl: './element-slider-new-bootcamp.component.html',
  styleUrls: ['./element-slider-new-bootcamp.component.scss']
})
export class ElementSliderNewBootcampComponent extends ElementSlider implements OnInit {
  bootcamp: Bootcamp = new Bootcamp({});

  constructor(
    public navService: NavService,
    public bootcampService: BootcampService
  ) {
    super();
  }

  ngOnInit() {
  }

  newBootcamp() {
    if (!this.bootcamp.title) {
      this.error = new Error('C0001', 'Bootcamps require a title.', 'Bootcamp - New bootcamp slider', 9);
    } else if (!this.bootcamp.startDate) {
      this.error = new Error('C0002', 'Bootcamps require a start date', 'Bootcamp - New bootcamp slider', 9);
    } else if (!this.bootcamp.endDate) {
      this.error = new Error('C0003', 'Bootcamps require an end date.', 'Bootcamp - New bootcamp slider', 9);
    } else if (!this.bootcamp.selectionDate) {
      this.error = new Error('C0004', 'Bootcamps require a selection date.', 'Bootcamp - New bootcamp slider', 9);
    } else {
      this.loading = true;
      this.bootcampService.newBootcamp(this.bootcamp).subscribe(res => {
        this.loading = false;
        if (res.code !== 'S0010') {
          this.failure = true;
        } else {
          this.success = true;
          this.bootcampService.bootcamps.push(this.bootcamp);
        }
        this.resultMessage = res.message;
      });
    }
  }

  clearBootcamp() {
    this.success = false;
    this.failure = false;
    this.resultMessage = '';
    this.bootcamp = new Bootcamp({});
  }

  closeSlider() {
    this.clearBootcamp();
    this.navService.isNewBootcampSliderToggled = false;
  }

  setStartDate(event) {
    this.bootcamp.startDate = event;
  }

  setEndDate(event) {
    this.bootcamp.endDate = event;
  }

  setSelectionDate(event) {
    this.bootcamp.selectionDate = event;
  }

}
