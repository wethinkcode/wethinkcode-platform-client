import {Component, Input, OnInit} from '@angular/core';

import { Error } from '../../../models/error';
import { User } from '../../../models/user';

import { NavService } from '../../../services/nav.service';
import { UserService } from '../../../services/user.service';
import { NoteService } from '../../../services/note.service';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-element-note',
  templateUrl: './element-note.component.html',
  styleUrls: ['./element-note.component.scss']
})
export class ElementNoteComponent implements OnInit {
  loading: Boolean = false;
  error: Error;
  failure: Boolean = false;
  failureMessage: String = '';
  success: Boolean = false;
  successMessage: String = '';
  isEdit: Boolean = false;

  constructor(
    public navService: NavService,
    private _userService: UserService,
    private _authenticationService: AuthenticationService,
    public noteService: NoteService
  ) {
  }

  ngOnInit() {
  }

  clearError() {
    delete this.error;
  }

  saveNote() {
    this.noteService.activeNote.user = this._userService.selectedUser;
    this.noteService.activeNote.noteTakingUser.id = this._authenticationService.id;
    if (!this.noteService.activeNote.title) {
      this.error = new Error('C0001', 'A note title is required.', 'Add user note.', 9);
    } else if (!this.noteService.activeNote.content) {
      this.error = new Error('C0002', 'Note content is required.', 'Add user note.', 9);
    } else if (!this.noteService.activeNote.priority) {
      this.error = new Error('C0003', 'Note priority is required.', 'Add user note.', 9);
    } else {
      if (!this.isEdit) {
        this.loading = true;
        this.noteService.newUserNote().subscribe(res => {
          this.loading = false;
          if (res.code === 'S0007') {
            this.success = true;
            this.successMessage = 'Note added successfully';
            this.noteService.notes.unshift(this.noteService.activeNote);
          } else {

          }
        });
      } else {
        this.loading = true;
        this.noteService.updateUserNote().subscribe(res => {
        });
      }
    }
  }

  clearNote() {
    this.success = false;
    this.successMessage = '';
    this.failure = false;
    this.failureMessage = '';
    this._userService.selectedUser = new User({});
  }

}
