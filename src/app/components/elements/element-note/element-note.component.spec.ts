import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementNoteComponent } from './element-note.component';

describe('ElementNoteComponent', () => {
  let component: ElementNoteComponent;
  let fixture: ComponentFixture<ElementNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
