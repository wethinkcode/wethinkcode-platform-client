import {Component, EventEmitter, Input, OnInit} from '@angular/core';

import { ElementSlider } from '../../../abstract-classes/element-slider';

import { ProjectInstance } from '../../../models/project-instance';
import { Error } from '../../../models/error';

import { NavService } from '../../../services/nav.service';
import { ProjectService } from '../../../services/project.service';

@Component({
  selector: 'app-element-slider-new-project-instance',
  templateUrl: './element-slider-new-project-instance.component.html',
  styleUrls: ['./element-slider-new-project-instance.component.scss']
})
export class ElementSliderNewProjectInstanceComponent extends ElementSlider implements OnInit {
  projectInstance: ProjectInstance;

  constructor(
    public navService: NavService,
    public projectService: ProjectService
  ) {
    super();
  }

  ngOnInit() {
    this.projectInstance = new ProjectInstance({});
  }

  clearProjectInstance() {
    this.failure = false;
    this.success = false;
    this.loading = false;
    this.resultMessage = '';
    this.projectInstance = new ProjectInstance({});
    this.navService.isNewProjectInstanceSliderToggled = false;
  }

  newProjectInstance() {
    if (!this.projectInstance.title) {
      this.error = new Error('C0001', 'Project instances require a title.', 'Project Instance - New project instance', 9);
    } else if (!this.projectInstance.validationGrade) {
      this.error = new Error('C0002', 'Project instances require a validation grade.', 'Project Instance - New project instance', 9);
    } else if (isNaN(this.projectInstance.validationGrade)) {
      this.error = new Error('C0003', 'Project instance validation grades require numbers as a value.', 'Project Instance - New project instance', 9);
    } else if (!this.projectInstance.startDate) {
      this.error = new Error('C0004', 'Project instances require a start date.', 'Project Instance - New project instance', 9);
    } else if (!this.projectInstance.endDate) {
      this.error = new Error('C0005', 'Project instances require an end date.', 'Project Instance - New project instance', 9);
    } else {
      this.loading = true;
      this.projectService.newProjectInstance(this.projectInstance).subscribe(res => {
        this.loading = false;
        if (res.code !== 'S0007') {
          this.failure = true;
        } else {
          this.success = true;
          this.projectService.projectInstances.push(this.projectInstance);
        }
        this.resultMessage = res.message;
      });
    }
  }

  setStartDate(event) {
    this.projectInstance.startDate = event;
  }

  setEndDate(event) {
    this.projectInstance.endDate = event;
  }

  clearForm() {
    this.failure = false;
    this.success = false;
    this.projectInstance = new ProjectInstance({});
  }

  closeForm() {
    this.clearForm();
    this.navService.isNewProjectInstanceSliderToggled = false;
  }
}
