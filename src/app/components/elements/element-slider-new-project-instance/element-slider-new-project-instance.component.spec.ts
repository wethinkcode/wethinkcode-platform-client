import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewProjectInstanceComponent } from './element-slider-new-project-instance.component';

describe('ElementSliderNewProjectInstanceComponent', () => {
  let component: ElementSliderNewProjectInstanceComponent;
  let fixture: ComponentFixture<ElementSliderNewProjectInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewProjectInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewProjectInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
