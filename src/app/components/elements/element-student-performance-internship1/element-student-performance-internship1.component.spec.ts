import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentPerformanceInternship1Component } from './element-student-performance-internship1.component';

describe('ElementStudentPerformanceInternship1Component', () => {
  let component: ElementStudentPerformanceInternship1Component;
  let fixture: ComponentFixture<ElementStudentPerformanceInternship1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentPerformanceInternship1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentPerformanceInternship1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
