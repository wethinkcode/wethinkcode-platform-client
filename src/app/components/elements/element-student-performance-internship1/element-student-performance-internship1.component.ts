import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-element-student-performance-internship1',
  templateUrl: './element-student-performance-internship1.component.html',
  styleUrls: ['./element-student-performance-internship1.component.scss']
})
export class ElementStudentPerformanceInternship1Component implements OnInit {
  @Input() student: any;
  studentInternship1Data = null;

  constructor() { }

  ngOnInit() {
    this.student.event.subscribe(event => {
      if (event === 'getInternship1Data' && this.studentInternship1Data === null) {
        this.getStudentInternship1Data();
      }
    });
  }

  getStudentInternship1Data() {

  }
}
