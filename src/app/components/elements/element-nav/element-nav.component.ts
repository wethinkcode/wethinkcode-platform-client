import { Component, OnInit } from '@angular/core';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-element-nav',
  templateUrl: './element-nav.component.html',
  styleUrls: ['./element-nav.component.scss']
})
export class ElementNavComponent implements OnInit {

  constructor(
    public navService: NavService
  ) { }

  toggleMenu() {
    this.navService.menuToggled = !this.navService.menuToggled;
  }

  ngOnInit() {
  }

}
