import {Component, Input, OnInit} from '@angular/core';
import { UserService } from '../../../services/user.service';
import {User} from "../../../models/user";

@Component({
  selector: 'app-element-user-information',
  templateUrl: './element-user-information.component.html',
  styleUrls: ['./element-user-information.component.scss']
})
export class ElementUserInformationComponent implements OnInit {
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: Boolean = false;
  @Input() user: User;
  userInformation = {};

  dateToText(dateFormat) {
    let date = '';
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    date += dateFormat.getDate().toString() + ' ';
    date += months[dateFormat.getMonth()] + ' ';
    date += dateFormat.getFullYear().toString();
    return date;
  }

  constructor(
    private _userService: UserService
  ) {
  }

  ngOnInit() {
    this.loading = true;
    this._userService.getUserInformation(this.user.id).subscribe(res => {
      this.loading = false;
     if (res.status !== 'success') {
       this.componentFailure = true;
       this.resultMessage = res.message;
     } else {
       if (res.data) {
         this.user.updateUser(res.data);
       }
     }
    });
  }
}
