import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementUserInformationComponent } from './element-user-information.component';

describe('ElementUserInformationComponent', () => {
  let component: ElementUserInformationComponent;
  let fixture: ComponentFixture<ElementUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
