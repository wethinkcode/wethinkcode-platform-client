import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementImageLoaderComponent } from './element-image-loader.component';

describe('ElementImageLoaderComponent', () => {
  let component: ElementImageLoaderComponent;
  let fixture: ComponentFixture<ElementImageLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementImageLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementImageLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
