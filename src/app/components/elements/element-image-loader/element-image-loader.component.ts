import { Component, Input, OnInit } from '@angular/core';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-element-image-loader',
  templateUrl: './element-image-loader.component.html',
  styleUrls: ['./element-image-loader.component.scss']
})
export class ElementImageLoaderComponent implements OnInit {
  @Input() imagePlaceholder: String = '';
  @Input() imageUrl: String = '';
  @Input() imageProperties: Object = {};
  loading: Boolean = false;
  imageLoaded: Boolean = false;

  constructor(
    public navService: NavService
  ) {}

  ngOnInit() {
    if (this.imageUrl) {
      this.loading = true;
      this.replaceImage();
    }
  }

  replaceImage() {
  }

}
