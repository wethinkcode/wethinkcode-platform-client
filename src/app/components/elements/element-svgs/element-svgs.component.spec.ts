import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSvgsComponent } from './element-svgs.component';

describe('ElementSvgsComponent', () => {
  let component: ElementSvgsComponent;
  let fixture: ComponentFixture<ElementSvgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSvgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSvgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
