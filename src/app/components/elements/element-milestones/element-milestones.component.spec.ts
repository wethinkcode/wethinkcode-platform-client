import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementMilestonesComponent } from './element-milestones.component';

describe('ElementMilestonesComponent', () => {
  let component: ElementMilestonesComponent;
  let fixture: ComponentFixture<ElementMilestonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementMilestonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementMilestonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
