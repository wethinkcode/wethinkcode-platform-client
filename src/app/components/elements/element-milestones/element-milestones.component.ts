import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-element-milestones',
  templateUrl: './element-milestones.component.html',
  styleUrls: ['./element-milestones.component.scss']
})
export class ElementMilestonesComponent implements OnInit {
  milestoneItemsY1: Array<any> = [];
  progressy1: any = 0;
  milestoneItemsInternship1 = [];
  milestoneItemsY2: Array<any> = [];
  milestoneItemsInternship2: Array<any> = [];
  progressPosition: any = 0;
  progress: any = 0;
  startDate: any = 0;
  endDate: any = 0;
  $examsValidated = Observable.of(this.userService.selectedUser.examsValidated);
  $y1ModulesValidated = Observable.of(this.userService.selectedUser.y1ModulesValidated);
  y1ModulesValidated;
  $modules = Observable.of(this.userService.selectedUser.modules);
  examsValidated = 0;

  constructor(
    public userService: UserService
  ) {
  }

  changeProgress(direction) {
    if (direction === 'next' && this.progress < 3) {
      this.progress++;
    } else if (direction === 'previous' && this.progress > 0) {
      this.progress--;
    }
    this.progressPosition = 'calc((100% + 105px) * -' + this.progress + ')';
  }

  ngOnInit() {

    this.progress = 0;
    if (this.userService.selectedUser.currentMonth) {
      if (this.userService.selectedUser.currentMonth > 8 && this.userService.selectedUser.currentMonth <= 12) {
        this.progress = 1;
      } else if (this.userService.selectedUser.currentMonth > 12 && this.userService.selectedUser.currentMonth <= 20) {
        this.progress = 2;
      } else if (this.userService.selectedUser.currentMonth > 20 && this.userService.selectedUser.currentMonth <= 24) {
        this.progress = 3;
      }
    }
    this.progressPosition = 'calc((100% + 105px) * -' + this.progress + ')';
    this.$examsValidated.subscribe(data => {
      this.examsValidated = data;
    });
    this.milestoneItemsY1 = [
      {icon: 'bootcamp-icon', slug: 'y1-enrolled', title: 'Y1 Enrolled', position: 0},
      {icon: 'exam-icon', slug: 'y1-exams', title: '5 Exams', position: 16},
      {icon: 'module-icon', slug: 'y1-modules', title: '2 Modules', position: 38},
      {icon: 'intern-icon', slug: 'internship1-ready', title: 'Internship-ready', position: 100}
    ];
    this.milestoneItemsInternship1 = [
      {icon: 'intern-icon', slug: 'internship1-allocated', title: 'Internship Allocated', position: 0},
      {icon: 'module-icon', slug: 'internship1-completed', title: 'Internship Completed', position: 100},
    ];
    this.milestoneItemsY2 = [
      {icon: 'bootcamp-icon', slug: 'y2-enrolled', title: 'Y2 Enrolled', position: 0},
      {icon: 'module-icon', slug: 'y2-modules', title: '3 modules', position: 70},
      {icon: 'open-project-icon', slug: 'open-project', title: 'Open Project', position: 85},
      {icon: 'intern-icon', slug: 'internship2-ready', title: 'Internship-ready', position: 100}
    ];
    this.milestoneItemsInternship2 = [
      {icon: 'intern-icon', slug: 'internship2-allocated', title: 'Internship Allocated', position: 0},
      {icon: 'module-icon', slug: 'internship2-completed', title: 'Internship Completed', position: 100}
      ];
  }

  isMilestoneComplete(slug) {
    return (
      (slug === 'y1-enrolled') ||
      (slug === 'y1-exams' && this.userService.selectedUser.examsValidated >= 5) ||
      (slug === 'y1-modules' && this.userService.selectedUser.y1ModulesValidated >= 2) ||
      (slug === 'internship1-ready' && this.userService.selectedUser.y1ModulesValidated >= 2 && this.userService.selectedUser.examsValidated >= 5) ||
      (slug === 'internship1-allocated' && this.userService.selectedUser.currentMonth > 8) ||
      (slug === 'internship1-completed' && this.userService.selectedUser.internship1Validated) ||
      (slug === 'y2-enrolled' && this.userService.selectedUser.currentMonth > 12) ||
      (slug === 'y2-modules' && this.userService.selectedUser.y2ModulesValidated >= 3) ||
      (slug === 'internship2-ready' && this.userService.selectedUser.internship2Ready) ||
      (slug === 'internship2-allocated' && this.userService.selectedUser.currentMonth > 20) ||
      (slug === 'internship2-completed' && this.userService.selectedUser.internship2Validated)

    );
  }
}
