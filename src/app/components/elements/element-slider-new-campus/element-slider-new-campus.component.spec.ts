import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewCampusComponent } from './element-slider-new-campus.component';

describe('ElementSliderNewCampusComponent', () => {
  let component: ElementSliderNewCampusComponent;
  let fixture: ComponentFixture<ElementSliderNewCampusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewCampusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewCampusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
