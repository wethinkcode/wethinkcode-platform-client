import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NavService } from '../../../services/nav.service';
import { NotificationService } from '../../../services/notification.service';
import { MilestoneService } from '../../../services/milestone.service';
import { CurriculumService } from '../../../services/curriculum.service';

import { ElementSlider } from '../../../abstract-classes/element-slider';

import { Milestone } from '../../../models/milestone';
import { Module } from '../../../models/module';
import { Curriculum } from '../../../models/curriculum';
import { Error } from '../../../models/error';
import {ModulesService} from "../../../services/modules.service";

@Component({
  selector: 'app-element-slider-new-module',
  templateUrl: './element-slider-new-module.component.html',
  styleUrls: ['./element-slider-new-module.component.scss']
})
export class ElementSliderNewModuleComponent extends ElementSlider implements OnInit {
  module: Module;
  curriculum: Array<Curriculum> = []  ;
  milestones: Array<Milestone> = [];
  loadingCurriculum: Boolean = true;
  loadingMilestones: Boolean = true;

  constructor(
    private _notificationService: NotificationService,
    private _router: Router,
    private _milestoneService: MilestoneService,
    private _curriculumService: CurriculumService,
    private _modulesService: ModulesService,
    public navService: NavService
  ) {
    super();
    this.module = new Module({});
    this.module.curriculum.push(new Curriculum({id: -1}));
    this.module.milestones.push(new Milestone({id: -1}));
  }

  ngOnInit() {
    this._router.events.subscribe(event => {
      this._milestoneService.page = 1;
      this._milestoneService.milestones = [];
    });
    this.getMilestones().catch(err => {
      this.failure = true;
      this.resultMessage = 'We could not fetch the milestones you requested';
      this._notificationService.newNotification({type: 'once-off', content: this.resultMessage, classification: 'error' });
    });
    this.getCurriculum().catch(err => {
      this.failure = true;
      this.resultMessage = 'We could not fetch the curriculum you requested';
      this._notificationService.newNotification({type: 'once-off', content: this.resultMessage, classification: 'error' });
    });
  }

  getMilestones() {
    this.loadingMilestones = true;
    return new Promise((resolve, reject) => {
      this._milestoneService.getMoreMilestones().subscribe(result => {
        this._milestoneService.page++;
        this.loadingMilestones = false;
        if (result.code !== 'S0002') {
          reject(result);
        } else {
          for (const milestone of result.data) {
            if (milestone) {
              this.milestones.push(new Milestone(milestone));
            }
          }
          resolve(result);
        }
      });
    });
  }

  setCurriculum(curriculumId, index) {
    for (const curriculum of this.curriculum) {
      if (curriculum.id === parseInt(curriculumId)) {
        this.module.curriculum[index] = new Curriculum(curriculum);
      }
    }
  }

  setMilestone(milestoneId, index) {
    for (const milestone of this.milestones) {
      if (milestone.id === parseInt(milestoneId)) {
        this.module.milestones[index] = new Milestone(milestone);
      }
    }
  }

  newModule() {
    if (!this.module.title) {
      this.error = new Error('C0001', 'A module title is required.', 'Module - Add module', 9);
    } else {
      this.loading = true;
      this._modulesService.newModule(this.module).subscribe(res => {
        this.loading = false;
        if (res.code !== 'S0003') {
          this.failure = true;
          this.resultMessage = res.message;
        } else {
          this._modulesService.modules.unshift(this.module);
          this.success = true;
          this.resultMessage = 'Campus added successfully.';
        }
      });
    }
  }

  getCurriculum() {
   this.loadingCurriculum = true;
   return new Promise((resolve, reject) => {
     this._curriculumService.getMoreCurriculum().subscribe(result => {
       this._curriculumService.page++;
       this.loadingCurriculum = false;
       if (result.code !== 'S0002') {
         reject(result);
       } else {
         for (const curriculum of result.data) {
           if (curriculum) {
             this.curriculum.push(new Curriculum(curriculum));
           }
         }
         resolve(result);
       }
     });
   });
  }


  clearModule() {
    this.navService.isNewModuleSliderToggled = false;
    this.loading = true;
    setTimeout(() => {

      this.module = new Module({});
      this.module.curriculum.push(new Curriculum({id: -1}));
      this.module.milestones.push(new Milestone({id: -1}));
      this.resultMessage = '';
      this.success = false;
      this.failure = false;
      this.loading = false;
    }, 1000);
  }
}
