import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderNewModuleComponent } from './element-slider-new-module.component';

describe('ElementSliderNewModuleComponent', () => {
  let component: ElementSliderNewModuleComponent;
  let fixture: ComponentFixture<ElementSliderNewModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderNewModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderNewModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
