import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementComponentFailureComponent } from './element-component-failure.component';

describe('ElementComponentFailureComponent', () => {
  let component: ElementComponentFailureComponent;
  let fixture: ComponentFixture<ElementComponentFailureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementComponentFailureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementComponentFailureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
