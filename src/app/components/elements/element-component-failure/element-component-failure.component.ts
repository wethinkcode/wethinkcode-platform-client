import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-element-component-failure',
  templateUrl: './element-component-failure.component.html',
  styleUrls: ['./element-component-failure.component.scss']
})
export class ElementComponentFailureComponent implements OnInit {
  @Input() reasonSubject: any;
  reason: String;

  constructor() { }

  ngOnInit() {
    this.reasonSubject.subscribe(event => {
      this.reason = event;
    });
  }

}
