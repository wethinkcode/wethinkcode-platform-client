import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementFileUploaderComponent } from './element-file-uploader.component';

describe('ElementFileUploaderComponent', () => {
  let component: ElementFileUploaderComponent;
  let fixture: ComponentFixture<ElementFileUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementFileUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementFileUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
