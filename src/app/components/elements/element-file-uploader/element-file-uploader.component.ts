import { Component, Input, OnInit } from '@angular/core';
import { FileService } from '../../../services/file.service';

@Component({
  selector: 'app-element-file-uploader',
  templateUrl: './element-file-uploader.component.html',
  styleUrls: ['./element-file-uploader.component.scss']
})
export class ElementFileUploaderComponent implements OnInit {
  @Input() url;
  @Input() title;
  @Input() type;
  success: Boolean = false;
  failure: Boolean = false;
  resultMessage = '';
  resultRejected = 0;
  resultUpdated = 0;
  resultNew = 0;
  loading: Boolean = true;
  loadingStatus: String = 'Initializing file uploader';

  constructor(
    public fileService: FileService,
  ) { }

  ngOnInit() {
    this.loading = false;
  }

  parseUsers(users) {
    const parsedObject = {users: []};
    for (let parsingUser of users) {
      parsingUser = parsingUser.split(',');
      const parsedUser = {username: '', email: '', firstName: '', lastName: ''};
      if (parsingUser[0]) {
        parsedUser.username = parsingUser[0];
      }
      if (parsingUser[1]) {
        parsedUser.email = parsingUser[1];
      }
      if (parsingUser[2]) {
        parsedUser.firstName = parsingUser[2];
      }
      if (parsingUser[3]) {
        parsedUser.lastName = parsingUser[3];
      }
      parsedObject.users.push(parsedUser);
    }
    return parsedObject;
  }

  parseVotes(votes) {
    const parsedObject = {votes: []};
    for (let vote of votes) {
      vote = vote.split(',');
      const parsedVote = {voter: '', votee: ''};
      if (vote[0]) {
        parsedVote.votee = vote[0];
      }
      if (vote[1]) {
        parsedVote.voter = vote[1];
      }
      parsedObject.votes.push(parsedVote);
    }
    return parsedObject;
  }

  uploadCSV(event) {
    this.loading = true;
    this.loadingStatus = 'Reading file(s)';
    const fileReader = new FileReader();
    const files = event.target.files;
    for (const file of files) {
      fileReader.onloadend = e => {
        let objectToFill = {};
        switch (this.type) {
          case 'votes':
            objectToFill = this.parseVotes(fileReader.result.split('\n'));
            break;
          case 'users':
            objectToFill = this.parseUsers(fileReader.result.split('\n'));
            break;
          default:
            break;
        }
        this.loadingStatus = 'Uploading file(s)';
        this.fileService.uploadCSV(this.url, objectToFill).subscribe(res => {
           this.loading = false;
           if (res.status === 'success') {
             this.success = true;
             this.resultMessage = res.message;
             if (res.data.rejected) {
               this.resultRejected = res.data.rejected.length;
             }
             if (res.data.new) {
               this.resultNew = res.data.new.length;
             }
             if (res.data.updated) {
                this.resultUpdated = res.data.updated.length;
             }
           } else {
             this.failure = true;
             this.resultMessage = res.message;
           }
        });
      };
      fileReader.readAsText(file);
    }
  }
}
