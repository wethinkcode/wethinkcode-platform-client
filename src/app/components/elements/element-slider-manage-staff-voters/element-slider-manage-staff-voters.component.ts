import {Component, Input, OnInit} from '@angular/core';
import { ElementSlider } from '../../../abstract-classes/element-slider';
import { User } from '../../../models/user';
import { StaffService } from '../../../services/staff.service';
import { BootcampService } from '../../../services/bootcamp.service';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-element-slider-manage-staff-voters',
  templateUrl: './element-slider-manage-staff-voters.component.html',
  styleUrls: ['./element-slider-manage-staff-voters.component.scss']
})
export class ElementSliderManageStaffVotersComponent extends ElementSlider implements OnInit {
  page: Number = 1;
  limit: 30;

  constructor(
    public staffService: StaffService,
    public bootcampService: BootcampService,
    public navService: NavService
  ) {
    super();
  }

  ngOnInit() {
    this.loading = true;
    this.staffService.getMoreStaff().subscribe(res => {
      this.loading = false;
      if (res.status !== 'success') {
        this.failure = true;
        this.resultMessage = res.message;
      } else {
        if (res.data.length) {
          this.staffService.page++;
          for (const user of res.data) {
            this.staffService.staff.push(new User(user));
          }
        }
      }
    });
  }

  toggleStaffMember(staffMember) {
    staffMember.loading = true;
    if (!staffMember.selected) {
      this.staffService.subscribeToBootcamp(this.bootcampService.selectedBootcamp.id, staffMember).subscribe(res => {
        staffMember.loading = false;
        if (res.status === 'success') {
          staffMember.selected = true;
        }
      });
    } else {
      this.staffService.unsubscribeFromBootcamp(this.bootcampService.selectedBootcamp.id, staffMember).subscribe(res => {
        staffMember.loading = false;
        if (res.status === 'success') {
          staffMember.selected = false;
        }
      });
    }
  }
}
