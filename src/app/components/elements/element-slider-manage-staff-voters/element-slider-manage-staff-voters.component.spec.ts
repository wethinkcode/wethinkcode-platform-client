import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementSliderManageStaffVotersComponent } from './element-slider-manage-staff-voters.component';

describe('ElementSliderManageStaffVotersComponent', () => {
  let component: ElementSliderManageStaffVotersComponent;
  let fixture: ComponentFixture<ElementSliderManageStaffVotersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementSliderManageStaffVotersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementSliderManageStaffVotersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
