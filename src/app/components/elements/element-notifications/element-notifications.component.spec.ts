import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementNotificationsComponent } from './element-notifications.component';

describe('ElementNotificationsComponent', () => {
  let component: ElementNotificationsComponent;
  let fixture: ComponentFixture<ElementNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
