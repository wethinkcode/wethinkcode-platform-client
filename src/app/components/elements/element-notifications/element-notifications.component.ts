import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-element-notifications',
  templateUrl: './element-notifications.component.html',
  styleUrls: ['./element-notifications.component.scss']
})
export class ElementNotificationsComponent implements OnInit {

  constructor(
    public notificationService: NotificationService
  ) { }

  ngOnInit() {
  }

}
