import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-element-student-performance-year1',
  templateUrl: './element-student-performance-year1.component.html',
  styleUrls: ['./element-student-performance-year1.component.scss']
})
export class ElementStudentPerformanceYear1Component implements OnInit {
  @Input() student: any;
  studentYear1Data = {
    chartOptions: {
      responsive: true,
      maintainAspectRatio: false
    },
    chartData: [
      { data: [10, 20, 30, 20], label: 'Attendance'},
    ],
    chartLabels: ['January', 'February', 'Mars', 'April'],
    chartColors: [{borderColor: ['#016c90'], backgroundColor: ['rgba(1,108,144,0.1)']}]
  };

  constructor() { }

  ngOnInit() {
    this.student.event.subscribe(event => {
      if (event === 'getYear1Data' && this.studentYear1Data === null) {
        this.getStudentYear1Data();
      }
    });
  }

  getStudentYear1Data() {
  }
}
