import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementStudentPerformanceYear1Component } from './element-student-performance-year1.component';

describe('ElementStudentPerformanceYear1Component', () => {
  let component: ElementStudentPerformanceYear1Component;
  let fixture: ComponentFixture<ElementStudentPerformanceYear1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementStudentPerformanceYear1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementStudentPerformanceYear1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
