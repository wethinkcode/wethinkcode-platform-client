import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageBootcampsComponent } from './sub-page-bootcamps.component';

describe('SubPageBootcampsComponent', () => {
  let component: SubPageBootcampsComponent;
  let fixture: ComponentFixture<SubPageBootcampsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageBootcampsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageBootcampsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
