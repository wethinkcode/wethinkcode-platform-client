import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Title } from '@angular/platform-browser';

import { NavService } from '../../../services/nav.service';
import { BootcampService } from '../../../services/bootcamp.service';

import { Bootcamp } from '../../../models/bootcamp';

@Component({
  selector: 'app-sub-page-bootcamps',
  templateUrl: './sub-page-bootcamps.component.html',
  styleUrls: ['./sub-page-bootcamps.component.scss']
})
export class SubPageBootcampsComponent implements OnInit {
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: String = '';
  page: Number = 1;
  limit: Number = 100;

  constructor(
    public navService: NavService,
    public bootcampService: BootcampService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _titleService: Title
  ) { }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading bootcamps module ';
    this._titleService.setTitle('WeThinkCode_ | Bootcamps');
    this.navService.dashboardLoading.next(false);
    this._router.events.subscribe(event => {
      this.bootcampService.bootcamps = [];
    });
    this.bootcampService.getBootcamps(this.page, this.limit).subscribe(res => {
      this.loading = false;
      if (res.code !== 'S0003') {
        this.componentFailure = true;
        this.resultMessage = 'We could not load the projects you requested. Please reload your page.';
      } else {
        if (res.data.length) {
          for (const bootcamp of res.data) {
            this.bootcampService.bootcamps.push(new Bootcamp(bootcamp));
          }
          this.bootcampService.bootcamps.sort((a: any, b: any) => a.startDate - b.startDate);
        }
      }
    });
  }


  getBootcampIdentifier(bootcamp) {
    return bootcamp.slug ? bootcamp.slug : bootcamp.id;
  }
}
