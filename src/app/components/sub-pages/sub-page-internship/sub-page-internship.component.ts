import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-sub-page-internship',
  templateUrl: './sub-page-internship.component.html',
  styleUrls: ['./sub-page-internship.component.scss']
})
export class SubPageInternshipComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Internship');
  }
}
