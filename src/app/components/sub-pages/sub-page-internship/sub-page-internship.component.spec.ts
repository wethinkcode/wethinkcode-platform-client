import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageInternshipComponent } from './sub-page-internship.component';

describe('SubPageInternshipComponent', () => {
  let component: SubPageInternshipComponent;
  let fixture: ComponentFixture<SubPageInternshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageInternshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageInternshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
