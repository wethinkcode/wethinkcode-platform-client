import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-community-service',
  templateUrl: './sub-page-community-service.component.html',
  styleUrls: ['./sub-page-community-service.component.scss']
})
export class SubPageCommunityServiceComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Community Service');
  }

}
