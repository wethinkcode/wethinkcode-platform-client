import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCommunityServiceComponent } from './sub-page-community-service.component';

describe('SubPageCommunityServiceComponent', () => {
  let component: SubPageCommunityServiceComponent;
  let fixture: ComponentFixture<SubPageCommunityServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCommunityServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCommunityServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
