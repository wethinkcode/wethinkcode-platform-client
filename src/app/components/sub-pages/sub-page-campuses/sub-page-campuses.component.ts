import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import { Title } from '@angular/platform-browser';

import { CampusService } from '../../../services/campus.service';
import { NavService } from '../../../services/nav.service';

import { Campus } from '../../../models/campus';
import {NotificationService} from "../../../services/notification.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-sub-page-campuses',
  templateUrl: './sub-page-campuses.component.html',
  styleUrls: ['./sub-page-campuses.component.scss']
})
export class SubPageCampusesComponent implements OnInit {
  loading: Boolean = true;
  componentFailure = false;
  componentFailureMessage = '';

  constructor(
    private _titleService: Title,
    private _router: Router,
    public campusService: CampusService,
    public navService: NavService,
    private _notificationService: NotificationService,
    private _elementRef: ElementRef
  ) { }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading campus module...';
    this._titleService.setTitle('WeThinkCode_ | Campuses');
    this._router.events.subscribe(event => {
      this.campusService.campuses = [];
    });
    this.campusService.getMoreCampuses().subscribe(result => {
      this.navService.dashboardLoading.next(false);
      this.loading = false;
      if (result.code !== 'S0002') {
        this.componentFailure = true;
        this.componentFailureMessage = result.message;
        this._notificationService.newNotification({type: 'once-off', content: 'Could not retrieve campuses.', classification: 'error' });
      } else {
        for (const campus of result.data) {
          this.campusService.campuses.push(new Campus(campus));
        }
      }
    });
  }

}
