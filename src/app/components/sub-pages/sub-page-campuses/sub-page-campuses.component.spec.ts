import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCampusesComponent } from './sub-page-campuses.component';

describe('SubPageCampusesComponent', () => {
  let component: SubPageCampusesComponent;
  let fixture: ComponentFixture<SubPageCampusesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCampusesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCampusesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
