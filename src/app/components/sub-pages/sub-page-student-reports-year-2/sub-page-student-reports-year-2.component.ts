import {Component, HostListener, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { StudentService } from '../../../services/student.service';
import { NavService } from '../../../services/nav.service';
import {User} from "../../../models/user";

@Component({
  selector: 'app-sub-page-student-reports-year-2',
  templateUrl: './sub-page-student-reports-year-2.component.html',
  styleUrls: ['./sub-page-student-reports-year-2.component.scss']
})
export class SubPageStudentReportsYear2Component implements OnInit {
  users: Array<any> = [];
  loading: Boolean = true;
  loadingMoreStudents: Boolean = false;
  componentFailure: Boolean = false;
  componentFailureMessage: String = '';
  modules: Array<any> = [];
  page = 1;
  limit = 20;
  gettingAllUsers: Boolean = false;
  gotAllUsers: Boolean = false;

  isStudentNameFilterMenuToggled: Boolean = false;
  isStudentUsernameFilterMenuToggled: Boolean = false;
  isStudentStateFilterMenuToggled: Boolean = false;
  isAttendanceFilterMenuToggled: Boolean = false;
  isExamFilterMenuToggled: Boolean = false;
  isModulesValidatedY1MenuToggled: Boolean = false;
  isModulesValidatedY2MenuToggled: Boolean = false;
  isUnix1FilterMenuToggled: Boolean = false;
  isUnix2FilterMenuToggled: Boolean = false;
  isAlgorithms1FilterMenuToggled: Boolean = false;
  isAlgorithms2FilterMenuToggled: Boolean = false;
  isGraphics1FilterMenuToggled: Boolean = false;
  isGraphics2FilterMenuToggled: Boolean = false;
  isPHPBootcampFilterMenuToggled: Boolean = false;
  isWebFilterMenuToggled: Boolean = false;
  isInternship1FilterMenuToggled: Boolean = false;
  isCPPBootcampFilterMenuToggled: Boolean = false;
  isCPPFilterMenuToggled: Boolean = false;
  isKernelFilterMenuToggled: Boolean = false;

  @HostListener('document:click', ['$event'])
  clickTracker(event) {
    const attendanceFilterRef = document.getElementById('attendanceFilter');
    const examFilterRef = document.getElementById('examFilter');
    const modulesValidatedY1Ref = document.getElementById('modulesValidatedY1Filter');
    const modulesValidatedY2Ref = document.getElementById('modulesValidatedY2Filter');
    const unix1FilterRef = document.getElementById('unix1Filter');
    const unix2FilterRef = document.getElementById('unix2Filter');
    const graphics1FilterRef = document.getElementById('graphics1Filter');
    const graphics2FilterRef = document.getElementById('graphics2Filter');
    const algorithms1FilterRef = document.getElementById('algorithms1Filter');
    const algorithms2FilterRef = document.getElementById('algorithms2Filter');
    const webFilterRef = document.getElementById('webFilter');
    const PHPBootcampFilterRef = document.getElementById('PHPBootcampFilter');
    const CPPBootcampFilterRef = document.getElementById('CPPBootcampFilter');
    const CPPFilterRef = document.getElementById('CPPFilter');
    const kernelFilterRef = document.getElementById('kernelFilter');
    const studentNameFilterRef = document.getElementById('studentNameFilter');
    const studentUsernameFilterRef = document.getElementById('studentUsernameFilter');
    const studentStateFilterRef = document.getElementById('studentStateFilter');
    const internship1FilterRef = document.getElementById('internship1Filter');
    const attendanceStickyFilterRef = document.getElementById('attendanceStickyFilter');
    const examStickyFilterRef = document.getElementById('examStickyFilter');
    const unix1StickyFilterRef = document.getElementById('unix1StickyFilter');
    const unix2StickyFilterRef = document.getElementById('unix2StickyFilter');
    const graphics1StickyFilterRef = document.getElementById('graphics1StickyFilter');
    const graphics2StickyFilterRef = document.getElementById('graphics2StickyFilter');
    const algorithms1StickyFilterRef = document.getElementById('algorithms1StickyFilter');
    const algorithms2StickyFilterRef = document.getElementById('algorithms2StickyFilter');
    const webStickyFilterRef = document.getElementById('webStickyFilter');
    const PHPBootcampStickyFilterRef = document.getElementById('PHPBootcampStickyFilter');
    const CPPBootcampStickyFilterRef = document.getElementById('CPPBootcampStickyFilter');
    const CPPStickyFilterRef = document.getElementById('CPPStickyFilter');
    const kernelStickyFilterRef = document.getElementById('kernelStickyFilter');
    const studentStickyNameFilterRef = document.getElementById('studentNameStickyFilter');
    const studentStickyUsernameFilterRef = document.getElementById('studentUsernameStickyFilter');
    const studentStickyStateFilterRef = document.getElementById('studentStateStickyFilter');
    const internship1StickyFilterRef = document.getElementById('internship1StickyFilter');
    if (!attendanceFilterRef.contains(event.target)) {
      this.isAttendanceFilterMenuToggled = false;
    }
    if (!studentNameFilterRef.contains(event.target)) {
      this.isStudentNameFilterMenuToggled = false;
    }
    if (!studentUsernameFilterRef.contains(event.target)) {
      this.isStudentUsernameFilterMenuToggled = false;
    }
    if (!studentStateFilterRef.contains(event.target)) {
      this.isStudentStateFilterMenuToggled = false;
    }
    if (!examFilterRef.contains(event.target)) {
      this.isExamFilterMenuToggled = false;
    }
    if (!modulesValidatedY1Ref.contains(event.target)) {
      this.isModulesValidatedY1MenuToggled = false;
    }
    if (!modulesValidatedY2Ref.contains(event.target)) {
      this.isModulesValidatedY2MenuToggled = false;
    }
    if (!unix1FilterRef.contains(event.target)) {
      this.isUnix1FilterMenuToggled = false;
    }
    if (!unix2FilterRef.contains(event.target)) {
      this.isUnix2FilterMenuToggled = false;
    }
    if (!graphics1FilterRef.contains(event.target)) {
      this.isGraphics1FilterMenuToggled = false;
    }
    if (!graphics2FilterRef.contains(event.target)) {
      this.isGraphics2FilterMenuToggled = false;
    }
    if (!algorithms1FilterRef.contains(event.target)) {
      this.isAlgorithms1FilterMenuToggled = false;
    }
    if (!algorithms2FilterRef.contains(event.target)) {
      this.isAlgorithms2FilterMenuToggled = false;
    }
    if (!CPPBootcampFilterRef.contains(event.target)) {
      this.isCPPBootcampFilterMenuToggled = false;
    }
    if (!CPPFilterRef.contains(event.target)) {
      this.isCPPFilterMenuToggled = false;
    }
    if (!webFilterRef.contains(event.target)) {
      this.isWebFilterMenuToggled = false;
    }
    if (!PHPBootcampFilterRef.contains(event.target)) {
      this.isPHPBootcampFilterMenuToggled = false;
    }
    if (!kernelFilterRef.contains(event.target)) {
      this.isKernelFilterMenuToggled = false;
    }
    if (!internship1FilterRef.contains(event.target)) {
      this.isInternship1FilterMenuToggled = false;
    }
    if (event.screenX < window.innerWidth / 2) {
      this.navService.screenSide = 'left';
    } else {
      this.navService.screenSide = 'right';
    }
  }

  constructor(
    private _router: Router,
    public studentService: StudentService,
    public navService: NavService
  ) { }

  ngOnInit() {
    this.studentService.getModules().subscribe(res => {
      this.navService.dashboardLoading.next(false);
      if (res.code !== 'S0002') {
        this.componentFailure = true;
        this.componentFailureMessage = res.message;
      } else {
        for (const module of res.data) {
          this.modules.push(module);
        }
        this.getAllUsers();
      }
    });
    this._router.events.subscribe(event => {
      this.users = [];
    });
  }

  getAllUsers() {
    this.studentService.visibleStudents = 0;
    return new Promise((resolve, reject) => {
      this.gettingAllUsers = true;
      this.users = [];
      this.loadingMoreStudents = true;
      this.studentService.getStudentReportsData((new Date()).getFullYear() - 2, 1, 1000).subscribe(res => {
        this.gettingAllUsers = false;
        this.loadingMoreStudents = false;
        this.loading = false;
        if (res.code !== 'S0005') {
          this.componentFailure = true;
          this.componentFailureMessage = res.message;
        } else {
          for (const user of res.data) {
            const newUser = new User(user);
            this.studentService.visibleStudents++;
            newUser.visible$ = new BehaviorSubject(true);
            newUser.filters = {
              filterByAttendance: false,
              filterByExam: false,
              filterByUnix1: false,
              filterByAlgorithms1: false,
              filterByGraphics1: false,
              filterByPHPBootcamp: false,
              filterByWeb: false,
              filterByAlgorithms2: false,
              filterByCPPBootcamp: false,
              filterByCPP: false,
              filterByUnix2: false,
              filterByKernel: false,
              filterBySecurity: false,
              filterByOcaml: false,
              filterBySwiftIos: false,
              filterByJavaAndroid: false,
              filterByUnity: false,
              filterByDevops: false,
              filterByNetwork: false,
              filterByVirus: false,
              filterByMath: false,
              filterByModulesValidatedY1: false,
              filterByModulesValidatedY2: false
            };
            this.users.push(newUser);
            for (const module of this.modules) {
              if (this.studentService.isModuleValidated(user, module, newUser.projects)) {
                switch (module.slug) {
                  case 'c-exam':
                    newUser.validatedCExam = true;
                    break;
                  case 'unix1':
                    newUser.validatedUnix1 = true;
                    newUser.validatedModulesY1++;
                    break;
                  case 'algo1':
                    newUser.validatedAlgorithms1 = true;
                    newUser.validatedModulesY1++;
                    break;
                  case 'algo2':
                    newUser.validatedAlgorithms2 = true;
                    newUser.validatedModulesY1++;
                    break;
                  case 'internship1':
                    newUser.validatedInternship1 = true;
                    break;
                  case 'graphics1':
                    newUser.validatedGraphics1 = true;
                    newUser.validatedModulesY1++;
                    break;
                  case 'web':
                    newUser.validatedWeb = true;
                    newUser.validatedModulesY1++;
                    break;
                  case 'graphics2':
                    newUser.validatedGraphics2 = true;
                    newUser.validatedModulesY1++;
                    break;
                  case 'unix2':
                    newUser.validatedUnix2 = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'cpp':
                    newUser.validatedCPP = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'kernel':
                    newUser.validatedKernel = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'social-tech-lab':
                    newUser.validatedSocialTechLab = true;
                    break;
                  case 'security':
                    newUser.validatedSecurity = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'ocaml':
                    newUser.validatedOcaml = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'unity':
                    newUser.validatedUnity = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'swift-ios':
                    newUser.validatedSwiftIos = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'java-android':
                    newUser.validatedJavaAndroid = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'devops':
                    newUser.validatedDevops = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'math':
                    newUser.validatedMath = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'network':
                    newUser.validatedNetwork = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'virus':
                    newUser.validatedVirus = true;
                    newUser.validatedModulesY2++;
                    break;
                  case 'rushes':
                    newUser.validatedrushes = true;
                    break;
                  case 'piscine-cpp':
                    newUser.validatedCPPBootcamp = true;
                    break;
                  case 'internship2':
                    newUser.validatedInternship2 = true;
                    break;
                  case 'piscine-php':
                    newUser.validatedPHPBootcamp = true;
                    if (newUser.year === 2016) {
                      newUser.validatedModulesY1++;
                    }
                    break;
                  case 'piscine-c':
                    newUser.validatedCBootcamp = true;
                    break;
                  default:
                    break;
                }
              }
            }
          }
          this.gotAllUsers = true;
          resolve(true);
        }
      });
    });
  }

  initCSVDownload() {
    const csvObject = [];
    csvObject.push({
      1: 'Username',
      2: 'Name',
      3: 'Attendance',
      4: 'C Exam',
      5: 'Unix 1',
      6: 'Algo 1',
      7: 'Graphics 1',
      8: 'PHP Bootcamp',
      9: 'Web',
      10: 'Graphics 2',
      11: 'Internship 1',
      12: 'CPP Bootcamp',
      13: 'CPP',
      14: 'Kernel',
      15: 'Unix 2',
      16: 'Social Tech Lab',
      17: 'Security',
      18: 'OCAML',
      19: 'Swift / iOS',
      20: 'Unity',
      21: 'Java / Android',
      22: 'DevOps',
      23: 'Math',
      24: 'Network',
      25: 'Virus',
      26: 'Modules Validated Y1',
      27: 'Modules Validated Y2'
    });
    for (const user of this.users) {
      let attendance = 0;
      this.studentService.isAttendanceCompliantLastMonth(user).subscribe(res => attendance = res);
      if (user.visible$.getValue() === true) {
        const userObject = {
          username: user.username,
          name: user.firstName + ' ' + user.lastName,
          attendance: attendance,
          c_exam: user.validatedCExam ? 1 : 0,
          unix1: user.validatedUnix1 ? 1 : 0,
          algo1: user.validatedAlgorithms1 ? 1 : 0,
          graphics1: user.validatedGraphics1 ? 1 : 0,
          phpBootcamp: user.validatedPHPBootcamp ? 1 : 0,
          web: user.validatedWeb ? 1 : 0,
          graphics2: user.validatedGraphics2 ? 1 : 0,
          internship1: user.validatedInternship1 ? 1 : 0,
          cppBootcamp: user.validatedCPPBootcamp ? 1 : 0,
          cpp: user.validatedCPP ? 1 : 0,
          kernel: user.validatedKernel ? 1 : 0,
          unix2: user.validatedUnix2 ? 1 : 0,
          socialTechLab: user.validatedSocialTechLab ? 1 : 0,
          security: user.validatedSecurity ? 1 : 0,
          ocaml: user.validatedOcaml ? 1 : 0,
          swiftIos: user.validatedSwiftIos ? 1 : 0,
          unity: user.validatedUnity ? 1 : 0,
          javaAndroid: user.validatedJavaAndroid ? 1 : 0,
          devops: user.validatedDevops ? 1 : 0,
          math: user.validatedMath ? 1 : 0,
          network: user.validatedNetwork ? 1 : 0,
          virus: user.validatedVirus ? 1 : 0,
          modulesValidatedY1: user.validatedModulesY1,
          modulesValidatedY2: user.validatedModulesY2,
        };
        csvObject.push(userObject);
      }
    }
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false
    };
    const CSV = new Angular2Csv(csvObject, 'year-2-report (' + new Date().getDate() + '-' + new Date().getMonth() + '-' + new Date().getFullYear() + ')', options);
    this.gotAllUsers = true;
  }

  downloadAsCSV() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.initCSVDownload();
      });
    } else {
      this.initCSVDownload();
    }
  }

  sortByAttendance(type) {
    this.studentService.attendanceSort = type;
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.studentService.sortByAttendanceArray(this.users, type);
      });
    } else {
      this.studentService.sortByAttendanceArray(this.users, type);
    }
  }

  sortByModulesValidatedY1(type) {
    this.studentService.modulesValidatedY1Sort = type;
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.studentService.sortByModulesValidatedY1Array(this.users, type);
      });
    } else {
      this.studentService.sortByModulesValidatedY1Array(this.users, type);
    }
  }

  sortByModulesValidatedY2(type) {
    this.studentService.modulesValidatedY2Sort = type;
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.studentService.sortByModulesValidatedY2Array(this.users, type);
      });
    } else {
      this.studentService.sortByModulesValidatedY2Array(this.users, type);
    }
  }

  sortByName(type) {
    this.studentService.studentNameSort = type;
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.studentService.sortByNameArray(this.users, type);
      });
    } else {
      this.studentService.sortByNameArray(this.users, type);
    }
  }

  scrollSticky(event) {
      const stickyElement = document.getElementById('sticky-student-performance-head');
      stickyElement.scrollLeft = event.target.scrollLeft;
  }

  toggleStudentNameFilter(user) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleStudentNameFilter(this.users, user);
      });
    } else {
      this.studentService.toggleStudentNameFilter(this.users, user);
    }
  }

  toggleStudentUsernameFilter(user) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleStudentUsernameFilter(this.users, user);
      });
    } else {
      this.studentService.toggleStudentUsernameFilter(this.users, user);
    }
  }

  toggleModulesValidatedY1Filter(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleModulesValidatedY1Filter(this.users, value);
      });
    } else {
      this.studentService.toggleModulesValidatedY1Filter(this.users, value);
    }
  }

  toggleModulesValidatedY2Filter(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleModulesValidatedY2Filter(this.users, value);
      });
    } else {
      this.studentService.toggleModulesValidatedY2Filter(this.users, value);
    }
  }

  toggleAllName(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterName(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterName(this.users, value);
    }
  }

  toggleAllUsername(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterUsername(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterUsername(this.users, value);
    }
  }

  toggleAllState(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterState(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterState(this.users, value);
    }
  }

  toggleAllAttendance(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterAttendance(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterAttendance(this.users, value);
    }
  }

  toggleAllExam(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterExam(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterExam(this.users, value);
    }
  }

  toggleAllModulesValidatedY1(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterModulesValidatedY1(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterModulesValidatedY1(this.users, value);
    }
  }

  toggleAllModulesValidatedY2(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterModulesValidatedY2(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterModulesValidatedY2(this.users, value);
    }
  }

  toggleAllUnix1(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterUnix1(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterUnix1(this.users, value);
    }
  }

  toggleAllUnix2(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterUnix2(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterUnix2(this.users, value);
    }
  }

  toggleAllAlgorithms1(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterAlgorithms1(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterAlgorithms1(this.users, value);
    }
  }

  toggleAllAlgorithms2(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterAlgorithms2(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterAlgorithms2(this.users, value);
    }
  }

  toggleAllGraphics1(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterGraphics1(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterGraphics1(this.users, value);
    }
  }

  toggleAllGraphics2(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterGraphics2(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterGraphics2(this.users, value);
    }
  }

  toggleAllPHPBootcamp(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterPHPBootcamp(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterPHPBootcamp(this.users, value);
    }
  }

  toggleAllWeb(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterWeb(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterWeb(this.users, value);
    }
  }

  toggleAllCPPBootcamp(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterCPPBootcamp(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterCPPBootcamp(this.users, value);
    }
  }

  toggleAllCPP(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterCPP(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterCPP(this.users, value);
    }
  }

  toggleAllKernel(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterKernel(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterKernel(this.users, value);
    }
  }

  toggleAllInternship1(value) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAllFilterInternship1(this.users, value);
      });
    } else {
      this.studentService.toggleAllFilterInternship1(this.users, value);
    }
  }

  toggleStateFilter(filters) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleStateFilter(this.users, filters);
      });
    } else {
      this.studentService.toggleStateFilter(this.users, filters);
    }
  }

  toggleAttendanceFilter(day) {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAttendanceFilter(this.users, day);
      });
    } else {
      this.studentService.toggleAttendanceFilter(this.users, day);
    }
  }

  toggleExamFilter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleExamFilter(this.users);
      });
    } else {
      this.studentService.toggleExamFilter(this.users);
    }
  }

  toggleInternship1Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleInternship1Filter(this.users);
      });
    } else {
      this.studentService.toggleInternship1Filter(this.users);
    }
  }

  toggleUnix1Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleUnix1Filter(this.users);
      });
    } else {
      this.studentService.toggleUnix1Filter(this.users);
    }
  }

  toggleUnix2Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleUnix2Filter(this.users);
      });
    } else {
      this.studentService.toggleUnix2Filter(this.users);
    }
  }

  toggleGraphics1Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleGraphics1Filter(this.users);
      });
    } else {
      this.studentService.toggleGraphics1Filter(this.users);
    }
  }

  toggleGraphics2Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleGraphics2Filter(this.users);
      });
    } else {
      this.studentService.toggleGraphics2Filter(this.users);
    }
  }

  toggleAlgorithms1Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAlgorithms1Filter(this.users);
      });
    } else {
      this.studentService.toggleAlgorithms1Filter(this.users);
    }
  }

  toggleAlgorithms2Filter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleAlgorithms2Filter(this.users);
      });
    } else {
      this.studentService.toggleAlgorithms2Filter(this.users);
    }
  }

  toggleCPPBootcampFilter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleCPPBootcampFilter(this.users);
      });
    } else {
      this.studentService.toggleCPPBootcampFilter(this.users);
    }
  }

  toggleCPPFilter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleCPPFilter(this.users);
      });
    } else {
      this.studentService.toggleCPPFilter(this.users);
    }
  }

  togglePHPBootcampFilter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.togglePHPBootcampFilter(this.users);
      });
    } else {
      this.studentService.togglePHPBootcampFilter(this.users);
    }
  }

  toggleWebFilter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleWebFilter(this.users);
      });
    } else {
      this.studentService.toggleWebFilter(this.users);
    }
  }

  toggleKernelFilter() {
    if (!this.gotAllUsers) {
      this.getAllUsers().then(res => {
        this.gettingAllUsers = false;
        this.studentService.toggleKernelFilter(this.users);
      });
    } else {
      this.studentService.toggleKernelFilter(this.users);
    }
  }

}
