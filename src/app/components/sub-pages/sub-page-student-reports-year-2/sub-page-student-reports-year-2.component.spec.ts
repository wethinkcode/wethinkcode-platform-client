import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageStudentReportsYear2Component } from './sub-page-student-reports-year-2.component';

describe('SubPageStudentReportsYear2Component', () => {
  let component: SubPageStudentReportsYear2Component;
  let fixture: ComponentFixture<SubPageStudentReportsYear2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageStudentReportsYear2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageStudentReportsYear2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
