import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UserService } from '../../../services/user.service';
import { NotificationService } from '../../../services/notification.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { NavService } from '../../../services/nav.service';
import {User} from "../../../models/user";
import set = Reflect.set;
import {StudentService} from "../../../services/student.service";
import {Student} from "../../../models/student";

@Component({
  selector: 'app-sub-page-user',
  templateUrl: './sub-page-user.component.html',
  styleUrls: ['./sub-page-user.component.scss']
})
export class SubPageUserComponent implements OnInit {
  loading: Boolean = true;
  studentLoading: Boolean = false;
  componentFailure: Boolean = false;
  resultMessage: Subject<any>;
  level = 0;
  countLevel: any = 0.01;


  constructor(
    private _titleService: Title,
    private _activatedRoute: ActivatedRoute,
    private _notificationService: NotificationService,
    public authenticationService: AuthenticationService,
    public userService: UserService,
    public studentService: StudentService,
    public navService: NavService
  ) {
    this.resultMessage = new Subject();
  }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading user module...';
    this._titleService.setTitle('WeThinkCode_ | User');
    if (this._activatedRoute.snapshot.params.id) {
      this.userService.getUser(this._activatedRoute.snapshot.params.id).subscribe(res => {
        this.navService.dashboardLoading.next(false);
        this.loading = false;
        if (res.code === 'S0003') {
          if (!res.data) {
            this.componentFailure = true;
            this.resultMessage.next('That user does not exist.');
          } else {
            if (!res.cover_image) {
              res.cover_image = '/assets/images/default-user-cover.jpg';
            }
            if (res.data.role === 'student') {
              this.userService.selectedUser = new Student(res.data);
            } else {
              this.userService.selectedUser = new User(res.data);
            }
            if (this.userService.selectedUser.role === 'student') {
              this.studentLoading = true;
              this.studentService.getStudentData(this.userService.selectedUser.id).subscribe(resStudent => {
                if (resStudent.data.cohort) {
                  this.userService.selectedUser.startDate = new Date(resStudent.data.cohort.start_date);
                  this.userService.selectedUser.getCurrentMonth();
                }
                if (resStudent.data.exams) {
                  for (const exam of resStudent.data.exams) {
                    this.userService.selectedUser.exams.push(exam);
                  }
                  this.userService.selectedUser.getExams(resStudent.data);
                }
                if (resStudent.status !== 'success') {
                  this.componentFailure = true;
                  this.resultMessage = resStudent.message;
                } else {
                  this.userService.selectedUser.level = resStudent.data.level;
                  this.countLevelInit();
                }
              });
            }
            this._titleService.setTitle('WeThinkCode_ | ' + this.userService.selectedUser.firstName + ' ' + this.userService.selectedUser.lastName);
            if (!this.userService.selectedUser.event) {
              this.userService.selectedUser.initialEvent = new BehaviorSubject('getAttendanceData');
            }
          }
        } else {
          this._notificationService.newNotification({type: 'once-off', content: 'Could not retrieve user.', classification: 'error' });
        }
      });
    }
  }

  calcRotationFirstHalf() {
    const string = this.userService.selectedUser.level.toPrecision(3).split('.')[1];
    return (parseInt(string,10) > 50) ? ((360 * (parseInt(string,10) / 100)) / 2) : 0;
  }

  calcRotationSecondHalf() {
    const string = this.userService.selectedUser.level.toPrecision(3).split('.')[1];
    return (parseInt(string,10) > 50) ? 180 : (360 * (parseInt(string,10) / 100));
  }

  countLevelInit() {
    setTimeout(() => {
      if (this.countLevel < this.userService.selectedUser.level) {
        this.countLevel += 0.01;
        this.countLevelInit();
      }
    }, 0.1);
  }
}
