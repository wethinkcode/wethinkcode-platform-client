import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageSearchComponent } from './sub-page-search.component';

describe('SubPageSearchComponent', () => {
  let component: SubPageSearchComponent;
  let fixture: ComponentFixture<SubPageSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
