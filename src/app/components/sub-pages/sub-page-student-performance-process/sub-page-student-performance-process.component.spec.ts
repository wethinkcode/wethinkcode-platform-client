import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageStudentPerformanceProcessComponent } from './sub-page-student-performance-process.component';

describe('SubPageStudentPerformanceProcessComponent', () => {
  let component: SubPageStudentPerformanceProcessComponent;
  let fixture: ComponentFixture<SubPageStudentPerformanceProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageStudentPerformanceProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageStudentPerformanceProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
