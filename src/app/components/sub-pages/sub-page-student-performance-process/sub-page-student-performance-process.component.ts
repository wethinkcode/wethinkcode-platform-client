import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { SubPage } from '../../../abstract-classes/sub-page';

import { StudentPerformanceService } from '../../../services/student-performance.service';
import { CohortService } from '../../../services/cohort.service';
import { NavService } from '../../../services/nav.service';

import { Cohort } from '../../../models/cohort';

@Component({
  selector: 'app-sub-page-student-performance-process',
  templateUrl: './sub-page-student-performance-process.component.html',
  styleUrls: ['./sub-page-student-performance-process.component.scss']
})
export class SubPageStudentPerformanceProcessComponent extends SubPage implements OnInit {
  currentStep: any = 0;
  isFilterMenuToggled: Boolean = false;
  filter = {
    tags: {
      nonPerformer: false,
      lowPerformer: false,
      performer: false,
      highPerformer: false,
      internship1Ready: false,
      year2Ready: false,
      internship2Ready: false,
      exempt: false
    },
    state: {
      active: false,
      inactive: false,
      awol: false,
      employed: false,
      terminated: false,
    },
    warningCount: {
      0: false,
      1: false,
      2: false,
      3: false,
      4: false
    }
  };

  constructor(
    private _elementRef: ElementRef,
    private _titleService: Title,
    private _activatedRoute: ActivatedRoute,
    public cohortService: CohortService,
    public navService: NavService,
  ) {
    super();
  }

  findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
  }

  @HostListener('document:click', ['$event'])
  public onClick(event) {
    const clickedInside = this._elementRef.nativeElement.contains(event.target);
    if (!clickedInside || (!event.target.classList.contains('student-performance-filter') && !this.findAncestor(event.target, 'student-performance-filter'))) {
      this.isFilterMenuToggled = false;
    }
  }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Loading Student Performance...');
    if (this._activatedRoute.snapshot.params.id) {
      this.loading = true;
      this.cohortService.getCohort(this._activatedRoute.snapshot.params.id).subscribe(res => {
        this.navService.dashboardLoading.next(false);
        this._titleService.setTitle('WeThinkCode_ | Student Performance: ' + this._activatedRoute.snapshot.params.id);
        this.loading = false;
        if (res.status !== 'success') {
          this.failure = true;
          this.resultMessage = res.message;
        } else {
          this.cohortService.selectedCohort = new Cohort(res.data);
        }
      });
      this.navService.dashboardStatus = 'Loading student performance: ' + this._activatedRoute.snapshot.params.id;
    }
  }

  updateStep(event) {
    this.currentStep = event.order;
  }

  showAllTags() {
    this.filter.tags.nonPerformer = false;
    this.filter.tags.lowPerformer = false;
    this.filter.tags.performer = false;
    this.filter.tags.highPerformer = false;
    this.filter.tags.internship1Ready = false;
    this.filter.tags.year2Ready = false;
    this.filter.tags.internship2Ready = false;
    this.filter.tags.exempt = false;
  }

  hideAllTags() {
    this.filter.tags.nonPerformer = true;
    this.filter.tags.lowPerformer = true;
    this.filter.tags.performer = true;
    this.filter.tags.highPerformer = true;
    this.filter.tags.internship1Ready = true;
    this.filter.tags.year2Ready = true;
    this.filter.tags.internship2Ready = true;
    this.filter.tags.exempt = true;
  }

  showAllStates() {
    this.filter.state.active = false;
    this.filter.state.inactive = false;
    this.filter.state.awol = false;
    this.filter.state.employed = false;
    this.filter.state.terminated = false;
  }

  hideAllStates() {
    this.filter.state.active = true;
    this.filter.state.inactive = true;
    this.filter.state.awol = true;
    this.filter.state.employed = true;
    this.filter.state.terminated = true;
  }
}
