import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageInternsComponent } from './sub-page-interns.component';

describe('SubPageInternsComponent', () => {
  let component: SubPageInternsComponent;
  let fixture: ComponentFixture<SubPageInternsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageInternsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageInternsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
