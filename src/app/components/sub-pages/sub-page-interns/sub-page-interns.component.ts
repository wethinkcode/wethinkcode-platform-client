import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-interns',
  templateUrl: './sub-page-interns.component.html',
  styleUrls: ['./sub-page-interns.component.scss']
})
export class SubPageInternsComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Interns');
  }

}
