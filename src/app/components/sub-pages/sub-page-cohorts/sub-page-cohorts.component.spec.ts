import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCohortsComponent } from './sub-page-cohorts.component';

describe('SubPageCohortsComponent', () => {
  let component: SubPageCohortsComponent;
  let fixture: ComponentFixture<SubPageCohortsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCohortsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCohortsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
