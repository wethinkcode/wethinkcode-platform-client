import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { NavService } from '../../../services/nav.service';
import { CohortService } from '../../../services/cohort.service';
import {Cohort} from "../../../models/cohort";

@Component({
  selector: 'app-sub-page-cohorts',
  templateUrl: './sub-page-cohorts.component.html',
  styleUrls: ['./sub-page-cohorts.component.scss']
})
export class SubPageCohortsComponent implements OnInit {
  loading: Boolean = false;
  componentFailure: Boolean = false;
  resultMessage: String = '';
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(
    private _titleService: Title,
    public navService: NavService,
    public cohortService: CohortService
  ) {}

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading Cohorts module...';
    this._titleService.setTitle('WeThinkCode_ | Cohorts');
    this.loading = true;
    this.cohortService.getCohorts(1, 100).subscribe(res => {
      this.loading = false;
      if (res.status !== 'success') {
        this.componentFailure = true;
      } else {
        this.cohortService.cohorts = [];
        for (const cohort of res.data) {
          this.cohortService.cohorts.push(new Cohort(cohort));
        }
      }
      this.resultMessage = res.message;
    });
    this.navService.dashboardLoading.next(false);
  }

}
