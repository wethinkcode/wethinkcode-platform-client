import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-support',
  templateUrl: './sub-page-support.component.html',
  styleUrls: ['./sub-page-support.component.scss']
})
export class SubPageSupportComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Support');
  }

}
