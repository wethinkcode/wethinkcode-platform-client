import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageSupportComponent } from './sub-page-support.component';

describe('SubPageSupportComponent', () => {
  let component: SubPageSupportComponent;
  let fixture: ComponentFixture<SubPageSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageSupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
