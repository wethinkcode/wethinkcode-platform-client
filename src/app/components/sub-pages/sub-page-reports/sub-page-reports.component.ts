import { Component, OnInit } from '@angular/core';
import { NavService } from '../../../services/nav.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-reports',
  templateUrl: './sub-page-reports.component.html',
  styleUrls: ['./sub-page-reports.component.scss']
})
export class SubPageReportsComponent implements OnInit {

  constructor(
    public navService: NavService,
    private _titleService: Title
  ) { }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading reports module...';
    this._titleService.setTitle('WeThinkCode_ | Reports');
    this.navService.dashboardLoading.next(false);
  }

}
