import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageReportsComponent } from './sub-page-reports.component';

describe('SubPageReportsComponent', () => {
  let component: SubPageReportsComponent;
  let fixture: ComponentFixture<SubPageReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
