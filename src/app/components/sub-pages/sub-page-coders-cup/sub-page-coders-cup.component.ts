import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-coders-cup',
  templateUrl: './sub-page-coders-cup.component.html',
  styleUrls: ['./sub-page-coders-cup.component.scss']
})
export class SubPageCodersCupComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Coders Cup');
  }

}
