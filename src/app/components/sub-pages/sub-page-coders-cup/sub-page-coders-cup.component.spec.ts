import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCodersCupComponent } from './sub-page-coders-cup.component';

describe('SubPageCodersCupComponent', () => {
  let component: SubPageCodersCupComponent;
  let fixture: ComponentFixture<SubPageCodersCupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCodersCupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCodersCupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
