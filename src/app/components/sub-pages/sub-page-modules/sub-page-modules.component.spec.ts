import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageModulesComponent } from './sub-page-modules.component';

describe('SubPageModulesComponent', () => {
  let component: SubPageModulesComponent;
  let fixture: ComponentFixture<SubPageModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
