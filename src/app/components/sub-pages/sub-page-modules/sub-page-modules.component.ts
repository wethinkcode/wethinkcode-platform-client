import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';


import { ModulesService } from '../../../services/modules.service';
import { NavService } from '../../../services/nav.service';

import { Module } from '../../../models/module';

@Component({
  selector: 'app-sub-page-modules',
  templateUrl: './sub-page-modules.component.html',
  styleUrls: ['./sub-page-modules.component.scss']
})
export class SubPageModulesComponent implements OnInit {
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: String = '';

  constructor(
    private _router: Router,
    private _titleService: Title,
    public navService: NavService,
    public moduleService: ModulesService,
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Modules');
    this._router.events.subscribe(event => {
      this.moduleService.modules = [];
    });
    this.moduleService.getMoreModules().subscribe(res => {
      this.loading = false;
      if (res.code !== 'S0002') {
        this.componentFailure = true;
        this.resultMessage = 'We could not load the modules you requested. Please reload your page.';
      } else {
        for (const module of res.data) {
          this.moduleService.modules.push(new Module(module));
        }
        this.moduleService.modules.sort((a, b) => {if (a.title > b.title) { return 1; } else if (a.title < b.title) { return -1; } else { return 0; }});
      }
    });
  }
}
