import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageNotificationsComponent } from './sub-page-notifications.component';

describe('SubPageNotificationsComponent', () => {
  let component: SubPageNotificationsComponent;
  let fixture: ComponentFixture<SubPageNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
