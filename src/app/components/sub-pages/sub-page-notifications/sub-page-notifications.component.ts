import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-notifications',
  templateUrl: './sub-page-notifications.component.html',
  styleUrls: ['./sub-page-notifications.component.scss']
})
export class SubPageNotificationsComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Notifications');
  }

}
