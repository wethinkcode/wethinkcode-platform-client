import {Component, HostListener, OnInit} from '@angular/core';
import {NavService} from '../../../services/nav.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-student-reports',
  templateUrl: './sub-page-student-reports.component.html',
  styleUrls: ['./sub-page-student-reports.component.scss'],
})
export class SubPageStudentReportsComponent implements OnInit {

  constructor(
    public navService: NavService,
    private _titleService: Title
  ) { }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading student reports...';
  }
}
