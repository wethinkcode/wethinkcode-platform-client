import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageStudentReportsComponent } from './sub-page-student-reports.component';

describe('SubPageStudentReportsComponent', () => {
  let component: SubPageStudentReportsComponent;
  let fixture: ComponentFixture<SubPageStudentReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageStudentReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageStudentReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
