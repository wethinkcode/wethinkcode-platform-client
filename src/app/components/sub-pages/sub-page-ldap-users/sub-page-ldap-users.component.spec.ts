import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageLdapUsersComponent } from './sub-page-ldap-users.component';

describe('SubPageLdapUsersComponent', () => {
  let component: SubPageLdapUsersComponent;
  let fixture: ComponentFixture<SubPageLdapUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageLdapUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageLdapUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
