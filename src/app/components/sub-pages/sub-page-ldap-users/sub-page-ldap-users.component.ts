import { Component, OnInit } from '@angular/core';

import { NavService } from '../../../services/nav.service';

import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-ldap-users',
  templateUrl: './sub-page-ldap-users.component.html',
  styleUrls: ['./sub-page-ldap-users.component.scss']
})
export class SubPageLdapUsersComponent implements OnInit {

  constructor(
    public navService: NavService,
    private _titleService: Title
  ) {
    this.navService.dashboardStatus = 'Loading LDAP users module...';
    this._titleService.setTitle('WeThinkCode_ | LDAP users');
    this.navService.dashboardLoading.next(false);
  }

  ngOnInit() {
  }

}
