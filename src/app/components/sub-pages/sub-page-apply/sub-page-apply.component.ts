import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-apply',
  templateUrl: './sub-page-apply.component.html',
  styleUrls: ['./sub-page-apply.component.scss']
})
export class SubPageApplyComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Apply');
  }
}
