import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageApplyComponent } from './sub-page-apply.component';

describe('SubPageApplyComponent', () => {
  let component: SubPageApplyComponent;
  let fixture: ComponentFixture<SubPageApplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageApplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageApplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
