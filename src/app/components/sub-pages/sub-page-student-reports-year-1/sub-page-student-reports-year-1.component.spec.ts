import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageStudentReportsYear1Component } from './sub-page-student-reports-year-1.component';

describe('SubPageStudentReportsYear1Component', () => {
  let component: SubPageStudentReportsYear1Component;
  let fixture: ComponentFixture<SubPageStudentReportsYear1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageStudentReportsYear1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageStudentReportsYear1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
