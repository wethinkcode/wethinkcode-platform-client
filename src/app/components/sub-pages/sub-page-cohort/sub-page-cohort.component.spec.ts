import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCohortComponent } from './sub-page-cohort.component';

describe('SubPageCohortComponent', () => {
  let component: SubPageCohortComponent;
  let fixture: ComponentFixture<SubPageCohortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCohortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCohortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
