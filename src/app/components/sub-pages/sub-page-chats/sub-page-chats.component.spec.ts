import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageChatsComponent } from './sub-page-chats.component';

describe('SubPageChatsComponent', () => {
  let component: SubPageChatsComponent;
  let fixture: ComponentFixture<SubPageChatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageChatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageChatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
