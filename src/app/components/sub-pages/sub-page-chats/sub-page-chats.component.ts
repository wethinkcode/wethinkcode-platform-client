import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-chats',
  templateUrl: './sub-page-chats.component.html',
  styleUrls: ['./sub-page-chats.component.scss']
})
export class SubPageChatsComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Chats');
  }

}
