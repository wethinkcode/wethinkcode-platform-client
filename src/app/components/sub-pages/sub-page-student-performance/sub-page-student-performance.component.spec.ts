import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageStudentPerformanceComponent } from './sub-page-student-performance.component';

describe('SubPageStudentPerformanceComponent', () => {
  let component: SubPageStudentPerformanceComponent;
  let fixture: ComponentFixture<SubPageStudentPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageStudentPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageStudentPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
