import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Cohort } from '../../../models/cohort';

import { StudentService } from '../../../services/student.service';
import { NavService } from '../../../services/nav.service';
import { CohortService } from '../../../services/cohort.service';

@Component({
  selector: 'app-sub-page-student-performance',
  templateUrl: './sub-page-student-performance.component.html',
  styleUrls: ['./sub-page-student-performance.component.scss']
})
export class SubPageStudentPerformanceComponent implements OnInit {
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: String = '';
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(
    private _titleService: Title,
    private _router: Router,
    public studentService: StudentService,
    public navService: NavService,
    public cohortService: CohortService
  ) {}

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading student performance module...';
    this._titleService.setTitle('WeThinkCode_ | Student Performance');
    this.loading = true;
    this.cohortService.getCohorts(1, 100).subscribe(res => {
      this.loading = false;
      if (res.status !== 'success') {
        this.componentFailure = true;
      } else {
        this.cohortService.cohorts = [];
        for (const cohort of res.data) {
          this.cohortService.cohorts.push(new Cohort(cohort));
        }
      }
      this.resultMessage = res.message;
    });
    this.navService.dashboardLoading.next(false);
  }


}
