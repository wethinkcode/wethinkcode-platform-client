import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageProjectComponent } from './sub-page-project.component';

describe('SubPageProjectComponent', () => {
  let component: SubPageProjectComponent;
  let fixture: ComponentFixture<SubPageProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
