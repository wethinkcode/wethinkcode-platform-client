import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { Project } from '../../../models/project';

import { AuthenticationService } from '../../../services/authentication.service';
import { ProjectService } from '../../../services/project.service';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-sub-page-project',
  templateUrl: './sub-page-project.component.html',
  styleUrls: ['./sub-page-project.component.scss']
})
export class SubPageProjectComponent implements OnInit {
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: String = '';

  constructor(
    public authenticationService: AuthenticationService,
    public navService: NavService,
    public projectService: ProjectService,
    private _activatedRoute: ActivatedRoute,
    private _titleService: Title,
  ) {
  }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading project: ' + this._activatedRoute.snapshot.params.id;
    this._titleService.setTitle('WeThinkCode_ | Loading project');
      if (this._activatedRoute.snapshot.params.id) {
      this.projectService.getProject(this._activatedRoute.snapshot.params.id).subscribe(res => {
        this.navService.dashboardLoading.next(false);
        this.loading = false;
        if (res.code !== 'S0003') {
          this.componentFailure = true;
          this.resultMessage = res.message;
        } else {
          this.projectService.selectedProject = new Project(res.data);
          this._titleService.setTitle('WeThinkCode_ | Projects: ' + this.projectService.selectedProject.title);
        }
      });
    } else {
      this.navService.dashboardLoading.next(false);
      this.componentFailure = true;
      this.resultMessage = 'A project identifier is required.';
    }
  }
}
