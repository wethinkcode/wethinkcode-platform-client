import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from '../../../services/user.service';
import { NotificationService } from '../../../services/notification.service';
import {NavService} from "../../../services/nav.service";
import {ToolService} from "../../../services/tool.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-sub-page-users',
  templateUrl: './sub-page-users.component.html',
  styleUrls: ['./sub-page-users.component.scss']
})
export class SubPageUsersComponent implements OnInit {
  usersPage = 1;
  searchParam = '';
  users: Array<any> = [];
  loading: Boolean = true;
  loadingMoreUsers: Boolean = false;

  constructor(
    public navService: NavService,
    public toolService: ToolService,
    public authenticationService: AuthenticationService,
    private _titleService: Title,
    private _userService: UserService,
    private _router: Router,
    private _notificationService: NotificationService,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Users');
    this.navService.dashboardStatus = 'Loading users module...';
    this._activatedRoute.queryParams
      .subscribe(params => {
        if (params['search']) {
          this.searchParam = decodeURI(params.search);
          this.users = [];
        }
        this.getUsers(this.usersPage);
      });
    this._router.events.subscribe(event => {
      this.users = [];
    });
  }

  getUsers(page) {
    this._userService.getUsers(page, this.searchParam).subscribe(res => {
      this.loadingMoreUsers = false;
      this.navService.dashboardLoading.next(false);
      this.loading = false;
      if (res.code !== 'S0005') {
        this._notificationService.newNotification({type: 'once-off', content: res.message});
      } else {
        for (const user of res.data) {
          if (!user.username) {
            user.username = '--';
            user.identifier = user.id;
          } else {
            user.identifier = user.username;
          }
          this.users.push(user);
        }
      }
    });
  }

  getMoreUsers() {
    this.loadingMoreUsers = true;
    this.usersPage++;
    this.getUsers(this.usersPage);
  }

}
