import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserService } from '../../../services/user.service';
import { NavService } from '../../../services/nav.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-sub-page-dashboard',
  templateUrl: './sub-page-dashboard.component.html',
  styleUrls: ['./sub-page-dashboard.component.scss']
})
export class SubPageDashboardComponent implements OnInit {

  constructor(
    private _titleService: Title,
    public userService: UserService,
    public navService: NavService,
    public notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading your dashboard...';
    this.navService.dashboardLoading.next(false);
    this._titleService.setTitle('WeThinkCode_ | My Dashboard');
  }

}
