import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-internships',
  templateUrl: './sub-page-internships.component.html',
  styleUrls: ['./sub-page-internships.component.scss']
})
export class SubPageInternshipsComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Internships');
  }

}
