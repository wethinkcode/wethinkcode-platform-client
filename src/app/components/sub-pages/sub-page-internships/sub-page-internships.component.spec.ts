import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageInternshipsComponent } from './sub-page-internships.component';

describe('SubPageInternshipsComponent', () => {
  let component: SubPageInternshipsComponent;
  let fixture: ComponentFixture<SubPageInternshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageInternshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageInternshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
