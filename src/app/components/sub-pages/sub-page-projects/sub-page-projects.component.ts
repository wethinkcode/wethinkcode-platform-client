import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { Project } from '../../../models/project';

import { AuthenticationService } from '../../../services/authentication.service';
import { ProjectService } from '../../../services/project.service';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-sub-page-projects',
  templateUrl: './sub-page-projects.component.html',
  styleUrls: ['./sub-page-projects.component.scss']
})
export class SubPageProjectsComponent implements OnInit {
  page: Number = 1;
  limit: Number = 100;
  loading: Boolean = true;
  componentFailure: Boolean = false;
  resultMessage: String = '';

  constructor(
    private _router: Router,
    private _titleService: Title,
    public authenticationService: AuthenticationService,
    public projectService: ProjectService,
    public navService: NavService
  ) {
  }

  ngOnInit() {
    this.navService.dashboardStatus = 'Loading projects module...';
    this._titleService.setTitle('WeThinkCode_ | Projects');
    this._router.events.subscribe(event => {
      this.projectService.projects = [];
    });
    this.projectService.getMoreProjects(this.page, this.limit).subscribe(res => {
      this.navService.dashboardLoading.next(false);
      this.loading = false;
      if (res.code !== 'S0002') {
        this.componentFailure = true;
        this.resultMessage = 'We could not load the projects you requested. Please reload your page.';
      } else {
        for (const project of res.data) {
          const newProject = new Project(project);
          this.projectService.projects.push(newProject);
        }
        this.projectService.projects.sort((a, b) => {if (a.title > b.title) { return 1; } else if (a.title < b.title) { return -1; } else { return 0; }});
      }
    });
  }

  getProjectIdentifier(project) {
    return project.slug ? project.slug : project.id;
  }
}
