import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageProjectsComponent } from './sub-page-projects.component';

describe('SubPageProjectsComponent', () => {
  let component: SubPageProjectsComponent;
  let fixture: ComponentFixture<SubPageProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
