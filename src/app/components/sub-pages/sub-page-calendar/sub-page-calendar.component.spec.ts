import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCalendarComponent } from './sub-page-calendar.component';

describe('SubPageCalendarComponent', () => {
  let component: SubPageCalendarComponent;
  let fixture: ComponentFixture<SubPageCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
