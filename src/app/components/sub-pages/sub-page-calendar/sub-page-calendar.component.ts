import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-calendar',
  templateUrl: './sub-page-calendar.component.html',
  styleUrls: ['./sub-page-calendar.component.scss']
})
export class SubPageCalendarComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Calendar');
  }

}
