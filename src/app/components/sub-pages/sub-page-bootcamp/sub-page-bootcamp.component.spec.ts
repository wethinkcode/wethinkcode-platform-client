import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageBootcampComponent } from './sub-page-bootcamp.component';

describe('SubPageBootcampComponent', () => {
  let component: SubPageBootcampComponent;
  let fixture: ComponentFixture<SubPageBootcampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageBootcampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageBootcampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
