import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { NavService } from '../../../services/nav.service';
import { BootcampService } from '../../../services/bootcamp.service';
import { StaffService } from '../../../services/staff.service';
import { AuthenticationService } from '../../../services/authentication.service';

import { Bootcamp } from '../../../models/bootcamp';
import { User } from '../../../models/user';

@Component({
  selector: 'app-sub-page-bootcamp',
  templateUrl: './sub-page-bootcamp.component.html',
  styleUrls: ['./sub-page-bootcamp.component.scss']
})
export class SubPageBootcampComponent implements OnInit {
  users: Array<User> = [];
  isEligible: Boolean = false;
  page: Number = 1;
  limit: Number = 500;
  loading: Boolean = true;
  usersLoading: true;
  componentFailure: Boolean = false;
  resultMessage: String = '';
  totalActiveUsers = 0;
  bootcampStatsFilter = 'all';
  currentStep = 0;
  filterString = '';
  statsType = 'number';
  acceptedBootcampers = 0;
  declinedBootcampers = 0;
  retryBootcampers = 0;
  undecidedBootcampers = 0;
  ethnicityWhite = 0;
  ethnicityWhiteMale = 0;
  ethnicityWhiteFemale = 0;
  ethnicityBlack = 0;
  ethnicityBlackMale = 0;
  ethnicityBlackFemale = 0;
  ethnicityColoured = 0;
  ethnicityColouredMale = 0;
  ethnicityColouredFemale = 0;
  ethnicityIndian = 0;
  ethnicityIndianMale = 0;
  ethnicityIndianFemale = 0;
  ethnicityChinese = 0;
  ethnicityChineseMale = 0;
  ethnicityChineseFemale = 0;
  ethnicityOther = 0;
  ethnicityOtherMale = 0;
  ethnicityOtherFemale = 0;
  maleBootcampers = 0;
  femaleBootcampers = 0;
  citizenBootcampers = 0;
  nonCitizenBootcampers = 0;
  experienceCoding = 0;
  noExperienceCoding = 0;
  nonEducated = 0;
  educationMatric = 0;
  educationDiploma = 0;
  educationDegree = 0;
  educationHonors = 0;
  educationMasters = 0;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _titleService: Title,
    public authenticationService: AuthenticationService,
    public bootcampService: BootcampService,
    public navService: NavService,
    public staffService: StaffService
  ) {}

  changeStatsType(value) {
    switch (this.bootcampStatsFilter) {
      case 'all': {
        switch (this.statsType) {
          case 'percentile': {
            return ((value / this.users.length) * 100) + '%';
          }
          case 'number': {
            return value;
          }
        }
        break;
      }
      case 'accepted': {
        switch (this.statsType) {
          case 'percentile': {
            return ((value / this.acceptedBootcampers) * 100) + '%';
          }
          case 'number': {
            return value;
          }
        }
        break;
      }
    }
  }

  filterUsers() {
    for (const user of this.users) {
      if (!user.firstName.toLowerCase().includes(this.filterString.toLowerCase()) &&
        !user.lastName.toLowerCase().includes(this.filterString.toLowerCase()) &&
        !user.username.toLowerCase().includes((this.filterString.toLowerCase())) &&
        !user.email.toLowerCase().includes(this.filterString.toLowerCase())) {
        user.visible$.next(false);
      } else {
        user.visible$.next(true);
      }
    }
  }

  checkIsEligible() {
    this.loading = true;
    this.authenticationService.getUserId().then( userId => {
      this.loading = false;
      if (this.staffService.staff) {
        for (const user of this.staffService.staff) {
          if (user.id = userId && user.selected === true) {
            this.isEligible = true;
          }
        }
      }
    });
  }

  updateStats() {
    this.acceptedBootcampers = 0;
    this.declinedBootcampers = 0;
    this.retryBootcampers = 0;
    this.undecidedBootcampers = 0;
    this.citizenBootcampers = 0;
    this.nonCitizenBootcampers = 0;
    this.maleBootcampers = 0;
    this.femaleBootcampers = 0;
    this.educationMatric = 0;
    this.nonEducated = 0;
    this.educationDiploma = 0;
    this.educationDegree = 0;
    this.educationHonors = 0;
    this.educationMasters = 0;
    this.experienceCoding = 0;
    this.noExperienceCoding = 0;
    this.ethnicityBlackMale = 0;
    this.ethnicityBlackFemale = 0;
    this.ethnicityBlack = 0;
    this.ethnicityWhiteMale = 0;
    this.ethnicityWhiteFemale = 0;
    this.ethnicityWhite = 0;
    this.ethnicityColouredMale = 0;
    this.ethnicityColouredFemale = 0;
    this.ethnicityColoured = 0;
    this.ethnicityIndianMale = 0;
    this.ethnicityIndianFemale = 0;
    this.ethnicityIndian = 0;
    this.ethnicityChineseMale = 0;
    this.ethnicityChineseFemale = 0;
    this.ethnicityChinese = 0;
    this.ethnicityOtherMale = 0;
    this.ethnicityOtherFemale = 0;
    this.ethnicityOther = 0;
    this.totalActiveUsers = 0;
    if (this.bootcampStatsFilter !== 'all') {
      for (const user of this.users) {
        if (user.finalDecision === this.bootcampStatsFilter) {
          if (user.birthCountry === 'ZA') {
            this.citizenBootcampers++;
          } else {
            this.nonCitizenBootcampers++;
          }
          if (user.gender === 'male') {
            this.maleBootcampers++;
          } else {
            this.femaleBootcampers++;
          }
          switch (user.ethnicity) {
            case 'white': {
              if (user.gender === 'male') {
                this.ethnicityWhiteMale++;
              } else if (user.gender === 'female') {
                this.ethnicityWhiteFemale++;
              }
              this.ethnicityWhite++;
              break;
            }
            case 'black': {
              if (user.gender === 'male') {
                this.ethnicityBlackMale++;
              } else if (user.gender === 'female') {
                this.ethnicityBlackFemale++;
              }
              this.ethnicityBlack++;
              break;
            }
            case 'indian':
              if (user.gender === 'male') {
                this.ethnicityIndianMale++;
              } else if (user.gender === 'female') {
                this.ethnicityIndianFemale++;
              }
              this.ethnicityIndian++;
              break;
            case 'chinese':
              switch (user.gender) {
                case 'male': {
                  this.ethnicityChineseMale++;
                  break;
                }
                case 'female': {
                  this.ethnicityChineseFemale++;
                  break;
                }
              }
              this.ethnicityChinese++;
              break;
            case 'coloured':
              switch (user.gender) {
                case 'male': {
                  this.ethnicityColouredMale++;
                  break;
                }
                case 'female': {
                  this.ethnicityColouredFemale++;
                  break;
                }
              }
              this.ethnicityColoured++;
              break;
            default:
              switch (user.gender) {
                case 'male': {
                  this.ethnicityOtherMale++;
                  break;
                }
                case 'female': {
                  this.ethnicityOtherFemale++;
                  break;
                }
              }
              this.ethnicityOther++;
              break;
          }
          switch (user.education) {
            case 'matric':
              this.educationMatric++;
              break;
            case 'diploma':
              this.educationDiploma++;
              break;
            case 'bachelors_degree':
              this.educationDegree++;
              break;
            case 'honors_degree':
              this.educationHonors++;
              break;
            case 'masters_degree':
              this.educationMasters++;
              break;
            case 'none':
              this.nonEducated++;
              break;
            default:
              break;
          }
          if (!user.codeExperience) {
            this.experienceCoding++;
          } else {
            this.noExperienceCoding++;
          }
        }
        if (user.finalDecision === 'accepted') {
          this.acceptedBootcampers++;
        }
        if (user.finalDecision === 'declined') {
          this.declinedBootcampers++;
        }
        if (user.finalDecision === 'retry') {
          this.retryBootcampers++;
        }
        if (!user.finalDecision) {
          this.undecidedBootcampers++;
        }
        if (!user.isAwol) {
          this.totalActiveUsers++;
        }
      }
    } else {
      for (const user of this.users) {
        if (user.birthCountry === 'ZA') {
          this.citizenBootcampers++;
        } else {
          this.nonCitizenBootcampers++;
        }
        if (user.gender === 'male') {
          this.maleBootcampers++;
        } else {
          this.femaleBootcampers++;
        }
        if (user.finalDecision === 'accepted') {
          this.acceptedBootcampers++;
        }
        if (user.finalDecision === 'declined') {
          this.declinedBootcampers++;
        }
        if (user.finalDecision === 'retry') {
          this.retryBootcampers++;
        }
        switch (user.ethnicity) {
          case 'white': {
            if (user.gender === 'male') {
              this.ethnicityWhiteMale++;
            } else if (user.gender === 'female') {
              this.ethnicityWhiteFemale++;
            }
            this.ethnicityWhite++;
            break;
          }
          case 'black': {
            if (user.gender === 'male') {
              this.ethnicityBlackMale++;
            } else if (user.gender === 'female') {
              this.ethnicityBlackFemale++;
            }
            this.ethnicityBlack++;
            break;
          }
          case 'indian':
            if (user.gender === 'male') {
              this.ethnicityIndianMale++;
            } else if (user.gender === 'female') {
              this.ethnicityIndianFemale++;
            }
            this.ethnicityIndian++;
            break;
          case 'chinese':
            switch (user.gender) {
              case 'male': {
                this.ethnicityChineseMale++;
                break;
              }
              case 'female': {
                this.ethnicityChineseFemale++;
                break;
              }
            }
            this.ethnicityChinese++;
            break;
          case 'coloured':
            switch (user.gender) {
              case 'male': {
                this.ethnicityColouredMale++;
                break;
              }
              case 'female': {
                this.ethnicityColouredFemale++;
                break;
              }
            }
            this.ethnicityColoured++;
            break;
          default:
            switch (user.gender) {
              case 'male': {
                this.ethnicityOtherMale++;
                break;
              }
              case 'female': {
                this.ethnicityOtherFemale++;
                break;
              }
            }
            this.ethnicityOther++;
            break;
        }
        switch (user.education) {
          case 'matric':
            this.educationMatric++;
            break;
          case 'diploma':
            this.educationDiploma++;
            break;
          case 'bachelors_degree':
            this.educationDegree++;
            break;
          case 'honors_degree':
            this.educationHonors++;
            break;
          case 'masters_degree':
            this.educationMasters++;
            break;
          case 'none':
            this.nonEducated++;
            break;
          default:
            break;
        }
        if (!user.codeExperience) {
          this.experienceCoding++;
        } else {
          this.noExperienceCoding++;
        }
        if (!user.finalDecision) {
          this.undecidedBootcampers++;
        }
        if (!user.isAwol) {
          this.totalActiveUsers++;
        }
      }
    }
  }

  decideUser(bootcamper, type) {
    bootcamper.loading = true;
    this.bootcampService.decideBootcamper(bootcamper.id, type).subscribe(res => {
      bootcamper.loading = false;
      if (res.status !== 'success') {
        this.componentFailure = true;
        this.resultMessage = res.message;
      } else {
        if (type !== 'retract') {
          bootcamper.finalDecision = type;
        } else {
          bootcamper.finalDecision = '';
        }
      }
      this.updateStats();
    });
  }

  updateAwol(bootcamper, value) {
    bootcamper.loading = true;
    this.bootcampService.updateUserBootcamp(bootcamper.id, value).subscribe(res => {
      bootcamper.loading = false;
      if (res.status !== 'success') {
        this.componentFailure = true;
        this.resultMessage = res.message;
      } else {
        bootcamper.isAwol = value.isAwol;
      }
      this.updateStats();
    });
  }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Loading bootcamp');
    if (this._activatedRoute.snapshot.params.id) {
      this.navService.dashboardStatus = 'Loading bootcamp: ' + this._activatedRoute.snapshot.params.id;
      this.bootcampService.getBootcamp(this._activatedRoute.snapshot.params.id).subscribe(res => {
        this.navService.dashboardLoading.next(false);
        if (res.code !== 'S0003') {
          this.componentFailure = true;
          this.resultMessage = res.message;
          this.resultMessage = res.message;
        } else {
          if (res.data) {
            const newBootcamp = {
              id: res.data.id,
              title: res.data.title,
              slug: res.data.slug,
              startDate: res.data.start_date,
              endDate: res.data.end_date,
              selectionDate: res.data.selection_date,
              steps: [
                { order: 0, title: 'overview', is_current: false },
                { order: 1, title: 'registration', is_current: false },
                { order: 2, title: 'in progress', is_current: false },
                { order: 3, title: 'student voting', is_current: false },
                { order: 4, title: 'staff voting', is_current: false },
                { order: 5, title: 'final selection', is_current: false }
              ]
            };
            for (const step of newBootcamp.steps) {
              if (res.data.current_step === step.title) {
                if (step.title === 'staff voting' || step.title === 'final selection') {
                  this.checkIsEligible();
                }
                step.is_current = true;
                this.currentStep = step.order;
              }
            }
            this.bootcampService.selectedBootcamp = new Bootcamp(newBootcamp);
            this.staffService.getBootcampStaff(this.bootcampService.selectedBootcamp.id).subscribe(bootcampStaff => {
              this._titleService.setTitle('WeThinkCode_ | ' + this.bootcampService.selectedBootcamp.title);
              this.loading = false;
              this.staffService.bootcampStaffPage++;
              if (bootcampStaff.data) {
                for (const staff of bootcampStaff.data) {
                  for (const user of this.staffService.staff) {
                    if (user.id === staff.id) {
                      user.selected = true;
                    }
                  }
                }
              }
            });
          }
        }
        this.getMoreUsers();
      });
    } else {
      this.navService.dashboardLoading.next(false);
      this.componentFailure = true;
      this.resultMessage = 'A project identifier is required.';
    }
  }

  sortByRating(array) {
    array.sort((a, b) => b.rating - a.rating);
  }

  getAge(user) {
    if (user.birthDate) {
      return new Date().getFullYear() - user.birthDate.getFullYear();
    }
    return 0;
  }

  projectsValidated(projects) {
    let count =  0;
    for (const project of projects) {
      if (project.pass_percentage < project.final_mark) {
        count++;
      }
    }
    return count;
  }

  getMoreUsers() {
    this.usersLoading = true;
    this.bootcampService.getMoreUsers(this.page, this.limit).subscribe(usersResult => {
      if (usersResult.status !== 'success') {
        this.componentFailure = true;
        this.resultMessage = usersResult.message;
      } else {
        if (usersResult.data) {
          for (const user of usersResult.data) {
            const newUser = new User(user);
            this.calculateRating(newUser);
            this.users.push(newUser);
          }
          this.sortByRating(this.users);
          this.updateStats();
        }
      }
    });
  }

  calculateRating(user) {
    let userAverage = 0;
    let examsValidated = 0;
    let cheated: Boolean = false;
    let daysValidated = 0;
    for (const project of user.projects) {
      if ((project.slug === 'bootcamp-joburg-exam-00' ||
          project.slug === 'bootcamp-joburg-exam-01' ||
          project.slug === 'bootcamp-joburg-exam-02') && (project.final_mark >= project.pass_percentage)) {
      examsValidated++;
      userAverage += 3.0;
      user.validatedBootcampExams++;
      }
      if (project.final_mark === -42) {
        user.validatedBootcampCheating = false;
        cheated = true;
      }
      if (project.pass_percentage <= project.final_mark && project.slug === 'bootcamp-joburg-final-exam') {
        user.rating += 1.0;
          userAverage += 3.0;
          user.validatedBootcampFinalExam = true;
      }
      if ((project.slug === 'bootcamp-day-01' || project.slug === 'bootcamp-day-01' ||
          project.slug === 'bootcamp-day-02' || project.slug === 'bootcamp-day-03' ||
          project.slug === 'bootcamp-day-04' || project.slug === 'bootcamp-day-05' ||
          project.slug === 'bootcamp-day-06' || project.slug === 'bootcamp-day-07' ||
          project.slug === 'bootcamp-day-08' || project.slug === 'bootcamp-day-09' ||
          project.slug === 'bootcamp-day-10' || project.slug === 'bootcamp-day-11' ||
          project.slug === 'bootcamp-day-12' || project.slug === 'bootcamp-day-13') &&
        (project.final_mark >= project.pass_percentage)) {
        daysValidated++;
        user.validatedBootcampDays++;
        userAverage++;
      }
      if (project.slug === 'bootcamp-sastantua' || project.slug === 'bootcamp-match-n-match' ||
        project.slug === 'bootcamp-evalexpr' || project.slug === 'bootcamp-colle-00' ||
        project.slug === 'bootcamp-colle-01' || project.slug === 'bootcamp-colle-02') {
        userAverage += 2;
      }
    }
    user.bootcampAverage = userAverage;
    if (user.bootcampAverage >= 10) {
      user.rating++;
      user.validatedBootcampAverage = true;
    }
    if (user.correctionsCount >= 44) {
      user.rating++;
    }
    if (user.level && user.level > 1.0) {
      user.rating++;
      user.validatedBootamplevel = true;
    }
    if (daysValidated >= 5) {
      user.rating++;
    }
    if (user.studentVotes >= 5) {
      user.rating++;
      user.validatedBootcampStudentVotes = true;
    }
    if (!cheated) {
      user.rating += 1.0;
    }
    if (user.attendance >= 21) {
      user.rating += 1.0;
    }
    if (examsValidated > 0) {
      user.rating += 1.0;
    }
    if (user.attendance === 0) {
      user.rating = 0;
    }
  }

  updateStep(event) {
    if (event.title === 'staff voting' || event.title === 'final selection') {
      this.checkIsEligible();
    }
    this.currentStep = event.order;
  }
}
