import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { CurriculumService } from '../../../services/curriculum.service';
import { NavService } from '../../../services/nav.service';
import { Curriculum } from '../../../models/curriculum';
import {CampusService} from '../../../services/campus.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-sub-page-curriculum',
  templateUrl: './sub-page-curriculum.component.html',
  styleUrls: ['./sub-page-curriculum.component.scss']
})
export class SubPageCurriculumComponent implements OnInit {
  loading: Boolean = true;
  componentFailure: Boolean = false;
  componentFailureMessage: String = '';

  constructor(
    private _titleService: Title,
    public curriculumService: CurriculumService,
    public campusService: CampusService,
    public navService: NavService
  ) {
  }

  ngOnInit() {
    this.curriculumService.page = 1;
    this._titleService.setTitle('WeThinkCode_ | Curriculum');
    this.curriculumService.getMoreCurriculum().subscribe(res => {
      this.loading = false;
      if (res.code !== 'S0002') {
        this.componentFailure = true;
        this.componentFailureMessage = 'We could not retrieve requested curriculum';
      } else {
        for (const curriculum of res.data) {
          this.curriculumService.curriculum.push(new Curriculum(curriculum));
        }
      }
    });
  }

  campusesString(curriculum) {
    const returnValue = new BehaviorSubject('None');
    let newString = '';
    if (curriculum.campuses) {
      let counter = 1;
      for (const campus of curriculum.campuses) {
        newString += campus.title;
        if (counter !== curriculum.campuses.length ) {
          newString += ', ';
        }
        counter++;
      }
      if (newString) {
        returnValue.next(newString);
      }
    }
    return returnValue;
  }

}
