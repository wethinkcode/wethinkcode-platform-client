import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageCurriculumComponent } from './sub-page-curriculum.component';

describe('SubPageCurriculumComponent', () => {
  let component: SubPageCurriculumComponent;
  let fixture: ComponentFixture<SubPageCurriculumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageCurriculumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageCurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
