import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sub-page-analytics',
  templateUrl: './sub-page-analytics.component.html',
  styleUrls: ['./sub-page-analytics.component.scss']
})
export class SubPageAnalyticsComponent implements OnInit {

  constructor(
    private _titleService: Title
  ) { }

  ngOnInit() {
    this._titleService.setTitle('WeThinkCode_ | Analytics');
  }

}
