import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubPageAnalyticsComponent } from './sub-page-analytics.component';

describe('SubPageAnalyticsComponent', () => {
  let component: SubPageAnalyticsComponent;
  let fixture: ComponentFixture<SubPageAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubPageAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubPageAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
