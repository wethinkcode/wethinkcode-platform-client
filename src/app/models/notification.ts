import { Observable } from 'rxjs/Observable';

export class Notification {
  content: String = '';
  position;
  date: Date = new Date();
  $tsl: Observable<number>;
  tslDifference;
  tslSubscription;
  tsl;
  seen: Boolean = false;
  type: String;
  image: String;
  NotifyingUser: String;
  isVisible: Boolean = false;
  timeout;

  getTSL(elapsed) {
    const currentMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if (elapsed < 30) {
      return 'Now';
    }
    return currentMonth[this.date.getMonth()] + ' ' + this.date.getDate() + ' at ' + this.date.getHours() + ':' + this.date.getMinutes();
  }

  constructor(content) {
    this.tslDifference = Math.floor((this.date.getTime() - new Date().getTime()) / -1000);
    this.tsl = this.getTSL(this.tslDifference);
    this.content = content;
    this.$tsl = Observable.interval(1000).map((x) => {
      this.tslDifference = Math.floor((this.date.getTime() - new Date().getTime()) / -1000);
      return x;
    });
    this.tslSubscription = this.$tsl.subscribe((x) => this.tsl = this.getTSL(this.tslDifference));
    this.isVisible = true;
    this.timeout = setTimeout(function(){
      this.isVisible = false;
    }.bind(this), 4500);
  }

  closeNotification() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.isVisible = false;
  }
}
