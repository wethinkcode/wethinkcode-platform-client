export class Exam {
  id: Number = 0;
  userId: Number = 0;
  grade: Number = 0;
  createdAt: Date;
  closedAt: Date;

  constructor(exam) {
    if (exam.id) { this.id = exam.id;  }
    if (exam.user_id) {this.userId = exam.user_id; }
    if (exam.exam_id ) { this.id = exam.exam_id; }
    if (exam.created_at ) { this.createdAt = exam.created_at; }
    if (exam.closed_at ) { this.closedAt = exam.closed_at; }
  }
}
