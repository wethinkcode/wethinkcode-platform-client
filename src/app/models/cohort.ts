import { Campus } from './campus';
import {Student} from './student';

export class Cohort {
  id: Number;
  title: String;
  slug: String;
  campus: Campus;
  startDate: Date;
  studentCount: Number = 0;
  students: Array<Student> = [];
  processSteps: Array<any> = [
    { order: 0, title: 'Overview', is_current: true },
    { order: 1, title: 'Students', is_current: false },
    { order: 2, title: 'Under Review', is_current: false },
    { order: 3, title: 'Warnings', is_current: false },
    { order: 4, title: 'Dismissal', is_current: false }
  ];

  constructor(cohort) {
    if (cohort.id) { this.id = cohort.id; }
    if (cohort.title) { this.title = cohort.title; }
    if (cohort.slug) { this.slug = cohort.slug; }
    if (cohort.campus) { this.campus = cohort.campus; }
    if (cohort.start_date) { this.startDate = new Date(cohort.start_date); }
    if (cohort.student_count) {this.studentCount = cohort.student_count; }
    if (cohort.users && cohort.users.length) {
      for (const user of cohort.users) {
        user.start_date = this.startDate;
        const newUser = new Student(user);
        this.students.push(newUser);
      }
    }
  }
}
