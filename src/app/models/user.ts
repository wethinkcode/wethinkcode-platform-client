import {BehaviorSubject} from 'rxjs/BehaviorSubject';

export class User {
  visible$: BehaviorSubject<Boolean> = new BehaviorSubject(true);
  year: any;
  id;
  username: String = '';
  tools: Array<any> = [];
  firstName: String = '';
  identifier: String = '';
  lastName: String = '';
  email: String = '';
  password: String = '';
  passwordRepeat: String = '';
  role: String = '';
  state: String = '';
  coverImage: String = '';
  displayName: String = '';
  profileImage: String = '';
  accommodation: String = '';
  birthCity: String = '';
  birthCountry: String = '';
  birthDate: Date = null;
  campus: String = '';
  rating: any = 0.0;
  citizenshipCountry: String = '';
  codeExperience: String = '';
  computerAccess: String = '';
  country: String = '';
  education: String = '';
  ethnicity: String = '';
  gender: String = '';
  isAwol: Boolean = false;
  highSchool: String = '';
  homeLanguage: String = '';
  homeProvince: String = '';
  homeTown: String = '';
  idNumber: String = '';
  internetAccess: String = '';
  maritalStatus: String = '';
  medicalAidName: String = '';
  medicalAidNumber: String = '';
  phone: String = '';
  postalStreet: String = '';
  preferredName: String = '';
  referral: String = '';
  zipCode: String = '';
  projects: Array<any> = [];
  attendance: Number = 0;
  filters: any = {};
  exams: Array<any> = [];
  selected: Boolean = false;
  validatedBootcampExams: any = 0;
  validatedBootcampFinalExam: Boolean = false;
  validatedBootamplevel: any = 0;
  validatedBootcampDays: any = 0;
  level: any = 0;
  studentVotes = 0;
  correctionsCount = 0;
  cheated: Boolean = false;
  validatedBootcampCheating: Boolean = true;
  validatedBootcampStudentVotes: Boolean = false;
  bootcampAverage: any = 0;
  finalDecision: String = '';
  bootcampRatingSelected: Boolean = false;
  examsValidated: any = 0;
  validatedCExam: Boolean = false;
  validatedUnix1: Boolean = false;
  validatedAlgorithms1: Boolean = false;
  validatedAlgorithms2: Boolean = false;
  validatedInternship1: Boolean = false;
  validatedGraphics1: Boolean = false;
  validatedGraphics2: Boolean = false;
  validatedWeb: Boolean = false;
  validatedUnix2: Boolean = false;
  validatedCPP: Boolean = false;
  validatedKernel: Boolean = false;
  validatedrushes: Boolean = false;
  validatedInternship2: Boolean = false;
  validatedPHPBootcamp: Boolean = false;
  validatedCPPBootcamp: Boolean = false;
  validatedCBootcamp: Boolean = false;
  validatedSocialTechLab: Boolean = false;
  validatedSecurity: Boolean = false;
  validatedOcaml: Boolean = false;
  validatedUnity: Boolean = false;
  validatedSwiftIos: Boolean = false;
  validatedJavaAndroid: Boolean = false;
  validatedDevops: Boolean = false;
  validatedMath: Boolean = false;
  validatedNetwork: Boolean = false;
  validatedVirus: Boolean = false;
  validatedModulesY1: any = 0;
  validatedModulesY2: any = 0;

  constructor(user) {
    this.updateUser(user);
  }

  updateUser(user) {
    if (user.id) { this.id = user.id; }
    if (user.username) { this.username = user.username; }
    if (user.tools) { this.tools = user.tools; }
    if (user.first_name) { this.firstName = user.first_name; }
    if (user.last_name) { this.lastName = user.last_name; }
    if (user.identifier) { this.identifier = user.identifier; }
    if (user.email) { this.email = user.email; }
    if (user.password) { this.password = user.password; }
    if (user.password_repeat) { this.passwordRepeat = user.password_repeat; }
    if (user.role) { this.role = user.role; }
    if (user.cover_image) { this.coverImage = user.cover_image; }
    if (user.display_name) { this.displayName = user.display_name; }
    if (user.profile_image) { this.profileImage = user.profile_image; }
    if (user.birth_city) { this.birthCity = user.birth_city; }
    if (user.birth_country) { this.birthCountry = user.birth_country; }
    if (user.birth_date) { this.birthDate = new Date(user.birth_date); }
    if (user.campus) { this.campus = user.campus; }
    if (user.citizenship_country) { this.citizenshipCountry = user.citizenship_country; }
    if (user.code_experience) { this.codeExperience = user.code_experience; }
    if (user.computer_access) { this.computerAccess = user.computer_access; }
    if (user.country) { this.country = user.country; }
    if (user.education) { this.education = user.education; }
    if (user.ethnicity) { this.ethnicity = user.ethnicity; }
    if (user.gender) { this.gender = user.gender; }
    if (user.high_school) { this.highSchool = user.highSchool; }
    if (user.home_language) { this.homeLanguage = user.home_language; }
    if (user.home_province) { this.homeProvince = user.home_province; }
    if (user.home_town) { this.homeTown = user.home_town; }
    if (user.id_number) { this.idNumber = user.id_number; }
    if (user.internet_access) { this.internetAccess = user.internet_access; }
    if (user.marital_status) { this.maritalStatus = user.marital_status; }
    if (user.medical_aid_name) { this.medicalAidName = user.medical_aid_name; }
    if (user.medical_aid_number) { this.medicalAidNumber = user.medical_aid_number; }
    if (user.phone) { this.phone = user.phone; }
    if (user.postal_street) { this.postalStreet = user.postal_street; }
    if (user.preferred_name) { this.preferredName = user.preferred_name; }
    if (user.referral) { this.referral = user.referral; }
    if (user.zip_code) { this.zipCode = user.zip_code; }
    if (user.projects) { this.projects = user.projects; }
    if (user.attendance) { this.attendance = user.attendance; }
    if (user.exams) { this.exams = user.exams; }
    if (user.state) { this.state = user.state; }
    if (user.year) {this.year = user.year; }
    if (user.level) {this.level = user.level; }
    if (user.student_votes) {this.studentVotes = user.student_votes; }
    if (user.final_decision) {this.finalDecision = user.final_decision; }
    if (user.is_awol) {this.isAwol = user.is_awol; }
    if (user.corrections_count) {this.correctionsCount = user.corrections_count; }
  }
}
