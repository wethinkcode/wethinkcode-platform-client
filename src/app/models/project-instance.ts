import { Campus } from './campus';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

export class ProjectInstance {
  id: Number;
  projectId: Number;
  title: String = '';
  startDate: Date = new Date();
  startDateTSL: String;
  $startDateTSL: Observable<any>;
  startDateTSLSubscription: Subscription;
  endDate: Date = new Date();
  endDateTSL: String;
  $endDateTSL: Observable<any>;
  endDateTSLSubscription: Subscription;
  campuses: Campus;
  validationGrade: number;
  createdDate: Date;
  updatedDate: Date;

  constructor(instance) {
    if (instance.id) {
      this.id = instance.id;
    }
    if (instance.title) {
      this.title = instance.title;
    }
    if (instance.projectId) {
      this.projectId = instance.projectId;
    }
    if (instance.startDate) {
      this.startDate = new Date(instance.startDate);
      let startDateTSLDifference = Math.floor((this.startDate.getTime() - new Date().getTime()) / -1000);
      this.startDateTSL = this.getTSL(startDateTSLDifference, this.startDate);
      this.$startDateTSL = Observable.interval(1000).map((x) => {
        startDateTSLDifference = Math.floor((this.startDate.getTime() - new Date().getTime()) / -1000);
        return x;
      });
      this.startDateTSLSubscription = this.$startDateTSL.subscribe((x) => this.startDateTSL = this.getTSL(startDateTSLDifference, this.startDate));
    }
    if (instance.endDate) {
      this.endDate = new Date(instance.endDate);
      let endDateTSLDifference =  Math.floor((this.endDate.getTime() - new Date().getTime()) / -1000);
      this.endDateTSL = this.getTSL(endDateTSLDifference, this.endDate);
      this.$endDateTSL = Observable.interval(1000).map((x) => {
        endDateTSLDifference = Math.floor((this.endDate.getTime() - new Date().getTime()) / -1000);
        return x;
      });
      this.endDateTSLSubscription = this.$endDateTSL.subscribe((x) => this.endDateTSL = this.getTSL(endDateTSLDifference, this.endDate));
    }
    if (instance.validationGrade) {
      this.validationGrade = instance.validationGrade;
    }
    if (instance.campuses) {
      this.campuses = instance.campuses;
    }
    if (instance.createdDate) {
      this.createdDate = instance.createdDate;
    }
    if (instance.updatedDate) {
      this.updatedDate = instance.updatedDate;
    }
  }

  getTSL(elapsed, date) {
    const currentMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if (elapsed < 30) {
      return 'Now';
    } else if (elapsed < 60) {
      return 'Less than a minute ago';
    } else if (elapsed < 300) {
      return 'Less than 5 minutes ago';
    } else if (elapsed < 3600) {
      return 'Less than 1 hour ago';
    }
    return currentMonth[date.getMonth()] + ' ' + date.getDate() + ' at ' + date.getHours() + ':' + date.getMinutes();
  }
}
