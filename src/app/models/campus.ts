export class Campus {
  id: Number;
  slug: String;
  profileImage: String;
  coverImage: String;
  title: String;
  description: String;
  physicalFloor: Number;
  physicalUnit: Number;
  physicalBuilding: String;
  physicalStreet: String;
  physicalCity: String;
  physicalProvince: String;
  physicalCountry: String = '';
  physicalPostalCode: String;
  postalFloor: Number;
  postalUnit: Number;
  postalBuilding: String;
  postalStreet: String;
  postalCity: String;
  postalProvince: String;
  postalCountry: String = '';
  postalPostalCode: String;

  constructor(campus) {
    if (campus.id) {this.id = campus.id; }
    if (campus.slug) {this.slug = campus.slug; }
    if (campus.profile_image) {this.profileImage = campus.profile_image; }
    if (campus.cover_image) {this.coverImage = campus.cover_image; }
    if (campus.title) {this.title = campus.title; }
    if (campus.description) {this.description = campus.description; }
    if (campus.physical_floor) {this.physicalFloor = campus.physical_floor; }
    if (campus.physical_building) {this.physicalBuilding = campus.physical_building; }
    if (campus.physical_street) {this.physicalStreet = campus.physical_street; }
    if (campus.physicalC_city) {this.physicalCity = campus.physical_city; }
    if (campus.physical_province) {this.physicalProvince = campus.physical_province; }
    if (campus.physical_country) {this.physicalCountry = campus.physical_country; }
    if (campus.physical_postal_code) {this.postalPostalCode = campus.physical_postal_code; }
    if (campus.postal_floor) {this.postalFloor = campus.postal_floor; }
    if (campus.postal_unit) {this.postalUnit = campus.postal_unit; }
    if (campus.postal_building) {this.postalBuilding = campus.poatal_building; }
    if (campus.postal_street) {this.postalStreet = campus.postal_street; }
    if (campus.postal_city) {this.postalCity = campus.postal_city; }
    if (campus.postal_province) {this.postalProvince = campus.postal_province; }
    if (campus.postal_country) {this.postalCountry = campus.postal_country; }
    if (campus.postal_postal_code) {this.postalPostalCode = campus.postal_postal_code; }
  }
}
