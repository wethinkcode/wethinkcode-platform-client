import {Curriculum} from './curriculum';
import { Milestone } from './milestone';
import {Project} from "./project";

export class Module {
  id: Number;
  title: String;
  slug: String;
  milestoneId: Number;
  curriculum: Array<Curriculum> = [];
  milestones: Array<Milestone> = [];
  projects: Array<Project> = [];
  isValidated: Boolean = false;

  constructor(module) {
    if (module.id) {
      this.id = module.id;
    }
    if (module.projects) {
      for (const project of module.projects) {
        this.projects.push(new Project(project));
      }
    }
    if (module.title) { this.title = module.title; }
    if (module.slug) { this.slug = module.slug; }
    if (module.curriculum) {
      for (const singleCurriculum of module.curriculum) {
        this.curriculum.push(new Curriculum(singleCurriculum));
      }
    }
  }
}
