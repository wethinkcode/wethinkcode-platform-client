import { Module } from './module';

export class Project {
  id: Number;
  slug: String;
  moduleId: Number;
  title: String;
  description: String;
  size: String;
  module: Module;
  validationGrade: Number;
  finalGrade: Number = 0;

  constructor(project: any) {
    if (project.description) {
      this.description = project.description;
    }
    if (project.title) {
      this.title = project.title;
    }
    if (project.slug) {
      this.slug = project.slug;
    }
    if (project.size) {
      this.size = project.size;
    }
    if (project.module_id) {
      this.moduleId = project.module_id;
    }
    if (project.id) {
      this.id = project.id;
    }
    if (project.final_mark) {
      this.finalGrade = project.final_mark;
    }
    if (project.pass_percentage) {
      this.validationGrade = project.pass_percentage;
    }
  }
}
