export class Milestone {
  id: Number;
  title: String;
  slug: String;

  constructor(milestone) {
    if (milestone.id) { this.id = milestone.id;  }
    if (milestone.title) { this.title = milestone.title;  }
    if (milestone.slug) { this.slug = milestone.slug;  }
  }
}
