import { Campus } from './campus';

export class Curriculum {
  id: Number = -1;
  title: String = '';
  slug: String = '';
  campuses: Array<Campus> = [];

  constructor(curriculum) {
    if (curriculum.id) { this.id = curriculum.id; }
    if (curriculum.title) { this.title = curriculum.title; }
    if (curriculum.slug) { this.slug = curriculum.slug; }
    if (curriculum.campuses) {
      for (const campus of curriculum.campuses) {
        this.campuses.push(new Campus(campus));
      }
    }
  }
}
