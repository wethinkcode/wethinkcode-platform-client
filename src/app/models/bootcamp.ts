import { Campus } from './campus';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

export class Bootcamp {
  id: number;
  slug: String;
  title: String;
  steps: any;
  startDate: Date;
  startDateTSL: String;
  $startDateTSL: Observable<any>;
  startDateTSLSubscription: Subscription;
  endDate: Date;
  endDateTSL: String;
  $endDateTSL: Observable<any>;
  endDateTSLSubscription: Subscription;
  selectionDate: Date;
  selectionDateTSL: String;
  $selectionDateTSL: Observable<any>;
  selectionDateTSLSubscription: Subscription;
  campuses: Array<Campus>;
  countAccepted: any = 0;
  countBootcampers: any = 0;
  countDeclined: any = 0;
  countRetry: any = 0;
  countUndecided: any = 0;
  countMale: any = 0;
  countFemale: any = 0;

  constructor(bootcamp) {
    if (bootcamp.id) {
      this.id = bootcamp.id;
    }
    if (bootcamp.slug) {
      this.slug = bootcamp.slug;
    }
    if (bootcamp.title) {
      this.title = bootcamp.title;
    }
    if (bootcamp.startDate) {
      this.startDate = new Date(bootcamp.startDate);
      let startDateTSLDifference = Math.floor((this.startDate.getTime() - new Date().getTime()) / -1000);
      this.startDateTSL = this.getTSL(startDateTSLDifference, this.startDate);
      this.$startDateTSL = Observable.interval(1000).map((x) => {
        startDateTSLDifference = Math.floor((this.startDate.getTime() - new Date().getTime()) / -1000);
        return x;
      });
      this.startDateTSLSubscription = this.$startDateTSL.subscribe((x) => this.startDateTSL = this.getTSL(startDateTSLDifference, this.startDate));
    }
    if (bootcamp.endDate) {
      this.endDate = new Date(bootcamp.endDate);
      let endDateTSLDifference = Math.floor((this.endDate.getTime() - new Date().getTime()) / -1000);
      this.endDateTSL = this.getTSL(endDateTSLDifference, this.endDate);
      this.$endDateTSL = Observable.interval(1000).map((x) => {
        endDateTSLDifference = Math.floor((this.endDate.getTime() - new Date().getTime()) / -1000);
        return x;
      });
      this.endDateTSLSubscription = this.$endDateTSL.subscribe((x) => this.endDateTSL = this.getTSL(endDateTSLDifference, this.endDate));
    }
    if (bootcamp.selectionDate) {
      this.selectionDate = new Date(bootcamp.selectionDate);
      let selectionDateTSLDifference = Math.floor((this.selectionDate.getTime() - new Date().getTime()) / -1000);
      this.selectionDateTSL = this.getTSL(selectionDateTSLDifference, this.selectionDate);
      this.$selectionDateTSL = Observable.interval(1000).map((x) => {
        selectionDateTSLDifference = Math.floor((this.selectionDate.getTime() - new Date().getTime()) / -1000);
        return x;
      });
      this.selectionDateTSLSubscription = this.$selectionDateTSL.subscribe((x) => this.selectionDateTSL = this.getTSL(selectionDateTSLDifference, this.selectionDate));
    }
    if (bootcamp.campuses) {
      this.campuses = bootcamp.campuses;
    }
    if (bootcamp.steps) {
      this.steps = bootcamp.steps;
    }
    if (bootcamp.count_accepted) {
      this.countAccepted = bootcamp.count_accepted;
    }
    if (bootcamp.count_declined) {
      this.countDeclined = bootcamp.count_declined;
    }
    if (bootcamp.count_retry) {
      this.countRetry = bootcamp.count_retry;
    }
    if (bootcamp.count_undecided) {
      this.countUndecided = bootcamp.count_undecided;
    }
    if (bootcamp.count_bootcampers) {
      this.countBootcampers = bootcamp.count_bootcampers;
    }
    if (bootcamp.count_male) {
      this.countMale = bootcamp.count_male;
    }
    if (bootcamp.count_female) {
      this.countFemale = bootcamp.count_female;
    }
  }

  getTSL(elapsed, date) {
    const currentMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if (elapsed < 30) {
      return 'Now';
    } else if (elapsed < 60) {
      return 'Less than a minute ago';
    } else if (elapsed < 300) {
      return 'Less than 5 minutes ago';
    } else if (elapsed < 3600) {
      return 'Less than 1 hour ago';
    }
    return currentMonth[date.getMonth()] + ' ' + date.getDate() + ' at ' + (date.getHours() < 10 ? '0' : '') +
      date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') +  date.getMinutes();
  }
}
