import { User } from './user';
import { Module } from './module';
import { Project } from './project';

export class Student extends User {
  startDate: Date;
  currentMilestone: String = '';
  modules: Array<Module> = [];
  projects: Array<Project> = [];
  attendance: any = 0;
  exams: Array<any> = [];
  examsValidated: any = 0;
  isUnderReview: Boolean = false;
  isNonPerformer: Boolean = false;
  isLowPerformer: Boolean = false;
  isPerformer: Boolean = false;
  isExempt: Boolean = false;
  isHighPerformer: Boolean = false;
  isInternship1Ready: Boolean = false;
  isYear2Ready: Boolean = false;
  isInternship2Ready: Boolean = false;
  y1ModulesValidated = 0;
  y2ModulesValidated = 0;
  internship1Validated = false;
  internship2Validated = false;
  socialTechValidated = false;
  currentMonth = 0;
  isVisible = true;

  constructor(student) {
    super(student);
    if (student.start_date) {
      this.startDate = student.start_date;
      this.currentMonth = (new Date().getFullYear() - this.startDate.getFullYear()) * 12;
      this.currentMonth -= this.startDate.getMonth() + 1;
      this.currentMonth += new Date().getMonth();
    }
    if (student.modules) {
        this.getModules(student).then(res => {
          if (!this.isHighPerformer && !this.isNonPerformer && !this.isPerformer && !this.isLowPerformer) {
            this.getTags();
          }
        });
    }
    if (student.exams) {
      this.getExams(student);
    }
    if (student.attendance) {
      this.attendance = student.attendance;
    }
  }

  getCurrentMonth() {
    if (this.startDate) {
      this.currentMonth = (new Date().getFullYear() - this.startDate.getFullYear()) * 12;
      this.currentMonth -= this.startDate.getMonth() + 1;
      this.currentMonth += new Date().getMonth();
    }
  }

  getModules(student) {
    return new Promise((resolve, reject) =>  {
      let iterator = 0;
      if (student.modules) {
        for (const module of student.modules) {
          iterator++;
          if (student.projects && student.projects.length) {
            this.getModuleProjects(student, module).then(res => {
              this.getModulesValidated(module);
            });
          }
          const newModule = new Module(module);
          newModule.projects = [];
          this.modules.push(newModule);
          if (iterator === student.modules.length) {
            resolve();
          }
        }
      }
    });
  }

  getModuleProjects(student, module) {
    return new Promise((resolve, reject) => {
      let iterator = 0;
      module.projects = [];
      if (student.projects) {
        for (const project of student.projects) {
          if (project && project !== undefined && project.module_id === module.id) {
            const newProject = new Project(project);
            module.projects.push(newProject);
          }
          if (iterator === student.projects.length - 1) {
            resolve();
          }
          iterator++;
        }
      }
    });
  }

  getModulesValidated(module) {
    if (this.startDate) {
      let smallProjectsValidated = 0;
      let bigProjectsValidated = 0;
      let internship1Validated = 0;
      let internship2Validated = 0;
      let cppBootcampValidated = 0;
      let phpBootcampValidated = 0;
      let socialTechValidated = 0;
      let examValidated = 0;
      let rushesValidated = 0;
      if (module.projects && module.projects.length) {
        for (const project of module.projects) {
          if ((project.slug === 'piscine-php' && module.slug === 'web' && this.startDate.getFullYear() !== 2016 && project.finalGrade >= project.validationGrade) ||
            (project.slug === 'piscine-cpp' && module.slug === 'cpp' && project.finalGrade >= project.validationGrade) ||
            (module.id === project.moduleId && project.finalGrade >= project.validationGrade)) {
            switch (project.size) {
              case ('small'):
                smallProjectsValidated++;
                break;
              case ('big'):
                bigProjectsValidated++;
                break;
              case ('internship'):
                if (module.slug === 'internship1') {
                  internship1Validated++;
                } else {
                  internship2Validated++;
                }
                break;
              case ('bootcamp'):
                if (module.slug === 'piscine-cpp' || (project.slug === 'piscine-cpp' && module.slug === 'cpp')) {
                  cppBootcampValidated++;
                } else if (module.slug === 'piscine-php' || (project.slug === 'piscine-php' && module.slug === 'web' && this.startDate.getFullYear() !== 2016)) {
                  phpBootcampValidated++;
                }
                break;
              case ('exam'):
                examValidated++;
                break;
              case('rushes'):
                rushesValidated++;
                break;
              case('social-tech-lab'):
                socialTechValidated++;
                break;
              default:
                break;
            }
          }
        }
        switch (this.startDate.getFullYear()) {
          case 2016: {
            switch (module.slug) {
              case 'unix1':
              case 'algo1':
                if ((bigProjectsValidated && smallProjectsValidated) || smallProjectsValidated >= 3) {
                  module.isValidated = true;
                }
                break;
              case 'c-exam':
                if (examValidated >= 1) {
                  module.isValidated = true;
                }
                break;
              case 'graphics1':
              case 'algo2':
                if ((bigProjectsValidated && smallProjectsValidated) || smallProjectsValidated >= 2) {
                  module.isValidated = true;
                }
                break;
              case 'devops':
                if (smallProjectsValidated) {
                  module.isValidated = true;
                }
                break;
              case 'graphics2':
              case 'security':
              case 'ocaml':
              case 'network':
              case 'java-android':
              case 'math':
              case 'swift-ios':
              case 'virus':
              case 'unity':
                if (smallProjectsValidated >= 2) {
                  module.isValidated = true;
                }
                break;
              case 'web': {
                if (bigProjectsValidated || smallProjectsValidated >= 2) {
                  module.isValidated = true;
                }
                break;
              }
              case 'internship1':
                if (internship1Validated) {
                  module.isValidated = true;
                  this.y1ModulesValidated++;
                }
                break;
              case 'internship2':
                if (internship2Validated) {
                  module.isValidated = true;
                }
                break;
              case 'social-tech-lab':
                if (socialTechValidated) {
                  module.isValidated = true;
                }
                break;
              case 'rushes':
                if (rushesValidated) {
                  module.isValidated = true;
                }
                break;
              case 'piscine-cpp':
                if (cppBootcampValidated) {
                  module.isValidated = true;
                }
                break;
              case 'piscine-php':
                if (phpBootcampValidated) {
                  module.isValidated = true;
                }
                break;
              case 'cpp':
                if ((bigProjectsValidated && (smallProjectsValidated || cppBootcampValidated)) || (smallProjectsValidated + cppBootcampValidated >= 2)) {
                  module.isValidated = true;
                }
                break;
              case 'unix2':
                if ((bigProjectsValidated && smallProjectsValidated) || smallProjectsValidated >= 2) {
                  module.isValidated = true;
                }
                break;
              case 'kernel':
                if (smallProjectsValidated >= 3) {
                  module.isValidated = true;
                }
                break;
              default:
                break;
            }
            break;
          }
          default: {
            switch (module.slug) {
              case 'devops':
                if (smallProjectsValidated) {
                  module.isValidated = true;
                }
                break;
              case 'unix1':
              case 'algo1':
                if ((bigProjectsValidated && smallProjectsValidated) || (smallProjectsValidated >= 3)) {
                  module.isValidated = true;
                }
                break;
              case 'kernel':
                if (smallProjectsValidated >= 3) {
                  module.isValidated = true;
                }
                break;
              case 'unix2':
              case 'graphics1':
              case 'algo2':
              case 'security':
              case 'ocaml':
              case 'network':
              case 'java-android':
              case 'math':
              case 'virus':
              case 'unity':
              case 'swift-ios':
                if ((bigProjectsValidated && smallProjectsValidated) || (smallProjectsValidated >= 2)) {
                  module.isValidated = true;
                }
                break;
              case 'web':
                if ((bigProjectsValidated && (smallProjectsValidated || phpBootcampValidated)) || (smallProjectsValidated + phpBootcampValidated >= 2)) {
                  module.isValidated = true;
                }
                break;
              case 'cpp':
                if ((bigProjectsValidated && (smallProjectsValidated || cppBootcampValidated)) || (smallProjectsValidated + cppBootcampValidated >= 2)) {
                  module.isValidated = true;
                }
                break;
              case 'c-exam':
                if (examValidated >= 1) {
                  module.isValidated = true;
                }
                break;
              case 'internship1':
                if (internship1Validated) {
                  this.internship1Validated = true;
                  module.isValidated = true;
                }
                break;
              case 'internship2':
                this.internship2Validated = true;
                if (internship2Validated) {
                  module.isValidated = true;
                }
                break;
              case 'social-tech-lab':
                if (socialTechValidated) {
                  module.isValidated = true;
                }
                break;
              case 'rushes':
                if (rushesValidated) {
                  module.isValidated = true;
                }
                break;
              case 'piscine-cpp':
                if (cppBootcampValidated) {
                  module.isValidated = true;
                }
                break;
              case 'piscine-php':
                if (phpBootcampValidated) {
                  module.isValidated = true;
                }
                break;
              case 'graphics2':
                if (smallProjectsValidated >= 2) {
                  module.isValidated = true;
                }
                break;
              default:
                break;
            }
            break;
          }
        }
      }
      if (module.isValidated && (module.slug === 'unix1' || module.slug === 'graphics1' || module.slug === 'web' ||
          module.slug === 'algo1' || module.slug === 'graphics2' || module.slug === 'algo2')) {
        this.y1ModulesValidated++;
        module.validated = true;
      } else if (module.isValidated && (module.slug === 'unix2' || module.slug === 'cpp' || module.slug === 'kernel' || module.slug === 'security'
          || module.slug === 'ocaml' || module.slug === 'unity' || module.slug === 'swift-ios' ||
          module.slug === 'java-android' || module.slug === 'math' || module.slug === 'devops' ||
          module.slug === 'network' || module.slug === 'virus')) {
        this.y2ModulesValidated++;
        module.validated = true;
      }
    }
  }

  getExams(student) {
    if (student.exams && student.exams.length) {
      this.exams = student.exams;
      for (const exam of student.exams) {
        if (exam.grade >= exam.pass_percentage) {
          this.examsValidated++;
        }
      }
    }
  }

  getTags() {
    if (this.currentMonth > 3 && this.currentMonth <= 12) {
      if (this.examsValidated >= 5 && this.y1ModulesValidated >= 2) {
        this.isHighPerformer = true;
      } else if (this.examsValidated >= 5 && this.y1ModulesValidated === 1) {
        this.isPerformer = true;
      } else if (this.examsValidated >= 5 && this.y1ModulesValidated === 0) {
        this.isLowPerformer = true;
      } else if (this.examsValidated < 5 && !this.y1ModulesValidated ) {
        this.isNonPerformer = true;
      } else if (this.y1ModulesValidated >= 2 && this.examsValidated >= 5 && this.currentMonth > 8 && this.currentMonth <= 1 && this.internship1Validated) {
        this.isYear2Ready = true;
      } else {
        this.isExempt = true;
      }
    } else if (this.currentMonth > 12 && this.currentMonth <= 24) {
      if (this.examsValidated >= 5 && this.y1ModulesValidated >= 2 && this.y2ModulesValidated >= 2) {
        this.isHighPerformer = true;
      } else if (this.examsValidated >= 5 && this.y1ModulesValidated >= 2 && this.y2ModulesValidated === 1) {
        this.isPerformer = true;
      } else if (this.y1ModulesValidated === 1 && this.y2ModulesValidated < 2 ) {
        this.isLowPerformer = true;
      } else if (((this.examsValidated < 5 && !this.y1ModulesValidated) || (!this.y2ModulesValidated)) && this.currentMonth > 12 && this.currentMonth <= 20) {
        this.isNonPerformer = true;
      } else if (this.y2ModulesValidated >= 2 && this.validatedSocialTechLab && this.currentMonth <= 16) {
        this.isInternship2Ready = true;
      } else {
        this.isExempt = true;
      }
    }
  }
}
