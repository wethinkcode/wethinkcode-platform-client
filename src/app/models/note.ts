import { Observable } from 'rxjs/Observable';
import { User } from './user';

export class Note {
  id: any;
  user: User;
  noteTakingUser: User;
  title: String = '';
  content: String = '';
  priority: String = '';
  date: Date;
  $tsl: Observable<number>;
  tslDifference;
  tslSubscription;
  tsl;
  attachments = [];

  constructor(content, date) {
    this.noteTakingUser = new User({});
    this.user = new User({});
    if (date) {
      this.date = date;
    }
    this.tslDifference = Math.floor((this.date.getTime() - new Date().getTime()) / -1000);
    this.tsl = this.getTSL(this.tslDifference);
    this.content = content;
    this.$tsl = Observable.interval(1000).map((x) => {
      this.tslDifference = Math.floor((this.date.getTime() - new Date().getTime()) / -1000);
      return x;
    });
    this.tslSubscription = this.$tsl.subscribe((x) => this.tsl = this.getTSL(this.tslDifference));
  }

  getTSL(elapsed) {
    const currentMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if (elapsed < 30) {
      return 'Now';
    } else if (elapsed < 60) {
      return 'Less than a minute ago';
    } else if (elapsed < 300) {
      return 'Less than 5 minutes ago';
    } else if (elapsed < 3600) {
      return 'Less than 1 hour ago';
    }
    return currentMonth[this.date.getMonth()] + ' ' + this.date.getDate() + ' at ' + this.date.getHours() + ':' + this.date.getMinutes();
  }
}
