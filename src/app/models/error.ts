export class Error {
  code = '';
  status =  '';
  message =  '';
  origin = '';
  priority = 10;
  data = {};

  constructor(code, message, origin, priority) {
      this.code = code;
      this.message = message;
      this.origin = origin;
      this.priority = priority;
  }
}
